var app = angular.module('inspinia');
app.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);
        for (var i=0; i<total; i++)
            input.push(i);
        return input;
    };
});

app.filter('dateNow',['$filter',  function($filter) {
    return function() {
        return $filter('date')(new Date(), 'dd MMMM yyyy');
    };
}])


app.filter('words', function() {
    function isInteger(x) {
        return x % 1 === 0;
    }


    return function(value) {
        if (value && isInteger(value))
            return  toWords(value);

        return value;
    };

});
app.filter('Mydate',['$filter',  function($filter) {
    return function(input) {
        if(input != null)
        {
            return $filter('date')(new Date(input), 'dd MMM yyyy');
        }
        return '';
    };
}])
app.filter('monthName', [function() {
    return function (monthNumber) { //1 = January
        var monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December' ];
        return monthNames[monthNumber - 1];
    }
}]);

app.filter('Mydate_simple',['$filter',  function($filter) {
    return function(input) {
        if(input != null)
        {
            return $filter('date')(new Date(input), 'dd/MM/yyyy');
        }
        return '';

    };
}])

app.filter('MakeJam',['$filter',  function($filter) {
    return function(input) {
        var panjang = input.length;
        var indek1 = input.substring(panjang-2,panjang);
        var indek2 = input.substring(panjang-4,panjang-2);
        var indek3 = input.substring(0,panjang-4);
        if(indek3.length < 2)
        {
            indek3 = "0" + indek3;
        }
        input = input.replace('1970-01-01 ','');
        return input;

    };
}])

app.filter('status_bg',['$filter',  function($filter) {
    return function(input) {
        var warna = ['default','success','success','warning','black'];
        var status = ['draft','active','open','approved','inactive'];
        if(status.indexOf(input.toLowerCase()) > -1)
        {
            return warna[status.indexOf(input.toLowerCase())];
        }
        return 'default';
    };
}]);

app.filter('status',['$filter',  function($filter) {
    return function(input) {
        var warna = ['Draft','Need Approval','Open','Approved','Closed','active'];
        var status = ['draft','confirm','open','approved','close','Active'];
        if(status.indexOf(input) > -1)
        {
            return warna[status.indexOf(input)];
        }
        return 'default';
    };
}]);


app.filter('To_int',['$filter',  function($filter) {
    return function(input) {
        return parseFloat(input) * 1;

    };
}])


var app = angular.module("demoApp", ['ngResource']);

app.filter("trust", ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}])

// app.filter('due_list_val',['$filter',  function($filter) {
//     return 100;
//     return function(variabel,value) {
//         if(variabel =='years' )
//         {
//             return value *360;
//         }
//
//         if(variabel =='months' )
//         {
//             return value * 30;
//         }
//
//         return value;
//     };
// }])

    .filter('currentdate',['$filter',  function($filter) {
        return function() {
            return new Date();
        };
    }])

app.filter('number', [function() {
    return function(input) {
        return parseInt(input, 10);
    };
}]);

app.filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.filter('status_proyek', function() {
    return function(input) {
        if(input == null)
        {
            return "Progress";
        }
    }
});
app.filter('filterTsnVar', function() {
    return function(input) {
        if(input == 'days')
        {
            return "D";
        }
        else if(input == 'months')
        {
            return "M";
        }
        else if(input == 'hours')
        {
            return "H";
        }
        else if(input == 'years')
        {
            return "Y";
        }
    }
});

app.run(function($rootScope) {
    $rootScope.hitung_hari = function(start,end) {
        var hari = (end - start) / (1000 * 60 * 60 * 24);
        // console.log(start);
        return Math.round(hari);
    };
});


app.run(function($rootScope) {
    $rootScope.hitung_hari_form = function(start,end) {
        var hari = (end - start) / (1000 * 60 * 60 * 24);
        // console.log(start);
        return Math.round(hari);
    };
});

app.run(function($rootScope) {
    $rootScope.isLoading = function () {
        return $http.pendingRequests.length !== 0;
    };
});

app.run(function($rootScope) {
    $rootScope.btnSumbit = function (id) {
        return btnSumbit(id)
    };
});

var myApp = angular.module('myApp', []);
myApp.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);

        for (var i=0; i<total; i++) {
            input.push(i);
        }

        return input;
    };
});


function LoadTabel()
{
    element.ready(function () {
        element.find("tfoot th").each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
        });
        console.log('  document ready function, add search by column feature ');
        var table = element.DataTable({
            "autoWidth": true,
            select: true
        });
        // Apply the search
        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

    });
}