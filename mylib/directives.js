/**
 * INSPINIA - Responsive Admin Theme
 *
 * Main directives.js file
 * Define directives for used plugin
 *
 *
 * Functions (directives)
 *  - sideNavigation
 *  - iboxTools
 *  - minimalizaSidebar
 *  - vectorMap
 *  - sparkline
 *  - icheck
 *  - ionRangeSlider
 *  - dropZone
 *  - responsiveVideo
 *  - chatSlimScroll
 *  - customValid
 *  - fullScroll
 *  - closeOffCanvas
 *  - clockPicker
 *  - landingScrollspy
 *  - fitHeight
 *  - iboxToolsFullScreen
 *  - slimScroll
 *  - truncate
 *  - touchSpin
 *  - markdownEditor
 *  - resizeable
 *  - bootstrapTagsinput
 *  - passwordMeter
 *
 */


/**
 * pageTitle - Directive for set Page title - mata title
 */
function pageTitle($rootScope, $timeout) {
    return {
        link: function (scope, element) {
            var listener = function (event, toState, toParams, fromState, fromParams) {
                // Default title - load on Dashboard 1
                var title = 'AMIMS | Responsive Admin Theme';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = 'AMIMS | ' + toState.data.pageTitle;
                $timeout(function () {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    }
};

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $('.side-menu-list li.with-sub').each(function () {
                var parent = $(this),
                    clickLink = parent.find('>span'),
                    subMenu = parent.find('>ul');

                clickLink.click(function () {
                    if (parent.hasClass('opened')) {
                        parent.removeClass('opened');
                        subMenu.slideUp();
                        subMenu.find('.opened').removeClass('opened');
                    } else {
                        if (clickLink.parents('.with-sub').length == 1) {
                            $('.side-menu-list .opened').removeClass('opened').find('ul').slideUp();
                        }
                        parent.addClass('opened');
                        subMenu.slideDown();
                    }
                });
            });

            /* ==========================================================================
            Scroll
            ========================================================================== */

            if (!("ontouchstart" in document.documentElement)) {

                document.documentElement.className += " no-touch";

                var jScrollOptions = {
                    autoReinitialise: true,
                    autoReinitialiseDelay: 100,
                    contentWidth: '0px'
                };

                $('.scrollable .box-typical-body').jScrollPane(jScrollOptions);
                $('.side-menu').jScrollPane(jScrollOptions);
                $('.side-menu-addl').jScrollPane(jScrollOptions);
                $('.scrollable-block').jScrollPane(jScrollOptions);
            }

        }
    };
};

function topNavigation($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $('#show-hide-sidebar-toggle').on('click', function () {
                if (!$('body').hasClass('sidebar-hidden')) {
                    $('body').addClass('sidebar-hidden');
                } else {
                    $('body').removeClass('sidebar-hidden');
                }
            });

        }
    };
};


var table;

function untukDataTable($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            scope.$watch('loadData', function (val) {
                console.log(val);
                if (table) {
                    table.destroy();
                    table=null;
                }
                if (val === true) {

                    $timeout(function () {

                        /*
                        element.find("tfoot th").each(function () {
                            var title = $(this).text();
                            $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
                        });
                        */
                        var option = {
                            responsive: true,
                            "autoWidth": false
                        };
                        if (attrs.order > -1) {
                            option.order = [[attrs.order, "desc"]];
                        }
                        else {
                            option.order = [];
                        }
                        console.log(option)

                        console.log('  document ready function, add search by column feature ');
                        table = element.DataTable(option);
                        /*
                        // Apply the search
                        table.columns().every(function () {
                            var that = this;
                            $('input', this.footer()).on('keyup change', function () {
                                console.log(that)
                                if (that.search() !== this.value) {
                                    that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                        });
                        */
                    }, 200)

                }
            });

        }
    };


}

/**
 *
 * Pass all functions into module
 */
angular
    .module('inspinia')
    .directive('pageTitle', pageTitle)
    .directive('sideNavigation', sideNavigation)
    .directive('topNavigation', topNavigation)
    .directive('myDataTable', untukDataTable);


//my directive

var app = angular.module('myDirectiv', []);

// Click to navigate
// similar to <a href="#/partial"> but hash is not required,
// e.g. <div click-link="/partial">
app.directive('clickLink', function ($location) {
    return {
        link: function (scope, element, attrs) {
            element.on('click', function () {
                scope.$apply(function () {
                    var url = attrs.clickLink;
                    // var show = attrs.show;
                    console.log("asdsad");
                    $('#detail').addClass('wd_40');
                    $location.path(url);


                });
            });
        }
    }
});


app.directive('onClick', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            $(element).click(function (e, rowid) {
                scope.method({myParam: id});
            });
        }
    };
});


app.directive("backButton", ["$window", function ($window) {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
            elem.bind("click", function () {
                $window.history.back();
                if ($('#popup')[0]) {
                    $('#popup').remove();
                }
            });
        }
    };
}]);

app.directive("backDblClick", ["$window", function ($window) {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
            elem.bind("dblclick", function () {
                $window.history.back();
            });
        }
    };
}]);

app.directive('datepicker', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, el, attr, ngModel) {
            jQuery(el).datepicker({format: 'yyyy-mm-dd'})
                .on('changeDate', function (ev) {
                    console.log(ev);
                    console.log($(ev.target).val());

                    ngModel.$viewValue = ev.date;
                    // ngModel.$commitViewValue();
                    ngModel.$setViewValue(ev.date);

                });
        }
    };
});


app.directive('timepicker', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, el, attr, ngModel) {
            jQuery(el).datepicker({format: 'hh:mm'})
                .on('changeDate', function (ev) {
                    console.log(ev);
                    console.log($(ev.target).val());

                    ngModel.$viewValue = ev.date;
                    // ngModel.$commitViewValue();
                    ngModel.$setViewValue(ev.date);

                });
        }
    };
});


app.directive('datelimit', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, el, attr, ngModel) {

            // tanggal();
            jQuery(el).click(function functionName() {

                tanggal();
            })
            var awal = '';

            function tanggal() {
                //$(el).datepicker('remove');

                var startDate = $(el).attr("startDate");
                var endDate = $(el).attr("endDate");
                console.log(endDate);
                if (startDate) {
                    if (startDate !== awal) {
                        $(el).datepicker('remove');
                        awal = startDate;
                    }


                    startDate = new Date(startDate);
                    endDate = new Date(endDate);

                    console.log(startDate);
                    console.log(endDate);
                }
                else {
                    awal = startDate;
                    console.log("empty");
                    startDate = new Date('2016-12-08');
                    endDate = new Date('2016-12-08');
                }


                $(el).datepicker({
                    startDate: startDate,
                    endDate: endDate,
                    format: 'yyyy-mm-dd'
                })
                    .on('changeDate', function (ev) {
                        ngModel.$viewValue = ev.date;
                        // ngModel.$commitViewValue();
                        ngModel.$setViewValue(ev.date);

                    });
                // $(el).datepicker('update');

            }

        }
    };
});

app.directive('select2lama', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $timeout(function () {
                element.show();
                $(element).select2();
                console.log("asds");
            });
        }
    };
});

app.directive("select2", function ($timeout, $http) {
    return {
        restrict: 'AC',
        require: 'ngModel',
        link: function (scope, element, attrs, model) {


            var x = 0;

            function cekPending() {

                if ($http.pendingRequests.length == 0 && x == 0) {
                    $(element).select2();
                    console.log($http.pendingRequests.length);
                    x = 1;

                    model.$render = function () {
                        $(element).select2("val", model.$viewValue);
                        console.log(model.$viewValue);
                    }
                }
                else {
                    $timeout(function () {
                        cekPending();
                    }, 1000);
                }

            }

            $timeout(function () {
                cekPending();
            }, 1000);


            $(element).on('change', function () {
                scope.$apply(function () {
                    model.$setViewValue($(element).select2("val"));

                });

            })
        }
    };
});

app.directive("select2lokasi", function ($timeout) {
    return {
        restrict: 'AC',
        require: 'ngModel',
        link: function (scope, element, attrs, model) {
            // scope.$watch(attrs.ngModel, function(newVal, oldVal){
            //         console.log("ganti");
            //         $timeout(function() {
            //             $('.' + attrs.ngModelNext).select2();
            //         },1000);
            //  });

            $timeout(function () {
                $(element).select2();
            }, 1000);

            model.$render = function () {
                $(element).select2("val", model.$viewValue);
                console.log(model.$viewValue);
            }
            $(element).on('change', function () {
                scope.$apply(function () {
                    model.$setViewValue($(element).select2("val"));

                });

            })
        }
    };
});

app.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return parseInt(val, 10);
                console.log(val);
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});

app.directive('disallowSpaces', function () {
    return {
        restrict: 'A',

        link: function ($scope, $element) {
            $element.bind('input', function () {
                $(this).val($(this).val().replace(/ /g, ''));
            });
        }
    };
});


app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});


app.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
app.directive('customSubmit', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {

            if (attributes["novalidate"] == undefined)
                attributes.$set('novalidate', 'novalidate');

            $('#sumbit_button').click(function () {
                console.log("sadsadasda");

                eventForm();
            })


            function eventForm() {
                var form = scope.$eval(attributes.name);


                element.bind('submit', function (e) {
                    e.preventDefault();

                    // Get the form object.
                    var form = scope.$eval(attributes.name);
                    if (form.$valid) {
                        // If the form is valid call the method specified
                        scope.$eval(attributes.customSubmit);
                        return;
                    }
                    else {
                        warningView("form kosong");
                    }

                    element.addClass('submitted');

                });
            }

        }
    };
})



