var WindowTinggi = $(window).height();
var NavBarTinggi = $('.navbar-header').height() ;
var WindowLebar = $(window).width() ;
var SidebarLebar = $('.sidebar').width() ;

function wrapper() {
    var wrapper_height = $('#wrapper').height();
    $('.wrapper').height(wrapper_height - 100);
}

$('ul#page-menu li').click(function()
{
    $('ul#page-menu li > a').removeClass('active');
    $(this).children('a').addClass('active');
    console.log("ASdsa");

})

function Melayang()
{
    $('.popUp').show();
    $('.popUp .ibox-content').height($('.wrapper').height()-100);
}

function colosePopup()
{
    $('.popUp').hide();
}

function formAutoLoad()
{
    if ( $( ".select2" ).length ) {
        // $(".select2").select2();
    }

    if($('.form-content').length)
    {
        var tinggi = $('.hitam-popup').height();
        $('.form-content').height(tinggi - 200);
    }

}

function infoAction(pesan,id)
{
    $('#'+ id).click(function()
    {
        swal("Apakah Anda Yakin", pesan, "warning");
    });
}

function warningView(pesan)
{
    swal("Failed", pesan, "warning");
}

function errorView(pesan)
{
    if(pesan)
    {
        swal("Failed", pesan, "error");
    }
    else
    {
        swal("Failed", "Invalid data!", "error");
    }

}

function berhasilView(pesan)
{
    swal({
        title: "Saved",
        type: "success"
    });
}



function PindahParam($http,$state,url, data, redirek ,param_redirek,pesan)
{
    swal({
       title: "Confirmation?",
       text: "Are you sure continue to process "+ pesan +" ? ",
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, Process it!",
       cancelButtonText: "No, cancel it!",
       closeOnConfirm: false,
       closeOnCancel: false },
    function(isConfirm){
       if (isConfirm) {
           return $http.put(BASE_URL + url, data)
           .then(function mySuccesresponse()
           {
               swal({
                   title: "Success!",
                   type: "success"
               });

              $state.go(redirek,param_redirek, { reload: true })
           }
           , function myError(response)
           {
               if (response.data)
               {
                   errorView(response.data.status);
               }
               else
               {
                   errorView();
               }

           });

       } else {
           swal({
               title: "Cancelled",
               type: "success"
           });
       }
    });
}


function PindahParam2($http,$state,url, data, redirek ,param_redirek,pesan)
{
    swal({
       title: "Confirmation?",
       text: "Are you sure continue to process "+ pesan +"  ? ",
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, process it!",
       cancelButtonText: "No, cancel it!",
       closeOnConfirm: false,
       closeOnCancel: false },
    function(isConfirm){
       if (isConfirm) {
           return $http.put(BASE_URL + url, data)
           .then(function mySuccesresponse()
           {
              swal("Success!", " Request successfully sent.", "success");
              $state.go(redirek,param_redirek, { reload: true })
           }
           , function myError()
           {
               errorView();
           });

       } else {
          swal("Cancelled", "Your data is safe :)", "error");
       }
    });
}

function PindahParam3($http,$state,url, data, redirek ,param_redirek,pesan)
{
    swal({
       title: "Confirmation?",
       text: "Are you sure continue to process "+ pesan +"  ? ",
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, process it!",
       cancelButtonText: "No, cancel it!",
       closeOnConfirm: false,
       closeOnCancel: false },
    function(isConfirm){
       if (isConfirm) {
           return $http.post(BASE_URL + url, data)
           .then(function mySuccesresponse()
           {
              swal("Success!", " Request successfully sent.", "success");
              $state.go(redirek,param_redirek, { reload: true })
           }
           , function myError()
           {
               errorView();
           });

       } else {
           swal({
               title: "Cancelled",
               type: "success"
           });
       }
    });
}

function deleteParam($http,$state,url, data, redirek,param_redirek)
{
    swal({
       title: "Are you sure?",
       // text: "Your will not be able to recover this data!",
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
       cancelButtonText: "No, cancel it!",
       closeOnConfirm: false,
       closeOnCancel: false },
    function(isConfirm){
       if (isConfirm) {
           return $http.delete(BASE_URL + url, data)
           .then(function mySuccesresponse()
           {
               swal({
                   title: "Deleted!",
                   type: "success"
               });
               $state.go(redirek,param_redirek, { reload: true })

           }
           , function myError(respon)
           {
               errorView(respon.data.status);
           });

       } else {
           swal({
               title: "Cancelled",
               type: "success"
           });
       }
    });
}

function deleteParamStandar($http,$state,url, data,cb)
{
    swal({
       title: "Are you sure?",
       // text: "Your will not be able to recover this data!",
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
       cancelButtonText: "No, cancel it!",
       closeOnConfirm: false,
       closeOnCancel: false },
    function(isConfirm){
       if (isConfirm) {
           return $http.delete(BASE_URL + url, data)
           .then(function mySuccesresponse()
           {
               swal({
                   title: "Deleted!",
                   type: "success"
               });
               return cb(true);

           }
           , function myError(respon)
           {
               errorView(respon.data.status);
               return cb(true);
           });

       } else {
           swal({
               title: "Cancelled",
               type: "success"
           });
           return cb(true);
       }
    });
}

var count_hari = function(start,end) {
    var hari = (end - start) / (1000 * 60 * 60 * 24);
    return Math.round(hari);
};

function toForamatDate(tanggal)
{

    return $filter('date')(new Date(), 'dd MMMM yyyy');

}




// function btnSumbit(id) {
//     console.log("sadsa");
//     // var frmvalid = $(id).valid();
//     // if (!frmvalid) {
//     //     errorView();
//     //     return false;
//     // }
//     $(id).submit();
// }



