- Status pada AIRCRAFT belum (dummy)


modeul
    -   lib
        - Master Part (mpart)
        - Ata Chapter



/**
 * INSPINIA - Responsive Admin Theme
 *
 */

angular
    .module('inspinia')


/*----------------------------------------------------------------------*/
/*                      Request
/*----------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------
 PIRO
 /*----------------------------------------------------------------------------------------------*/
.controller('app.request.pro', function truncateCtrl($scope,$state,$stateParams,myHelp,$window,$rootScope,$timeout,$http){
    $scope.filters = {};
    $scope.status_piro = ['draft','confirm','open','close'];

    $scope.loadData = false;
    $scope.filter = function(page)
    {
        $scope.filters.page = page;
        myHelp.getParam('/mpc/piro',clearObj($scope.filters))
            .then(function(respons){
                $scope.datas = respons.data.data;
                $scope.filters = respons.data.request;
                $scope.loadData = true;
            });
    }
    $scope.filter(1);

    $scope.delete = function (id) {
        deleteParam($http, $state, '/mpc/piro/' + id, {}, 'app.request.pro', {});
    }

    $scope.export_report = function (id) {
        var url = $rootScope.report_url('release/PRO.rpt','&' + Object_toReport({id:id}));
        $window.open(url);
    }
})

.controller('app.request.pro.detail', function truncateCtrl($http,$scope,$state,$stateParams,myHelp){
    $scope.loadData = false;
    $scope.items = [];
    myHelp.getDetail('/mpc/piro/' + $stateParams.id_mpc_piro)
        .then(function(respons){
            $scope.piro = respons.data.data;
            $scope.items = respons.data.item;
            $scope.loadData = true;
        });

    $scope.BASE_URL = BASE_URL;
    $scope.acc = function (acc) {
        PindahParam($http,$state,'/mpc/piro/' + acc, {id_mpc_piro:$stateParams.id_mpc_piro} ,'app.request.pro.detail',{id_mpc_piro:$stateParams.id_mpc_piro},acc);
    }

    $scope.deletedItem = function(id) {
        deleteParam($http,$state,'/mpc/piro_item/' + id, {id_mpc_piro:$stateParams.id_mpc_piro}, $state.current.name,{});
    }


})


.controller('app.request.pro.add', function truncateCtrl($scope,$state,$stateParams,myHelp){
    $scope.items = [];
    $scope.mpart = {};
    $scope.param = {};
    myHelp.getDetail('/mpc/piro/create')
        .then(function(respons){
            $scope.piro = respons.data.data;
            $scope.aircraft = respons.data.aircraft;
            $scope.item = respons.data.item;
            debugData(respons);

            $scope.param.id_mpc_piro = $scope.piro.id_mpc_piro;
            $scope.getItems($scope.piro.id_mpc_piro);

        });

    /*-------------------------- POPUP  ----------------------------------------*/
    $scope.popup = false;
    $scope.startPopup = function()
    {
        $scope.popup = true;
    }
    $scope.closePopup = function()
    {
        $scope.popup = false;
    }

    /*-------------------------- POPUP  ----------------------------------------*/


    $scope.loadData = false;
    $scope.getItems = function($id)
    {
        myHelp.getDetail('/mpc/piro_item/' + $id)
            .then(function(respons){
                $scope.items = respons.data;
                $scope.loadData = true;
            });
    }
    //persiapan tambah >>>
    myHelp.getDetail('/master/mpart/create')
        .then(function(respons){
            $scope.master = respons.data;
            debugData(respons);
        });

    //persiapan tambah >>>

    $scope.simpanForm = function() {
        var Param = clearObj($scope.piro);
        myHelp.putParam('/mpc/piro/' + $scope.param.id_mpc_piro , Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.request.pro",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    };

    $scope.priority=['Normal', 'Urgent', 'AOG'];
    $scope.popupSearchParent = function () {
        var param = {};
        if($scope.mpart.keyword.length > 0)
        {
            param = clearObj($scope.mpart);
            param.limit = true;
            myHelp.getParam('/master/mpart',param)
                .then(function(respons){
                    $scope.params = respons.data;
                });
        }
    }

    $scope.selectItem = function(id_mpart,keyword)
    {
        $scope.param.id_mpart = id_mpart;
        console.log($scope.param);
        $scope.keyword=keyword;
        $scope.popup = false;
    }

    $scope.deletedItem = function (id) {
        myHelp.delete('/mpc/piro_item/' + id)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.getItems($scope.piro.id_mpc_piro);
                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    }

    $scope.submitItemAdd = function () {
        var param = clearObj($scope.param);
        myHelp.postParam('/mpc/piro_item', param)
            .then(function mySuccesresponse(response)
                {
                    berhasilView();
                    $scope.items = response.data;
                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    }

})

.controller('app.request.pro.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){
    $scope.items = [];
    $scope.mpart = {};
    $scope.param = {};
    myHelp.getDetail('/mpc/piro/' + $stateParams.id_mpc_piro + '/edit')
        .then(function(respons){
            $scope.piro = respons.data.data;
            $scope.aircraft = respons.data.aircraft;
            $scope.item = respons.data.item;
            debugData(respons);

            $scope.param.id_mpc_piro = $scope.piro.id_mpc_piro;
            $scope.getItems($scope.piro.id_mpc_piro);

        });

    /*-------------------------- POPUP  ----------------------------------------*/
    $scope.popup = false;
    $scope.startPopup = function()
    {
        $scope.popup = true;
    }
    $scope.closePopup = function()
    {
        $scope.popup = false;
    }

    /*-------------------------- POPUP  ----------------------------------------*/


    $scope.loadData = false;

    $scope.getItems = function($id)
    {
        myHelp.getDetail('/mpc/piro_item/' + $id)
            .then(function(respons){
                $scope.items = respons.data;
                $scope.loadData = false;
            });
    }
    //persiapan tambah >>>
    myHelp.getDetail('/master/mpart/create')
        .then(function(respons){
            $scope.master = respons.data;
            debugData(respons);
        });

    //persiapan tambah >>>

    $scope.simpanForm = function() {
        var Param = clearObj($scope.piro);
        myHelp.putParam('/mpc/piro/' + $scope.param.id_mpc_piro , Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.request.pro",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    };

    $scope.mpart="";
    $scope.priority=['Normal', 'Urgent', 'AOG'];
    $scope.popupSearchParent = function () {
        var param = {};

        if($scope.mpart.keyword.length > 0)
        {
            param = clearObj($scope.mpart);
            console.log(param);
            myHelp.getParam('/master/mpart',param)
                .then(function(respons){
                    $scope.params = respons.data;
                });
        }
    }

    $scope.selectItem = function(id_mpart,keyword)
    {
        $scope.param.id_mpart = id_mpart;
        console.log($scope.param);
        $scope.keyword=keyword;
        $scope.popup = false;
    }

    $scope.deletedItem = function (id) {
        myHelp.delete('/mpc/piro_item/' + id)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.getItems($scope.piro.id_mpc_piro);
                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    }

    $scope.submitItemAdd = function () {
        var param = clearObj($scope.param);
        myHelp.postParam('/mpc/piro_item', param)
            .then(function mySuccesresponse(response)
                {
                    berhasilView();
                    $scope.items = response.data;
                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    }

})