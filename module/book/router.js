function configengineer($stateProvider) {

    $stateProvider
        .state('app.book', {
            url: "/book",
            abstract:true,
            data: {pageTitle: 'book'}
        })
        .state('app.book.management', {
            url: "/management",
            templateUrl: "module/book/views/management/data.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management"
        })
        .state('app.book.management.add', {
            url: "/add",
            templateUrl: "module/book/views/management/add.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management.add"
        })
        .state('app.book.management.edit', {
            url: "/edit/:id_book",
            templateUrl: "module/book/views/management/edit.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management.edit"
        })
        .state('app.book.management.detail', {
            url: "/detail/:id_book",
            templateUrl: "module/book/views/management/detail.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management.detail"
        })

        .state('app.book.management.detail.content', {
            url: "/content",
            templateUrl: "module/book/views/management/detail.content.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management.detail.content"
        })

        .state('app.book.management.detail.section_add', {
            url: "/section_add",
            templateUrl: "module/book/views/management/detail.section_add.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management.detail.section_add"
        })

        .state('app.book.management.detail.section_edit', {
            url: "/section_edit/:id_book_section",
            templateUrl: "module/book/views/management/detail.section_edit.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management.detail.section_edit"
        })
        
        .state('app.book.management.detail.content_add', {
            url: "/content_add/:id_book_section",
            templateUrl: "module/book/views/management/detail.content_add.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management.detail.content_add"
        })

        .state('app.book.management.detail.content_edit', {
            url: "/content_edit/:id_book_content",
            templateUrl: "module/book/views/management/detail.content_edit.html",
            data: {pageTitle: 'book'},
            controller: "app.book.management.detail.content_edit"
        })

        .state('app.book.management.detail.ticket', {
            url: "/ticket",
            templateUrl: "module/book/views/ticket/data.html",
            data: {pageTitle: 'ticket'},
            controller: "app.book.management.detail.ticket"
        })
        .state('app.book.management.detail.ticket_detail', {
            url: "/ticket_detail/:id_ticket",
            templateUrl: "module/book/views/ticket/ticket_detail.html",
            data: {pageTitle: 'ticket_detail'},
            controller: "app.book.management.detail.ticket_detail"
        })
}

angular
    .module('inspinia')
    .config(configengineer)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });