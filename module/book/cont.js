
angular
    .module('inspinia')

    .controller('app.book.management', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/book/admin/books')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        //hanya di panggil jika state
        if($state.current.name === 'app.book.management')
        {
            $scope.filter();
        }


        $scope.delete = function (id) {
            deleteParam($http, $state, '/book/admin/books/' + id, {}, 'app.book.management', {});
        }
    })

    .controller('app.book.management.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.book = {};

        myHelp.getParam('/book/admin/books/create',{})
            .then(function (respons) {
                $scope.master = respons.data.data;
            });

        myHelp.postParam('/book/admin/books',{})
            .then(function (respons) {
                $scope.book = respons.data.data;
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.book);
            myHelp.putParam('/book/admin/books/' + $scope.book.id_book, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.book.management.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/book/admin/books/' + $stateParams.id_book +'/edit')
            .then(function (respons) {
                $scope.book = respons.data.data;

                //panggil setelah data ada
                myHelp.getParam('/book/admin/books/create',{})
                    .then(function (respons) {
                        $scope.master = respons.data.data;
                    });

            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.book);
            myHelp.putParam('/book/admin/books/' + $scope.book.id_book, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.book.management.detail', function truncateCtrl($scope, $state, $stateParams,$http, myHelp) {

        $scope.getBookDetail = function () {
            myHelp.getDetail('/book/admin/books/' + $stateParams.id_book)
                .then(function (respons) {
                    $scope.book = respons.data.data;
                });
        }

        $scope.getBookDetail();
        if($state.current.name === 'app.book.management.detail')
        {
            $state.go("app.book.management.detail.content",{id_book:$stateParams.id_book},{});
        }


        // Ganti foto thum
        $scope.changeThum = function(type) {
            var file = $scope.file_thum;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.book.id_book);
            fd.append('type', type);
            $http.post(BASE_URL + '/book/admin/book_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getBookDetail();
                    $scope.file_thum = false;
                }, function (err) {
                    errorView()
                })

        };
        // Ganti foto cover
        $scope.changeCover = function(type) {
            var file = $scope.file_cover;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.book.id_book);
            fd.append('type', type);
            $http.post(BASE_URL + '/book/admin/book_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getBookDetail();
                    $scope.file_cover = false;
                }, function (err) {
                    errorView()
                })

        };

    })
    .controller('app.book.management.detail.content', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {

        $scope.getContent = function()
        {
            myHelp.getParam('/book/admin/bookSections',{id_book:$stateParams.id_book})
                .then(function (respons) {
                    $scope.sections = respons.data.data;
                });
        }
        $scope.getContent();

        $scope.deleteSection = function(id) {
            deleteParam($http,$state,'/book/admin/bookSections/' + id, {}, 'app.book.management.detail.content',{id_book:$stateParams.id_book});
        }
        $scope.deletContent = function(id) {
            deleteParam($http,$state,'/book/admin/bookContents/' + id, {}, 'app.book.management.detail.content',{id_book:$stateParams.id_book});
            $scope.getContent();
        }
    })


    .controller('app.book.management.detail.section_add', function truncateCtrl($scope, $state, $stateParams, myHelp) {



        myHelp.postParam('/book/admin/bookSections',{id_book:$stateParams.id_book})
            .then(function (respons) {
                $scope.section = respons.data.data;
            });

        $scope.saveSection = function () {
            myHelp.putParam('/book/admin/bookSections/' + $scope.section.id_book_section,clearObj($scope.section))
                .then(function (res) {
                    berhasilView();
                    $state.go("app.book.management.detail.content",{id_book:$stateParams.id_book},{reload: true});

                }, function (err) {
                    errorView(err);
                })
        }

    })

    .controller('app.book.management.detail.section_edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/book/admin/bookSections/' + $stateParams.id_book_section)
            .then(function (respons) {
                $scope.section = respons.data.data;
            });

        $scope.saveSection = function () {
            myHelp.putParam('/book/admin/bookSections/' + $scope.section.id_book_section,clearObj($scope.section))
                .then(function (res) {
                    berhasilView();
                    $state.go("app.book.management.detail.content",{id_book:$stateParams.id_book},{reload: true});

                }, function (err) {
                    errorView(err);
                })
        }

    })
    
    

    .controller('app.book.management.detail.content_add', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.tinymceModel = 'Initial content';

        $scope.getContent = function() {
            console.log('Editor content:', $scope.tinymceModel);
        };

        $scope.setContent = function() {
            $scope.tinymceModel = 'Time: ' + (new Date());
        };

        $scope.tinymceOptions = {
            plugins: 'link image code',
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
        };

        myHelp.postParam('/book/admin/bookContents',{id_book_section:$stateParams.id_book_section})
            .then(function (respons) {
                $scope.content = respons.data.data;
            });

        $scope.saveContent = function () {
            myHelp.putParam('/book/admin/bookContents/' + $scope.content.id_book_content,clearObj($scope.content))
                .then(function (res) {
                    berhasilView();
                    $state.go("app.book.management.detail.content",{id_book:$stateParams.id_book},{reload: true});

                }, function (err) {
                    errorView(err);
                })
        }

    })

    .controller('app.book.management.detail.content_edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/book/admin/bookContents/' + $stateParams.id_book_content)
            .then(function (respons) {
                $scope.content = respons.data.data;
            });

        $scope.saveContent = function () {
            myHelp.putParam('/book/admin/bookContents/' + $scope.content.id_book_content,clearObj($scope.content))
                .then(function (res) {
                    berhasilView();
                    $state.go("app.book.management.detail.content",{id_book:$stateParams.id_book},{reload: true});

                }, function (err) {
                    errorView(err);
                })
        }

    })

// TICKET

    .controller('app.book.management.detail.ticket', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {

        $scope.loadData = false;
        $scope.getTicket = function(status)
        {
            $scope.loadData = false;
            myHelp.getParam('/ticket/admin/tickets', {id_book:$stateParams.id_book,ticket_status : status} )
                .then(function (respons) {
                    $scope.list_ticket = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.getTicket ('open');



    })

    .controller('app.book.management.detail.ticket_detail', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {
        $scope.delete_answere = function(id) {
            deleteParam($http,$state,'/ticket/admin/ticket_answers/' + id, {}, 'app.book.management.detail.ticket_detail',{id_book:$stateParams.id_book, id_packet:$stateParams.id_ticket});
        }
        myHelp.getDetail('/ticket/admin/tickets/' + $stateParams.id_ticket + ' ?for_module=packet')
            .then(function (respons) {
                $scope.detail_ticket = respons.data.data;
            });

        $scope.getListComment = function () {
            myHelp.getDetail('/ticket/admin/ticket_answers?id_ticket=' + $stateParams.id_ticket)
                .then(function (respons) {
                    $scope.ticket_answers = respons.data.data;
                });
        }
        $scope.getListComment();




        $scope.submitFormAnswere = function () {
            $scope.answer.id_ticket = $stateParams.id_ticket;
            var Param = clearObj($scope.answer);
            myHelp.postParam('/ticket/admin/ticket_answers', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        // return false;
                        $scope.getListComment();
                        $scope.answer.answer = '';
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })




