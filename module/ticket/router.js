function configengineer($stateProvider) {

    $stateProvider
        .state('app.ticket', {
            url: "/ticket",
            abstract:true,
            data: {pageTitle: 'ticket'}
        })
        .state('app.ticket.management', {
            url: "/management",
            templateUrl: "module/ticket/views/management/data.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.management"
        })
        .state('app.ticket.management.add', {
            url: "/add",
            templateUrl: "module/ticket/views/management/add.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.management.add"
        })
        .state('app.ticket.management.edit', {
            url: "/edit/:id_ticket",
            templateUrl: "module/ticket/views/management/edit.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.management.edit"
        })
        .state('app.ticket.management.detail', {
            url: "/detail/:id_ticket",
            templateUrl: "module/ticket/views/management/detail.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.management.detail"
        })
        .state('app.ticket.management.detail.book', {
            url: "/book",
            templateUrl: "module/ticket/views/management/detail.book.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.management.detail.book"
        })


        .state('app.ticket.team', {
            url: "/team",
            templateUrl: "module/ticket/views/team/data.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.team"
        })
        .state('app.ticket.team.add', {
            url: "/add",
            templateUrl: "module/ticket/views/team/add.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.team.add"
        })
        .state('app.ticket.team.edit', {
            url: "/edit/:id_ticket_team",
            templateUrl: "module/ticket/views/team/edit.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.team.edit"
        })
        .state('app.ticket.team.detail', {
            url: "/detail/:id_ticket_team",
            templateUrl: "module/ticket/views/team/detail.html",
            data: {pageTitle: 'ticket'},
            controller: "app.ticket.team.detail"
        })
}

angular
    .module('inspinia')
    .config(configengineer)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });