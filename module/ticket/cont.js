
angular
    .module('inspinia')

    .controller('app.ticket.management', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function (status) {
            $scope.loadData = false;
            myHelp.getParam('/ticket/admin/tickets' , {ticket_status:status, for_module:'packet'})
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter('open');

        $scope.delete = function (id) {
            myHelp.deleteParam('/master/ticket/ticket/' + id, {})
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.ticket.management", {}, {reload: true})
                    }
                    , function myError(respon) {
                        errorView(respon.data.status);
                    });
        }
    })

    .controller('app.ticket.management.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.ticket = {};

        myHelp.getParam('/ticket/admin/ticket/create',{})
            .then(function (respons) {
                $scope.master = respons.data.data;
            });

        myHelp.postParam('/ticket/admin/ticket',{})
            .then(function (respons) {
                $scope.ticket = respons.data.data;
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.ticket);
            myHelp.putParam('/ticket/admin/ticket/' + $scope.ticket.id_ticket, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.ticket.management.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/ticket/admin/ticket/' + $stateParams.id_ticket +'/edit')
            .then(function (respons) {
                $scope.ticket = respons.data.data;

                //panggil setelah data ada
                myHelp.getParam('/ticket/admin/ticket/create',{})
                    .then(function (respons) {
                        $scope.master = respons.data.data;
                    });

            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.ticket);
            myHelp.putParam('/ticket/admin/ticket/' + $scope.ticket.id_ticket, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.ticket.management.detail', function truncateCtrl($scope, $state, $stateParams,$http, myHelp) {

        $scope.getticketDetail = function()
        {
            myHelp.getDetail('/ticket/admin/ticket/' + $stateParams.id_ticket)
            .then(function (respons) {
                $scope.ticket = respons.data.data;
            });
        };
        $scope.getticketDetail();

        if($state.current.name === 'app.ticket.management.detail')
        {
            $state.go("app.ticket.management.detail.book",{id_ticket:$stateParams.id_ticket},{});
        }


        $scope.changeThum = function(type) {
            var file = $scope.file_thum;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.ticket.id_ticket);
            fd.append('type', type);
            $http.post(BASE_URL + '/ticket/admin/ticket_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getticketDetail();
                    $scope.file_thum = false;
                }, function (err) {
                    errorView()
                })

        };
        // Ganti foto cover
        $scope.changeCover = function(type) {
            var file = $scope.file_cover;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.ticket.id_ticket);
            fd.append('type', type);
            $http.post(BASE_URL + '/ticket/admin/ticket_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getticketDetail();
                    $scope.file_cover = false;
                }, function (err) {
                    errorView()
                })

        };



    })

    .controller('app.ticket.management.detail.book', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {

        $scope.getContent = function()
        {
            myHelp.getParam('/book/admin/books',{id_ticket:$stateParams.id_ticket})
                .then(function (respons) {
                    $scope.books = respons.data.data;
                });
        }
        $scope.getContent();

        $scope.deleteSection = function(id) {
            deleteParam($http,$state,'/book/admin/bookSections/' + id, {}, 'app.book.management.detail.content',{id_book:$stateParams.id_book});
        }
        $scope.deletContent = function(id) {
            deleteParam($http,$state,'/book/admin/bookContents/' + id, {}, 'app.book.management.detail.content',{id_book:$stateParams.id_book});
            $scope.getContent();
        }
    })

/*-------------------------------------------------------------------------------------------*/
/*                  TEAM
/*-------------------------------------------------------------------------------------------*/

    .controller('app.ticket.team', function truncateCtrl($scope,$http, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/ticket/admin/ticket_teams')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();

        $scope.delete = function (id) {
            deleteParam($http, $state, '/ticket/admin/ticket_teams/' + id, {}, 'app.ticket.team', {});
        }
    })

    .controller('app.ticket.team.add', function truncateCtrl($scope, $state,$timeout, $stateParams, myHelp) {
        $scope.team = {};
        $scope.id_ticket = [];
        $scope.tickets = [];

        myHelp.postParam('/ticket/admin/ticket_teams',{})
            .then(function (respons) {
                $scope.team = respons.data.data;
            });

        myHelp.getParam('/company/admin/companies',{})
            .then(function (respons) {
                $scope.companies = respons.data.data;
            });

        $scope.getticket = function()
        {
            $timeout(function () {
                myHelp.getParam('/ticket/admin/ticket',{id_company:clearInt($scope.team.for_company)})
                    .then(function (respons) {
                        $scope.tickets = respons.data.data;
                    });
            }, 200)
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.team);
            myHelp.putParam('/ticket/admin/ticket_teams/' + $scope.team.id_ticket_team, Param)
                .then(function mySuccesresponse() {
                        berhasilView();

                        myHelp.postParam('/ticket/admin/ticket_team_members',{id_ticket_team:$scope.team.id_ticket_team, id_ticket:$scope.id_ticket})
                            .then(function (respons) {
                                $state.go("^", {}, {reload: true})
                            });

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.ticket.team.edit', function truncateCtrl($scope,$timeout, $state, $stateParams, myHelp) {

        $scope.team = {};
        $scope.id_ticket = [];
        $scope.tickets = [];

        myHelp.getDetail('/ticket/admin/ticket_teams/' + $stateParams.id_ticket_team )
            .then(function (respons) {
                $scope.team = respons.data.data;

                myHelp.getParam('/company/admin/companies',{})
                    .then(function (respons) {
                        $scope.companies = respons.data.data;
                    });
                $scope.getticket();

                myHelp.getParam('/ticket/admin/ticket_team_members',{id_ticket_team:$stateParams.id_ticket_team})
                    .then(function (respons) {
                        for(let i=0; i < respons.data.data.length; i++)
                        {
                            $scope.id_ticket.push(respons.data.data[i].id_ticket);
                        }
                    });

            });



        $scope.getticket = function()
        {
            $timeout(function () {
                myHelp.getParam('/ticket/admin/ticket',{id_company:clearInt($scope.team.for_company)})
                    .then(function (respons) {
                        $scope.tickets = respons.data.data;
                    });
            }, 200)
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.team);
            myHelp.putParam('/ticket/admin/ticket_teams/' + $scope.team.id_ticket_team, Param)
                .then(function mySuccesresponse() {
                        berhasilView();

                        myHelp.postParam('/ticket/admin/ticket_team_members',{id_ticket_team:$scope.team.id_ticket_team, id_ticket:$scope.id_ticket})
                            .then(function (respons) {
                                $state.go("^", {}, {reload: true})
                            });

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.ticket.team.detail', function truncateCtrl($scope, $state, $stateParams,$http, myHelp) {

        $scope.getTeamDetail = function()
        {
            myHelp.getDetail('/ticket/admin/ticket_teams/' + $stateParams.id_team)
                .then(function (respons) {
                    $scope.team = respons.data.data;
                });
        };
        $scope.getTeamDetail();


    })


