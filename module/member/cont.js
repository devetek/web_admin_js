
angular
    .module('inspinia')

    .controller('app.member.report', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/member/admin/member_reports')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();
    })

    /*---------------------------------------------------------------------------------------------
    MANAGEMENT
    /*---------------------------------------------------------------------------------------------*/
    .controller('app.member.management', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            var param = {};
            if($stateParams.status_member !='all')
            {
                param.status_member = '%' + $stateParams.status_member;
            }
            param.id_company = $stateParams.id_company;
            myHelp.getParam('/member/admin/members',param)
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();

        $scope.delete = function (id) {
            myHelp.deleteParam('/master/member/members/' + id, {})
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.member.management", {}, {reload: true})
                    }
                    , function myError(respon) {
                        errorView(respon.data.status);
                    });
        }
    })

    .controller('app.member.management.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.member = {};

        myHelp.getParam('/member/admin/members/create',{})
            .then(function (respons) {
                $scope.master = respons.data.data;
            });

        myHelp.postParam('/member/admin/members',{})
            .then(function (respons) {
                $scope.member = respons.data.data;
            });

        $scope.submitForm = function () {
            $scope.member.id_company = $stateParams.id_company;
            var Param = clearObj($scope.member);
            myHelp.putParam('/member/admin/members/' + $scope.member.id_member, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.member.management.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/member/admin/members/' + $stateParams.id_member +'/edit')
            .then(function (respons) {
                $scope.member = respons.data.data;

                //panggil setelah data ada
                myHelp.getParam('/member/admin/members/create',{})
                    .then(function (respons) {
                        $scope.master = respons.data.data;
                    });

            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.member);
            myHelp.putParam('/member/admin/members/' + $scope.member.id_member, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.member.management.detail', function truncateCtrl($scope, $state, $stateParams,$http, myHelp) {

        $scope.getMemberDetail = function()
        {
            myHelp.getDetail('/member/admin/members/' + $stateParams.id_member)
            .then(function (respons) {
                $scope.member = respons.data.data;
            });
        };
        $scope.getMemberDetail();

        if($state.current.name === 'app.member.management.detail')
        {
            $state.go("app.member.management.detail.packet",{id_member:$stateParams.id_member},{});
        }


        $scope.changeThum = function(type) {
            var file = $scope.file_thum;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.member.id_member);
            fd.append('type', type);
            $http.post(BASE_URL + '/member/admin/member_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getMemberDetail();
                    $scope.file_thum = false;
                }, function (err) {
                    errorView()
                })

        };
        // Ganti foto cover
        $scope.changeCover = function(type) {
            var file = $scope.file_cover;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.member.id_member);
            fd.append('type', type);
            $http.post(BASE_URL + '/member/admin/member_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getMemberDetail();
                    $scope.file_cover = false;
                }, function (err) {
                    errorView()
                })

        };



    })

    .controller('app.member.management.packet', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {

        $scope.getContent = function()
        {
            myHelp.getParam('/packet/admin/my_packets',{id_member:$stateParams.id_member })
                .then(function (respons) {
                    $scope.packets = respons.data.data;
                });
        }
        $scope.getContent();

         
    })

/*-------------------------------------------------------------------------------------------*/
/*                  TEAM
/*-------------------------------------------------------------------------------------------*/



