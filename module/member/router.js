function configengineer($stateProvider) {

    $stateProvider
        .state('app.member', {
            url: "/member",
            abstract:true,
            data: {pageTitle: 'member'}
        })
        .state('app.member.report', {
            url: "/report",
            templateUrl: "module/member/views/report/data.html",
            data: {pageTitle: 'member'},
            controller: "app.member.report"
        })


        .state('app.member.management', {
            url: "/management/:status_member/:id_company",
            templateUrl: "module/member/views/management/data.html",
            data: {pageTitle: 'member'},
            controller: "app.member.management"
        })
        .state('app.member.management.add', {
            url: "/add",
            templateUrl: "module/member/views/management/add.html",
            data: {pageTitle: 'member'},
            controller: "app.member.management.add"
        })
        .state('app.member.management.edit', {
            url: "/edit/:id_member",
            templateUrl: "module/member/views/management/edit.html",
            data: {pageTitle: 'member'},
            controller: "app.member.management.edit"
        })
        .state('app.member.management.detail', {
            url: "/detail/:id_member",
            templateUrl: "module/member/views/management/detail.html",
            data: {pageTitle: 'member'},
            controller: "app.member.management.detail"
        })
        .state('app.member.management.detail.packet', {
            url: "/packet",
            templateUrl: "module/member/views/packet/data.html",
            data: {pageTitle: 'packet'},
            controller: "app.member.management.packet"
        })

}

angular
    .module('inspinia')
    .config(configengineer)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });