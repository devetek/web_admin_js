function configengineer($stateProvider) {

    $stateProvider
        .state('app.purchase', {
            url: "/purchase",
            abstract:true,
            data: {pageTitle: 'purchase'}
        })
        .state('app.purchase.management', {
            url: "/management",
            templateUrl: "module/purchase/views/management/data.html",
            data: {pageTitle: 'purchase'},
            controller: "app.purchase.management"
        })
        .state('app.purchase.management.add', {
            url: "/add",
            templateUrl: "module/purchase/views/management/add.html",
            data: {pageTitle: 'purchase'},
            controller: "app.purchase.management.add"
        })
        .state('app.purchase.management.edit', {
            url: "/edit/:id_purchase",
            templateUrl: "module/purchase/views/management/edit.html",
            data: {pageTitle: 'purchase'},
            controller: "app.purchase.management.edit"
        })
        .state('app.purchase.management.detail', {
            url: "/detail/:id_purchase",
            templateUrl: "module/purchase/views/management/detail.html",
            data: {pageTitle: 'purchase'},
            controller: "app.purchase.management.detail"
        })
        .state('app.purchase.management.detail.book', {
            url: "/book",
            templateUrl: "module/purchase/views/management/detail.book.html",
            data: {pageTitle: 'purchase'},
            controller: "app.purchase.management.detail.book"
        })
}

angular
    .module('inspinia')
    .config(configengineer)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });