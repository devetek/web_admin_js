
angular
    .module('inspinia')

    .controller('app.purchase.management', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            $scope.loadData = false;
            myHelp.getDetail('/purchase/admin/purchases')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();

        $scope.delete = function (id) {
            myHelp.deleteParam('/master/purchase/purchase/' + id, {})
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchase.management", {}, {reload: true})
                    }
                    , function myError(respon) {
                        errorView(respon.data.status);
                    });
        }
    })

    .controller('app.purchase.management.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.purchase = {};

        myHelp.getParam('/purchase/admin/purchase/create',{})
            .then(function (respons) {
                $scope.master = respons.data.data;
            });

        myHelp.postParam('/purchase/admin/purchases',{})
            .then(function (respons) {
                $scope.purchase = respons.data.data;
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.purchase);
            myHelp.putParam('/purchase/admin/purchase/' + $scope.purchase.id_purchase, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.purchase.management.detail', function truncateCtrl($scope, $state, $stateParams,$http, myHelp) {

        $scope.getpurchaseDetail = function()
        {
            myHelp.getDetail('/purchase/admin/purchases/' + $stateParams.id_purchase)
            .then(function (respons) {
                $scope.purchase = respons.data.data;
            });
        };
        $scope.getpurchaseDetail();
        $scope.submitForm = function () {
            var Param = clearObj($scope.purchase);
            myHelp.putParam('/purchase/admin/purchases/' + $scope.purchase.id_purchase, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })






