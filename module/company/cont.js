
angular
    .module('inspinia')

    .controller('app.company.management', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/company/admin/companies')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();

        $scope.delete = function (id) {
            myHelp.deleteParam('/master/company/company/' + id, {})
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.company.management", {}, {reload: true})
                    }
                    , function myError(respon) {
                        errorView(respon.data.status);
                    });
        }
    })

    .controller('app.company.management.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.company = {};

        myHelp.getParam('/company/admin/company/create',{})
            .then(function (respons) {
                $scope.master = respons.data.data;
            });

        myHelp.postParam('/company/admin/companies',{})
            .then(function (respons) {
                $scope.company = respons.data.data;
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.company);
            myHelp.putParam('/company/admin/company/' + $scope.company.id_company, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.company.management.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/company/admin/company/' + $stateParams.id_company +'/edit')
            .then(function (respons) {
                $scope.company = respons.data.data;

                //panggil setelah data ada
                myHelp.getParam('/company/admin/company/create',{})
                    .then(function (respons) {
                        $scope.master = respons.data.data;
                    });

            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.company);
            myHelp.putParam('/company/admin/company/' + $scope.company.id_company, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.company.management.detail', function truncateCtrl($scope, $state, $stateParams,$http, myHelp) {

        $scope.getcompanyDetail = function()
        {
            myHelp.getDetail('/company/admin/company/' + $stateParams.id_company)
            .then(function (respons) {
                $scope.company = respons.data.data;
            });
        };
        $scope.getcompanyDetail();

        if($state.current.name === 'app.company.management.detail')
        {
            $state.go("app.company.management.detail.book",{id_company:$stateParams.id_company},{});
        }


        $scope.changeThum = function(type) {
            var file = $scope.file_thum;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.company.id_company);
            fd.append('type', type);
            $http.post(BASE_URL + '/company/admin/company_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getcompanyDetail();
                    $scope.file_thum = false;
                }, function (err) {
                    errorView()
                })

        };
        // Ganti foto cover
        $scope.changeCover = function(type) {
            var file = $scope.file_cover;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.company.id_company);
            fd.append('type', type);
            $http.post(BASE_URL + '/company/admin/company_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getcompanyDetail();
                    $scope.file_cover = false;
                }, function (err) {
                    errorView()
                })

        };



    })

    .controller('app.company.management.detail.book', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {

        $scope.getContent = function()
        {
            myHelp.getParam('/book/admin/books',{id_company:$stateParams.id_company})
                .then(function (respons) {
                    $scope.books = respons.data.data;
                });
        }
        $scope.getContent();

        $scope.deleteSection = function(id) {
            deleteParam($http,$state,'/book/admin/bookSections/' + id, {}, 'app.book.management.detail.content',{id_book:$stateParams.id_book});
        }
        $scope.deletContent = function(id) {
            deleteParam($http,$state,'/book/admin/bookContents/' + id, {}, 'app.book.management.detail.content',{id_book:$stateParams.id_book});
            $scope.getContent();
        }
    })




