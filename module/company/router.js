function configengineer($stateProvider) {

    $stateProvider
        .state('app.company', {
            url: "/company",
            abstract:true,
            data: {pageTitle: 'company'}
        })
        .state('app.company.management', {
            url: "/management",
            templateUrl: "module/company/views/management/data.html",
            data: {pageTitle: 'company'},
            controller: "app.company.management"
        })
        .state('app.company.management.add', {
            url: "/add",
            templateUrl: "module/company/views/management/add.html",
            data: {pageTitle: 'company'},
            controller: "app.company.management.add"
        })
        .state('app.company.management.edit', {
            url: "/edit/:id_company",
            templateUrl: "module/company/views/management/edit.html",
            data: {pageTitle: 'company'},
            controller: "app.company.management.edit"
        })
        .state('app.company.management.detail', {
            url: "/detail/:id_company",
            templateUrl: "module/company/views/management/detail.html",
            data: {pageTitle: 'company'},
            controller: "app.company.management.detail"
        })
        .state('app.company.management.detail.book', {
            url: "/book",
            templateUrl: "module/company/views/management/detail.book.html",
            data: {pageTitle: 'company'},
            controller: "app.company.management.detail.book"
        })
}

angular
    .module('inspinia')
    .config(configengineer)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });