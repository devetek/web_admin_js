function configengineer($stateProvider) {

    $stateProvider
        .state('app.publisher', {
            url: "/publisher",
            abstract:true,
            data: {pageTitle: 'publisher'}
        })
        .state('app.publisher.management', {
            url: "/management",
            templateUrl: "module/publisher/views/management/data.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.management"
        })
        .state('app.publisher.management.add', {
            url: "/add",
            templateUrl: "module/publisher/views/management/add.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.management.add"
        })
        .state('app.publisher.management.edit', {
            url: "/edit/:id_publisher",
            templateUrl: "module/publisher/views/management/edit.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.management.edit"
        })
        .state('app.publisher.management.detail', {
            url: "/detail/:id_publisher",
            templateUrl: "module/publisher/views/management/detail.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.management.detail"
        })
        .state('app.publisher.management.detail.book', {
            url: "/book",
            templateUrl: "module/publisher/views/management/detail.book.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.management.detail.book"
        })


        .state('app.publisher.team', {
            url: "/team",
            templateUrl: "module/publisher/views/team/data.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.team"
        })
        .state('app.publisher.team.add', {
            url: "/add",
            templateUrl: "module/publisher/views/team/add.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.team.add"
        })
        .state('app.publisher.team.edit', {
            url: "/edit/:id_publisher_team",
            templateUrl: "module/publisher/views/team/edit.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.team.edit"
        })
        .state('app.publisher.team.detail', {
            url: "/detail/:id_publisher_team",
            templateUrl: "module/publisher/views/team/detail.html",
            data: {pageTitle: 'publisher'},
            controller: "app.publisher.team.detail"
        })
}

angular
    .module('inspinia')
    .config(configengineer)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });