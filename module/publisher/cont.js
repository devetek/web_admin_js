
angular
    .module('inspinia')

    .controller('app.publisher.management', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/publisher/admin/publisher')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();

        $scope.delete = function (id) {
            myHelp.deleteParam('/master/publisher/publisher/' + id, {})
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.publisher.management", {}, {reload: true})
                    }
                    , function myError(respon) {
                        errorView(respon.data.status);
                    });
        }
    })

    .controller('app.publisher.management.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.publisher = {};

        myHelp.getParam('/publisher/admin/publisher/create',{})
            .then(function (respons) {
                $scope.master = respons.data.data;
            });

        myHelp.postParam('/publisher/admin/publisher',{})
            .then(function (respons) {
                $scope.publisher = respons.data.data;
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.publisher);
            myHelp.putParam('/publisher/admin/publisher/' + $scope.publisher.id_publisher, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.publisher.management.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/publisher/admin/publisher/' + $stateParams.id_publisher +'/edit')
            .then(function (respons) {
                $scope.publisher = respons.data.data;

                //panggil setelah data ada
                myHelp.getParam('/publisher/admin/publisher/create',{})
                    .then(function (respons) {
                        $scope.master = respons.data.data;
                    });

            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.publisher);
            myHelp.putParam('/publisher/admin/publisher/' + $scope.publisher.id_publisher, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.publisher.management.detail', function truncateCtrl($scope, $state, $stateParams,$http, myHelp) {

        $scope.getPublisherDetail = function()
        {
            myHelp.getDetail('/publisher/admin/publisher/' + $stateParams.id_publisher)
            .then(function (respons) {
                $scope.publisher = respons.data.data;
            });
        };
        $scope.getPublisherDetail();

        if($state.current.name === 'app.publisher.management.detail')
        {
            $state.go("app.publisher.management.detail.book",{id_publisher:$stateParams.id_publisher},{});
        }


        $scope.changeThum = function(type) {
            var file = $scope.file_thum;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.publisher.id_publisher);
            fd.append('type', type);
            $http.post(BASE_URL + '/publisher/admin/publisher_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getPublisherDetail();
                    $scope.file_thum = false;
                }, function (err) {
                    errorView()
                })

        };
        // Ganti foto cover
        $scope.changeCover = function(type) {
            var file = $scope.file_cover;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.publisher.id_publisher);
            fd.append('type', type);
            $http.post(BASE_URL + '/publisher/admin/publisher_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function(){
                    berhasilView();
                    $scope.getPublisherDetail();
                    $scope.file_cover = false;
                }, function (err) {
                    errorView()
                })

        };



    })

    .controller('app.publisher.management.detail.book', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {

        $scope.getContent = function()
        {
            myHelp.getParam('/book/admin/books',{id_publisher:$stateParams.id_publisher})
                .then(function (respons) {
                    $scope.books = respons.data.data;
                });
        }
        $scope.getContent();

        $scope.deleteSection = function(id) {
            deleteParam($http,$state,'/book/admin/bookSections/' + id, {}, 'app.book.management.detail.content',{id_book:$stateParams.id_book});
        }
        $scope.deletContent = function(id) {
            deleteParam($http,$state,'/book/admin/bookContents/' + id, {}, 'app.book.management.detail.content',{id_book:$stateParams.id_book});
            $scope.getContent();
        }
    })

/*-------------------------------------------------------------------------------------------*/
/*                  TEAM
/*-------------------------------------------------------------------------------------------*/

    .controller('app.publisher.team', function truncateCtrl($scope,$http, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/publisher/admin/publisher_teams')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();

        $scope.delete = function (id) {
            deleteParam($http, $state, '/publisher/admin/publisher_teams/' + id, {}, 'app.publisher.team', {});
        }
    })

    .controller('app.publisher.team.add', function truncateCtrl($scope, $state,$timeout, $stateParams, myHelp) {
        $scope.team = {};
        $scope.id_publisher = [];
        $scope.publishers = [];

        myHelp.postParam('/publisher/admin/publisher_teams',{})
            .then(function (respons) {
                $scope.team = respons.data.data;
            });

        myHelp.getParam('/company/admin/companies',{})
            .then(function (respons) {
                $scope.companies = respons.data.data;
            });

        $scope.getPublisher = function()
        {
            $timeout(function () {
                myHelp.getParam('/publisher/admin/publisher',{id_company:clearInt($scope.team.for_company)})
                    .then(function (respons) {
                        $scope.publishers = respons.data.data;
                    });
            }, 200)
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.team);
            myHelp.putParam('/publisher/admin/publisher_teams/' + $scope.team.id_publisher_team, Param)
                .then(function mySuccesresponse() {
                        berhasilView();

                        myHelp.postParam('/publisher/admin/publisher_team_members',{id_publisher_team:$scope.team.id_publisher_team, id_publisher:$scope.id_publisher})
                            .then(function (respons) {
                                $state.go("^", {}, {reload: true})
                            });

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.publisher.team.edit', function truncateCtrl($scope,$timeout, $state, $stateParams, myHelp) {

        $scope.team = {};
        $scope.id_publisher = [];
        $scope.publishers = [];

        myHelp.getDetail('/publisher/admin/publisher_teams/' + $stateParams.id_publisher_team )
            .then(function (respons) {
                $scope.team = respons.data.data;

                myHelp.getParam('/company/admin/companies',{})
                    .then(function (respons) {
                        $scope.companies = respons.data.data;
                    });
                $scope.getPublisher();

                myHelp.getParam('/publisher/admin/publisher_team_members',{id_publisher_team:$stateParams.id_publisher_team})
                    .then(function (respons) {
                        for(let i=0; i < respons.data.data.length; i++)
                        {
                            $scope.id_publisher.push(respons.data.data[i].id_publisher);
                        }
                    });

            });



        $scope.getPublisher = function()
        {
            $timeout(function () {
                myHelp.getParam('/publisher/admin/publisher',{id_company:clearInt($scope.team.for_company)})
                    .then(function (respons) {
                        $scope.publishers = respons.data.data;
                    });
            }, 200)
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.team);
            myHelp.putParam('/publisher/admin/publisher_teams/' + $scope.team.id_publisher_team, Param)
                .then(function mySuccesresponse() {
                        berhasilView();

                        myHelp.postParam('/publisher/admin/publisher_team_members',{id_publisher_team:$scope.team.id_publisher_team, id_publisher:$scope.id_publisher})
                            .then(function (respons) {
                                $state.go("^", {}, {reload: true})
                            });

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.publisher.team.detail', function truncateCtrl($scope, $state, $stateParams,$http, myHelp) {

        $scope.getTeamDetail = function()
        {
            myHelp.getDetail('/publisher/admin/publisher_teams/' + $stateParams.id_team)
                .then(function (respons) {
                    $scope.team = respons.data.data;
                });
        };
        $scope.getTeamDetail();


    })


