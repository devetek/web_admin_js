function configengineer($stateProvider) {

    $stateProvider
        .state('app.packet', {
            url: "/packet",
            abstract:true,
            data: {pageTitle: 'packet'}
        })
        .state('app.packet.management', {
            url: "/management",
            templateUrl: "module/packet/views/management/data.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.management"
        })
        .state('app.packet.management.add', {
            url: "/add",
            templateUrl: "module/packet/views/management/add.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.management.add"
        })
        .state('app.packet.management.edit', {
            url: "/edit/:id_packet",
            templateUrl: "module/packet/views/management/edit.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.management.edit"
        })
        .state('app.packet.management.detail', {
            url: "/detail/:id_packet",
            templateUrl: "module/packet/views/management/detail.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.management.detail"
        })
        .state('app.packet.management.detail.add_info', {
            url: "/add_info",
            templateUrl: "module/packet/views/info/add.html",
            data: {pageTitle: 'add_info'},
            controller: "app.packet.management.detail.add_info"
        })
        .state('app.packet.management.detail.add_goal', {
            url: "/add_goal",
            templateUrl: "module/packet/views/goal/add.html",
            data: {pageTitle: 'add_goal'},
            controller: "app.packet.management.detail.add_goal"
        })
        .state('app.packet.management.detail.add_requirement', {
            url: "/add_requirement",
            templateUrl: "module/packet/views/requirement/add.html",
            data: {pageTitle: 'add_requirementl'},
            controller: "app.packet.management.detail.add_requirement"
        })

        .state('app.packet.management.detail.book', {
            url: "/book",
            templateUrl: "module/packet/views/management/detail.book.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.management.detail.book"
        })

        .state('app.packet.management.detail.book_add', {
            url: "/book_add/:id_company",
            templateUrl: "module/packet/views/management/detail.book_add.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.management.detail.book_add"
        })


        // Packet category-----------------------------------------------------------------
        .state('app.packet.management.detail.category', {
            url: "/category",
            templateUrl: "module/packet/views/packet_category/data.html",
            data: {pageTitle: 'category'},
            controller: "app.packet.management.detail.category"
        })

        // Income-----------------------------------------------------------------
        .state('app.packet.management.detail.income', {
            url: "/income",
            templateUrl: "module/packet/views/income/data.html",
            data: {pageTitle: 'income'},
            controller: "app.packet.management.detail.income"
        })

        // Sale-----------------------------------------------------------------
        .state('app.packet.management.detail.sale', {
            url: "/sale",
            templateUrl: "module/packet/views/sale/data.html",
            data: {pageTitle: 'sale'},
            controller: "app.packet.management.detail.sale"
        })
        .state('app.packet.management.detail.sale.add', {
            url: "/add",
            templateUrl: "module/packet/views/sale/add.html",
            data: {pageTitle: 'sale'},
            controller: "app.packet.management.detail.sale.add"
        })

        // CATEGORI-----------------------------------------------------------------
        .state('app.packet.category', {
            url: "/category",
            templateUrl: "module/packet/views/category/data.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.category"
        })
        .state('app.packet.category.add', {
            url: "/add",
            templateUrl: "module/packet/views/category/add.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.category.add"
        })
        .state('app.packet.category.edit', {
            url: "/edit/:id_packet_master_category",
            templateUrl: "module/packet/views/category/edit.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.category.edit"
        })
        .state('app.packet.category.detail', {
            url: "/detail/:id_packet_master_category",
            templateUrl: "module/packet/views/category/detail.html",
            data: {pageTitle: 'packet'},
            controller: "app.packet.category.detail"
        })
}

angular
    .module('inspinia')
    .config(configengineer)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });