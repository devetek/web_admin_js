angular
    .module('inspinia')

    .controller('app.packet.management', function truncateCtrl($scope, $http, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/packet/admin/packets')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();
        $scope.delete = function (id) {
            deleteParam($http, $state, '/packet/admin/packets/' + id, {}, 'app.packet.management', {});
        }

    })

    .controller('app.packet.management.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.packet = {};

        myHelp.getParam('/company/admin/companies', {})
            .then(function (respons) {
                $scope.companies = respons.data.data;
            });

        myHelp.postParam('/packet/admin/packets', {})
            .then(function (respons) {
                $scope.packet = respons.data.data;
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.packet);
            myHelp.putParam('/packet/admin/packets/' + $scope.packet.id_packet, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.packet.management.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/packet/admin/packets/' + $stateParams.id_packet + '/edit')
            .then(function (respons) {
                $scope.packet = respons.data.data;
                myHelp.getParam('/company/admin/companies', {})
                    .then(function (respons) {
                        $scope.companies = respons.data.data;
                    });
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.packet);
            myHelp.putParam('/packet/admin/packets/' + $scope.packet.id_packet, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.packet.management.detail', function truncateCtrl($scope, $state, $stateParams, $http, myHelp) {

        $scope.getPacketDetail = function () {
            myHelp.getDetail('/packet/admin/packets/' + $stateParams.id_packet)
                .then(function (respons) {
                    $scope.packet = respons.data.data.packet;
                    $scope.widget_active = respons.data.data.active;
                    $scope.widget_total = respons.data.data.total;
                    $scope.widget_ticket = respons.data.data.ticket;
                    $scope.getInfoDetail();
                    $scope.getGoalDetail();
                    $scope.getRequirementDetail();
                });

        };
        $scope.getPacketDetail();


        $scope.getInfoDetail = function () {
            myHelp.getParam('/packet/admin/packet_details', {id_packet: $stateParams.id_packet})
                .then(function (respons) {
                    $scope.detail_infos = respons.data.data;
                });
        }

        $scope.deleteInfo = function (id) {
            deleteParamStandar($http, $state, '/packet/admin/packet_details/' + id, {}, function () {
                $scope.getInfoDetail();
            });
        }

        // GOAL
        $scope.getGoalDetail = function () {
            myHelp.getParam('/packet/admin/packet_goals', {id_packet: $stateParams.id_packet})
                .then(function (respons) {
                    $scope.detail_goals = respons.data.data;
                });
        }

        $scope.deleteGoal = function (id) {
            deleteParamStandar($http, $state, '/packet/admin/packet_goals/' + id, {}, function () {
                $scope.getGoalDetail();
            });
        }
        // REQUIREMENT
        $scope.getRequirementDetail = function () {
            myHelp.getParam('/packet/admin/packet_requirements', {id_packet: $stateParams.id_packet})
                .then(function (respons) {
                    $scope.detail_requirements = respons.data.data;
                });
        }

        $scope.deleteRequirement = function (id) {
            deleteParamStandar($http, $state, '/packet/admin/packet_requirements/' + id, {}, function () {
                $scope.getRequirementDetail();
            });
        }


        if ($state.current.name === 'app.packet.management.detail') {
            $state.go("app.packet.management.detail.book", {id_packet: $stateParams.id_packet}, {});
        }


        $scope.changeThum = function (type) {
            var file = $scope.file_thum;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.packet.id_packet);
            fd.append('type', type);
            $http.post(BASE_URL + '/packet/admin/packet_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function () {
                    berhasilView();
                    $scope.getPacketDetail();
                    $scope.file_thum = false;
                }, function (err) {
                    errorView()
                })

        };
        // Ganti foto cover
        $scope.changeCover = function (type) {
            var file = $scope.file_cover;
            var fd = new FormData();
            fd.append('file', file);
            fd.append('id', $scope.packet.id_packet);
            fd.append('type', type);
            $http.post(BASE_URL + '/packet/admin/packet_covers', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function () {
                    berhasilView();
                    $scope.getPacketDetail();
                    $scope.file_cover = false;
                }, function (err) {
                    errorView()
                })

        };

    })

    .controller('app.packet.management.detail.add_info', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.submitForm = function () {
            $scope.info_detail.id_packet = $stateParams.id_packet;
            var Param = clearObj($scope.info_detail);
            myHelp.postParam('/packet/admin/packet_details', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $scope.getInfoDetail();
                        $state.go("^", {}, {reload: false})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.packet.management.detail.add_goal', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.submitForm = function () {
            $scope.goal_detail.id_packet = $stateParams.id_packet;
            var Param = clearObj($scope.goal_detail);
            myHelp.postParam('/packet/admin/packet_goals', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $scope.getGoalDetail();
                        $state.go("^", {}, {reload: false})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.packet.management.detail.add_requirement', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.submitForm = function () {
            $scope.requirement_detail.id_packet = $stateParams.id_packet;
            var Param = clearObj($scope.requirement_detail);
            myHelp.postParam('/packet/admin/packet_requirements', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $scope.getRequirementDetail();
                        $state.go("^", {}, {reload: false})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.packet.management.detail.book', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {

        $scope.getContent = function () {
            myHelp.getParam('/packet/admin/packet_books', {id_packet: $stateParams.id_packet})
                .then(function (respons) {
                    $scope.books = respons.data.data;
                });
        }
        $scope.getContent();

        $scope.deleteSection = function (id) {
            deleteParam($http, $state, '/book/admin/bookSections/' + id, {}, 'app.book.management.detail.content', {id_book: $stateParams.id_book});
        }
        $scope.deletContent = function (id) {
            deleteParam($http, $state, '/book/admin/bookContents/' + id, {}, 'app.book.management.detail.content', {id_book: $stateParams.id_book});
            $scope.getContent();
        }
    })

    .controller('app.packet.management.detail.book_add', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {

        $scope.id_book = [];
        $scope.book = {};

        myHelp.getDetail('/packet/admin/packet_books/create?id_company=' + $stateParams.id_company)
            .then(function (respons) {
                $scope.books = respons.data.data;
            });


        $scope.addBookToPacket = function () {
            $scope.book.id_packet = $stateParams.id_packet;
            $scope.book.id_book = $scope.id_book;
            var Param = clearObj($scope.book);
            myHelp.postParam('/packet/admin/packet_books', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        // return false;
                        $state.go("^", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    /*-------------------------------------------------------------------------------------------*/
    /*                  INCOME
    /*-------------------------------------------------------------------------------------------*/

    .controller('app.packet.management.detail.income', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {

        $scope.getIncome = function () {
            myHelp.getParam('/packet/admin/packet_incomes', {id_packet: $stateParams.id_packet})
                .then(function (respons) {
                    $scope.incomes = respons.data.data.data;
                    $scope.income_tahun = respons.data.data.tahun;
                });
        }
        $scope.getIncome();
        $('body').addClass('sidebar-hidden');

    })

    /*-------------------------------------------------------------------------------------------*/
    /*                  SALE
    /*-------------------------------------------------------------------------------------------*/

    .controller('app.packet.management.detail.sale', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {

        $scope.getContent = function () {
            myHelp.getParam('/packet/admin/packet_sales', {id_packet: $stateParams.id_packet})
                .then(function (respons) {
                    $scope.packet_saless = respons.data.data;
                });
        }
        $scope.getContent();

        $scope.deleteSale = function (id) {
            deleteParam($http, $state, '/packet/admin/packet_sales/' + id, {}, 'app.book.management.detail.sale', {id_book: $stateParams.id_book});
        }

    })
    .controller('app.packet.management.detail.sale.add', function truncateCtrl($scope, $state, $timeout, $stateParams, myHelp) {
        $scope.sale = {};

        myHelp.postParam('/packet/admin/packet_sales', {})
            .then(function (respons) {
                $scope.sale = respons.data.data;
            });


        $scope.submitForm = function () {
            $scope.sale.id_packet = $stateParams.id_packet;
            var Param = clearObj($scope.sale);
            myHelp.putParam('/packet/admin/packet_sales/' + $scope.sale.id_packet_sale, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })


    /*-------------------------------------------------------------------------------------------*/
    /*                  Packet_category
    /*-------------------------------------------------------------------------------------------*/

    .controller('app.packet.management.detail.category', function truncateCtrl($scope, $state, $stateParams, myHelp, $timeout, $http) {

        $scope.categorys = [];
        $scope.getContent = function () {
            myHelp.getParam('/packet/admin/packet_categories', {id_packet: $stateParams.id_packet})
                .then(function (respons) {
                    $scope.packet_categories = respons.data.data;
                });
        }
        $scope.getContent();

        $scope.getCategory = function () {
            myHelp.getDetail('/packet/admin/packet_categories/'+ $stateParams.id_packet)
                .then(function (respons) {
                    $scope.categorys = respons.data.data;
                });
        }
        $scope.getCategory();


        $scope.changeCategory = function () {
            $timeout(function () {
                myHelp.postParam('/packet/admin/packet_categories', {
                    id_packet: $stateParams.id_packet,
                    param: $scope.categorys
                })
                    .then(function mySuccesresponse() {
                            $scope.getCategory();
                        }
                        , function myError() {
                            errorView("Error data entry (400)");
                        });
            }, 200)
        }


    })

    /*-------------------------------------------------------------------------------------------*/
    /*                  CATEGORY
    /*-------------------------------------------------------------------------------------------*/

    .controller('app.packet.category', function truncateCtrl($scope, $http, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/packet/admin/packetMasterCategories')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();

        $scope.delete = function (id) {
            deleteParam($http, $state, '/packet/admin/packetMasterCategories/' + id, {}, 'app.packet.category', {});
        }
    })

    .controller('app.packet.category.add', function truncateCtrl($scope, $state, $timeout, $stateParams, myHelp) {
        $scope.category = {};

        myHelp.postParam('/packet/admin/packetMasterCategories', {})
            .then(function (respons) {
                $scope.category = respons.data.data;
            });


        $scope.submitForm = function () {
            var Param = clearObj($scope.category);
            myHelp.putParam('/packet/admin/packetMasterCategories/' + $scope.category.id_packet_master_category, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.packet.category.edit', function truncateCtrl($scope, $timeout, $state, $stateParams, myHelp) {

        $scope.category = {};

        myHelp.getDetail('/packet/admin/packetMasterCategories/' + $stateParams.id_packet_master_category)
            .then(function (respons) {
                $scope.category = respons.data.data;

            });


        $scope.submitForm = function () {
            var Param = clearObj($scope.category);
            myHelp.putParam('/packet/admin/packetMasterCategories/' + $scope.category.id_packet_master_category, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })
    .controller('app.packet.category.detail', function truncateCtrl($scope, $state, $stateParams, $http, myHelp) {

        $scope.getCategoryDetail = function () {
            myHelp.getDetail('/packet/admin/packetMasterCategories/' + $stateParams.id_category)
                .then(function (respons) {
                    $scope.category = respons.data.data;
                });
        };
        $scope.getCategoryDetail();


    })


