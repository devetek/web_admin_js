/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/app/dashboard");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('login', {
            url: "/login",
            templateUrl: "views/login.html",
            controller: 'loginCont'
        })
        .state('app', {
            url: "/app",
            abstract: true,
            // controller: "app",
            templateUrl: "views/common/content.html",
            resolve: {
                loadMyService: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([

                    ])
                }
            }

        })
        .state('app.dashboard', {
            url: "/dashboard",
            templateUrl: "views/app/dashboard.html",
            data: {pageTitle: 'dashboard'}
        })

    /*-------------------------------------------------------------------
                                    Maintenance Code
    -------------------------------------------------------------------*/

}

angular
    .module('inspinia')
    .config(config)
    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
    });
