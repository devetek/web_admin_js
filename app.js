/**
 * INSPINIA - Responsive Admin Theme
 *
 */
var ip_domin = '127.0.0.1';
var server = 'http://'+ ip_domin + ':8000/';

// var server = 'http://dev.amims.derazona.com/site/public/'; // -> ini alamat server lama (backend)
var server = 'http://apidev.hompes.id/'; // -> ini alamat server baru


var BASE_URL = server + 'api/v1';
var SITE_URL = server + '';

(function () {

    angular.module('inspinia', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                    // ngSanitize
        'ngSanitize',
        'angular-jwt',
        'helper',
        'ngStorage',
        'checklist-model',
        'ui.utils.masks',
        'angular-loading-bar',
        'ngJsTree',
        'myDirectiv',
        'ui.timepicker',
        'ui.tinymce'
    ])
        .config(function Config($httpProvider, jwtOptionsProvider) {
            jwtOptionsProvider.config({

                tokenGetter: ['$localStorage', function($localStorage) {
                    return $localStorage.token;
                }],

                whiteListedDomains: [ip_domin,'apidev.hompes.id'],
                unauthenticatedRedirector: ['$state', function($state) {

                    $state.go('login');
                }]
            });
            $httpProvider.interceptors.push('jwtInterceptor');

        })
        .run(function (authManager, $rootScope,myHelp,$state,$window) {

            $rootScope.cekLogin = function () {
                myHelp.getDetail('/user/cek').then(
                    function (response) {
                        if (response.data == 'false' && $state.current.name !== 'login') {
                            $state.go('login');
                        } else {
                            $rootScope.curren_user = response.data;
                        }
                    }, function (err) {
                        if ($state.current.name !== 'login') {
                            $state.go('login');
                        }
                    });
            };
            $rootScope.cekLogin();;

            authManager.checkAuthOnRefresh();

            $rootScope.$on('tokenHasExpired', function () {
                if($state.current.name !== 'login')
                {
                    $state.go('login');
                }
            });
            $rootScope.SITE_URL = function () {
                return SITE_URL;
            };

            authManager.redirectWhenUnauthenticated();

            $rootScope.is_run = false;

            //togle filter
            $rootScope.toglefilter = false;
            $rootScope.actionFilter = function () {
                if ($rootScope.toglefilter == false) {
                    $rootScope.toglefilter = true;
                }
                else {
                    $rootScope.toglefilter = false;
                }
            };

            $rootScope.report_url = function(file,id) {
                return "http://178.128.218.190:9000/?report=/var/www/html/REPORT/"+ file +"&init=pdf"+id;
            };
            $rootScope.pdf_out = function (file, param) {
                console.log(param);
                var url = $rootScope.report_url('release/' + file ,'&' + Object_toReport(param));
                $window.open(url);
            }

            $rootScope.menu_check = function (id_modul,action) {
                // if($localStorage.curren_user.id_group =='G000000001')
                // {
                //     return true;
                // }
                // var cek = $localStorage.akses.filter(function(obj) {
                //     return (obj.id_modul===id_modul && obj.action===action)
                // });
                //
                // if(cek.length >0 )
                // {
                //     return true;
                // }
                // return false;
                return true;

            }

            $rootScope.actionFilter = function()
            {
                if($rootScope.toglefilter == false)
                {
                    $rootScope.toglefilter = true;
                }
                else
                {
                    $rootScope.toglefilter = false;
                }
            }
            $rootScope.fooTablePerpage = 10;


            /*----------------------------------------------------------------*/
                            /*SEARCH*/
            $rootScope.param = {};

                            /*SEARCH*/
            /*----------------------------------------------------------------*/

        })

        .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
            cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
            cfpLoadingBarProvider.spinnerTemplate = '<div class="sweet-overlay" tabindex="-1" style="opacity: 1.06; display: block;"></div><div>' +
                'Loadin' +
                '</div>';
        }])

})();

