/**
 * INSPINIA - Responsive Admin Theme
 *
 */

angular
    .module('inspinia')


    /*----------------------------------------------------------------------*/
    /*                      PURCHASING
    /*----------------------------------------------------------------------*/


    /*----------------------------------------------------------------------------------------------
     PR
     /*----------------------------------------------------------------------------------------------*/

    .controller('app.purchasing.pr', function truncateCtrl($scope, $state, $stateParams, myHelp, $rootScope, $window, $http) {
        $scope.param = {};

        $scope.filters = {};
        $scope.status_pr = ['draft', 'confirm', 'open', 'close'];
        $scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $scope.years = [];
        $scope.BASE_URL = BASE_URL;
        $scope.id_mpc_piro_item = [];

        var date = new Date();

        for (var a = 0; a < 5; a++) {
            $scope.years.push(date.getFullYear() - a);
        }

        $scope.filter = function (page) {
            $scope.loadData = false;
            $scope.filters.page = page;
            myHelp.getParam('/store/pr', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.aircraft = respons.data.aircraft;
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);

        $scope.delete = function (id) {
            deleteParam($http, $state, '/store/pr/' + id, {}, $state.current.name, {});
        }

        $scope.export_report = function (id) {
            var url = $rootScope.report_url('release/PR.RPT', '&' + Object_toReport({id: id}));
            $window.open(url);
        }

    })

    .controller('app.purchasing.piro_item', function truncateCtrl($scope, $state, $stateParams, myHelp, $window, $rootElement) {
        $scope.param = {};

        $scope.filters = {};
        $scope.status_item = ['pro', 'pr'];
        $scope.priority = ['Normal', 'Urgent', 'AOG'];
        $scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $scope.years = [];
        $scope.BASE_URL = BASE_URL;
        $scope.id_mpc_piro_item = [];

        var date = new Date();

        for (var a = 0; a < 5; a++) {
            $scope.years.push(date.getFullYear() - a);
        }

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/store/piro_item', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.aircraft = respons.data.aircraft;
                    $scope.datas = respons.data.data;
                });
        }
        $scope.filter(1);


    })

    .controller('app.purchasing.pr.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {

        $scope.loadData = false;
        myHelp.getDetail('/store/pr/' + $stateParams.id_pur_pr)
            .then(function (respons) {
                $scope.pr = respons.data.data;
                $scope.items = respons.data.item;
                $scope.loadData = true;
            });

        $scope.BASE_URL = BASE_URL;
        $scope.acc = function (acc) {
            PindahParam($http, $state, '/store/pr/' + acc, {id_pur_pr: $stateParams.id_pur_pr}, 'app.purchasing.pr.detail', {id_pur_pr: $stateParams.id_pur_pr}, acc);
        }

        $scope.delete = function () {
            deleteParam($http, $state, '/store/pr/' + $stateParams.id_pur_pr, {}, '^', {});
        }
    })

    .controller('app.purchasing.pr.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.pr = {};
        $scope.vendor = {};
        $scope.qty_po = [];
        $scope.price = [];
        $scope.items = [];
        $scope.id_mpc_piro_item = [];
        $scope.priority = ['Normal', 'Urgent', 'AOG'];

        myHelp.getDetail('/store/pr/create')
            .then(function (respons) {
                $scope.pr = respons.data.data;
                $scope.partid = respons.data.partid;
                $scope.aircraft = respons.data.aircraft;
                $scope.getItems($scope.pr.id_pur_pr);

            });


        $scope.getItems = function () {
            myHelp.getParam('/store/pr_item/create', {id_pur_pr: $scope.pr.id_pur_pr})
                .then(function (respons) {
                    $scope.items = respons.data;
                });
        }

        $scope.add_item = function () {
            myHelp.postParam('/store/pr_item', {piro_items: $scope.id_mpc_piro_item, id_pur_pr: $scope.pr.id_pur_pr})
                .then(function mySuccesresponse() {
                        $scope.getItems();
                        $state.go("^", {})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

        $scope.delete_item = function (x) {
            myHelp.delete('/store/pr_item/' + x)
                .then(function mySuccesresponse() {
                        $scope.getItems();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

        // JIKA INJEK LANGSUNG

        /*-------------------------- POPUP  ----------------------------------------*/
        $scope.popup = false;
        $scope.startPopup = function () {
            $scope.popup = true;
        }
        $scope.closePopup = function () {
            $scope.popup = false;
        }

        /*-------------------------- POPUP  ----------------------------------------*/

        $scope.popupSearchParent = function () {
            var param = {};

            if ($scope.mpart.keyword.length > 0) {
                param = clearObj($scope.mpart);
                console.log(param);
                myHelp.getParam('/master/mpart', param)
                    .then(function (respons) {
                        $scope.params = respons.data;
                    });
            }
        }

        $scope.selectItem = function (id_mpart) {
            myHelp.postParam('/store/pr_item', {id_mpart: id_mpart, id_pur_pr: $scope.pr.id_pur_pr})
                .then(function mySuccesresponse() {
                        $scope.getItems();
                        $scope.popup = false;
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }
        // END INJEK LANGSUNG

        $scope.simpanForm = function () {
            console.log($scope.items);
            //cek validasi
            for (var a = 0; a < $scope.items.length; a++) {
                if (!$scope.items[a].qty_pr) {
                    warningView("Part Qty Item Kosong");
                    return false;

                }
                if (!$scope.items[a].note_pr_item) {
                    $scope.items[a].note_pr_item = "";
                }
            }


            if ($scope.items.length < 1) {
                warningView("Part Item Kosong");
                return false;
            }

            var Obj = {pr: $scope.pr, items: $scope.items};
            var Param = clearObj(Obj);
            myHelp.putParam('/store/pr/' + $scope.pr.id_pur_pr, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchasing.pr", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })

    .controller('app.purchasing.pr.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.pr = {};
        $scope.vendor = {};
        $scope.qty_po = [];
        $scope.price = [];
        $scope.items = [];
        $scope.id_mpc_piro_item = [];
        $scope.priority = ['Normal', 'Urgent', 'AOG'];

        /*-------------------------- POPUP  ----------------------------------------*/
        $scope.popup = false;
        $scope.startPopup = function () {
            $scope.popup = true;
        }
        $scope.closePopup = function () {
            $scope.popup = false;
        }

        /*-------------------------- POPUP  ----------------------------------------*/

        myHelp.getDetail('/store/pr/' + $stateParams.id_pur_pr + '/edit')
            .then(function (respons) {
                $scope.pr = respons.data.data;
                $scope.aircraft = respons.data.aircraft;
                $scope.getItems($scope.pr.id_pur_pr);

            });

        $scope.getItems = function () {
            myHelp.getParam('/store/pr_item/create', {id_pur_pr: $scope.pr.id_pur_pr})
                .then(function (respons) {
                    $scope.items = respons.data;
                });
        }

        $scope.add_item = function () {
            myHelp.postParam('/store/pr_item', {piro_items: $scope.id_mpc_piro_item, id_pur_pr: $scope.pr.id_pur_pr})
                .then(function mySuccesresponse() {
                        $scope.getItems();
                        $state.go("^", {})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

        $scope.delete_item = function (x) {
            myHelp.delete('/store/pr_item/' + x)
                .then(function mySuccesresponse() {
                        $scope.getItems();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

        // JIKA INJEK LANGSUNG
        $scope.popupSearchParent = function () {
            var param = {};

            if ($scope.mpart.keyword.length > 0) {
                param = clearObj($scope.mpart);
                console.log(param);
                myHelp.getParam('/master/mpart', param)
                    .then(function (respons) {
                        $scope.params = respons.data;
                    });
            }
        }

        $scope.selectItem = function (id_mpart) {
            myHelp.postParam('/store/pr_item', {id_mpart: id_mpart, id_pur_pr: $scope.pr.id_pur_pr})
                .then(function mySuccesresponse() {
                        $scope.getItems();
                        $scope.popup = false;
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }
        // END INJEK LANGSUNG

        $scope.simpanForm = function () {
            console.log($scope.items);
            //cek validasi
            for (a = 0; a < $scope.items.length; a++) {
                if (!$scope.items[a].qty_pr) {
                    warningView("Part Qty Item Kosong");
                    return false;

                }
                if (!$scope.items[a].note_pr_item) {
                    $scope.items[a].note_pr_item = "";
                }
            }


            if ($scope.items.length < 1) {
                warningView("Part Item Kosong");
                return false;
            }

            var Obj = {pr: $scope.pr, items: $scope.items};
            var Param = clearObj(Obj);
            myHelp.putParam('/store/pr/' + $scope.pr.id_pur_pr, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchasing.pr", {}, {reload: true})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })


    .controller('app.purchasing.pr.add.item', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.filters = {};
        $scope.status_item = ['open'];
        $scope.filters.status_item = 'pro';

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/store/piro_item', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.aircraft = respons.data.aircraft;
                    $scope.datas = respons.data.data;
                });
        }
        $scope.filter(1);
    })

    .controller('app.purchasing.pr.edit.item', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.filters = {};
        $scope.status_item = ['pro'];
        $scope.filters.status_item = 'pro';

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/store/piro_item', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.aircraft = respons.data.aircraft;
                    $scope.datas = respons.data.data;
                });
        }
        $scope.filter(1);
    })


    /*----------------------------------------------------------------------------------------------
     PO
     /*----------------------------------------------------------------------------------------------*/

    .controller('app.purchasing.po.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.po = {};
        $scope.vendor = {};
        $scope.qty_po = [];
        $scope.price = [];
        $scope.items = [];

        $scope.vendor_contact = [];
        $scope.id_pur_pr_item = [];
        $scope.priority = ['Normal', 'Urgent', 'AOG'];

        /*-------------------------- POPUP  ----------------------------------------*/
        $scope.popup = false;
        $scope.startPopup = function () {
            $scope.popup = true;
        }
        $scope.closePopup = function () {
            $scope.popup = false;
        }

        /*-------------------------- POPUP  ----------------------------------------*/

        myHelp.getDetail('/purchasing/po/create')
            .then(function (respons) {
                $scope.po = respons.data.data;
                $scope.shipping = respons.data.shipping;
                $scope.getItems();

            });

        $scope.getItems = function (id) {
            myHelp.getParam('/purchasing/po_item/create', {id_pur_po: $scope.po.id_pur_po})
                .then(function (respons) {
                    $scope.items = respons.data;
                    debugData(respons);
                });
        }

        $scope.add_item = function () {
            myHelp.postParam('/purchasing/po_item', {pr_items: $scope.id_pur_pr_item, id_pur_po: $scope.po.id_pur_po})
                .then(function mySuccesresponse() {
                        $scope.getItems();
                        $state.go("^", {})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

        $scope.delete_item = function (x) {
            myHelp.delete('/purchasing/po_item/' + x)
                .then(function mySuccesresponse() {
                        $scope.getItems();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

        myHelp.getDetail('/master/currency')
            .then(function (respons) {
                $scope.currencys = respons.data;
                debugData(respons);
            })

        $scope.popupSearchParent = function () {
            var param = {};

            if ($scope.vendor.keyword.length > 0) {
                param = clearObj($scope.vendor);
                console.log(param);
                myHelp.getParam('/purchasing/vendor', param)
                    .then(function (respons) {
                        $scope.params = respons.data;
                    });
            }
        }

        $scope.selectItem = function (id_our_vendor, vendor) {
            $scope.po.id_pur_vendor = id_our_vendor;
            console.log($scope.po);
            $scope.vendor_name = vendor;
            $scope.popup = false;

            myHelp.getDetail('/purchasing/vendor_contact/' + id_our_vendor)
                .then(function (respons) {
                    $scope.vendor_contact = respons.data;
                });

        }


        $scope.hitungTotal = function () {

            var Total = 0;
            for (a = 0; a < $scope.items.length; a++) {
                Total = Total + ($scope.items[a].price * $scope.items[a].qty_po);
            }
            if (Total < 1) {
                warningView("tentukan Harga Terlebih dahulu");
            }
            $scope.po.price_total = Total;
        }

        $scope.simpanForm = function () {
            $scope.hitungTotal();
            if ($scope.items.length < 1) {
                warningView("Part Item Kosong");
                return false;
            }
            if ($scope.po.id_pur_vendor < 1) {
                warningView("Vendor Kosong");
                return false;
            }
            var Obj = {po: $scope.po, items: $scope.items};
            var Param = clearObj(Obj);
            myHelp.putParam('/purchasing/po/' + $scope.po.id_pur_po, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchasing.po", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })

    .controller('app.purchasing.po.add.item', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.param = {};


        $scope.filters = {};
        $scope.status_item = ['pr'];
        $scope.priority = ['Normal', 'Urgent', 'AOG'];
        $scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $scope.years = [];
        $scope.BASE_URL = BASE_URL;

        var date = new Date();

        for (var a = 0; a < 5; a++) {
            $scope.years.push(date.getFullYear() - a);
        }

        $scope.filters.status_item = 'pr';

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/purchasing/pr_item', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.aircraft = respons.data.aircraft;
                    $scope.datas = respons.data.data;
                });
        }
        $scope.filter(1);


    })

    .controller('app.purchasing.po', function truncateCtrl($scope, $state, $stateParams, myHelp, $window, $rootScope, $http) {
        $scope.filters = {};
        $scope.status_po = ['draft', 'confirm', 'approved', 'grn', 'open', 'close'];
        $scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $scope.years = [];
        $scope.BASE_URL = BASE_URL;

        $scope.loadData = false;

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/purchasing/po', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);

        $scope.export_report = function (id) {
            var url = $rootScope.report_url('release/PO.rpt', '&' + Object_toReport({id: id}));
            $window.open(url);
        }
        $scope.deletePo = function (id) {
            deleteParam($http, $state, '/purchasing/po/' + id, {}, 'app.purchasing.po', {});
        }
    })

    .controller('app.purchasing.po.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {

        $scope.loadData = false;
        myHelp.getDetail('/purchasing/po/' + $stateParams.id_pur_po)
            .then(function (response) {
                $scope.po = response.data.data;
                $scope.items = response.data.items;
                $scope.loadData = true;
            })

        $scope.acc = function (acc) {
            PindahParam($http, $state, '/purchasing/po/' + acc, {id_pur_po: $stateParams.id_pur_po}, 'app.purchasing.po.detail', {id_pur_po: $stateParams.id_pur_po}, acc);
        }

        $scope.delete = function () {
            deleteParam($http, $state, '/purchasing/po/' + $stateParams.id_pur_po, {}, '^', {});
        }

    })


    .controller('app.purchasing.po.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.po = {};
        $scope.vendor = {};
        $scope.qty_po = [];
        $scope.price = [];
        $scope.items = [];

        $scope.vendor_contact = [];
        $scope.id_pur_pr_item = [];
        $scope.priority = ['Normal', 'Urgent', 'AOG'];

        /*-------------------------- POPUP  ----------------------------------------*/
        $scope.popup = false;
        $scope.startPopup = function () {
            $scope.popup = true;
        }
        $scope.closePopup = function () {
            $scope.popup = false;
        }

        /*-------------------------- POPUP  ----------------------------------------*/


        myHelp.getDetail('/purchasing/po/' + $stateParams.id_pur_po + '/edit')
            .then(function (respons) {
                $scope.po = respons.data.data;
                $scope.vendor_name = $scope.po.id_pur_vendor;
                $scope.shipping = respons.data.shipping;
                $scope.getItems();
                $scope.selectItem($scope.po.id_pur_vendor, $scope.po.id_pur_vendor);

                myHelp.getDetail('/master/currency')
                    .then(function (respons) {
                        $scope.currencys = respons.data;
                    });


            });

        $scope.getItems = function () {
            myHelp.getParam('/purchasing/po_item/create', {id_pur_po: $scope.po.id_pur_po})
                .then(function (respons) {
                    $scope.items = respons.data;
                    debugData(respons);
                });
        }

        $scope.add_item = function () {
            myHelp.postParam('/purchasing/po_item', {pr_items: $scope.id_pur_pr_item, id_pur_po: $scope.po.id_pur_po})
                .then(function mySuccesresponse() {
                        $scope.getItems();
                        $state.go("^", {})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

        $scope.delete_item = function (x) {
            myHelp.delete('/purchasing/po_item/' + x)
                .then(function mySuccesresponse() {
                        $scope.getItems();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }


        $scope.popupSearchParent = function () {
            var param = {};

            if ($scope.vendor.keyword.length > 2) {
                param = clearObj($scope.vendor);
                console.log(param);
                myHelp.getParam('/purchasing/vendor', param)
                    .then(function (respons) {
                        $scope.params = respons.data;
                    });
            }
        }

        $scope.selectItem = function (id_our_vendor, vendor) {
            $scope.po.id_pur_vendor = id_our_vendor;
            console.log($scope.po);
            $scope.vendor_name = vendor;
            $scope.popup = false;

            myHelp.getDetail('/purchasing/vendor_contact/' + id_our_vendor)
                .then(function (respons) {
                    $scope.vendor_contact = respons.data;
                });

        }


        $scope.hitungTotal = function () {

            var Total = 0;
            for (a = 0; a < $scope.items.length; a++) {
                Total = Total + ($scope.items[a].price * $scope.items[a].qty_po);
            }
            if (Total < 1) {
                warningView("tentukan Harga Terlebih dahulu");
            }
            $scope.po.price_total = Total;
        }

        $scope.simpanForm = function () {
            $scope.hitungTotal();
            if ($scope.items.length < 1) {
                warningView("Part Item Kosong");
            }
            if ($scope.po.id_pur_vendor < 1) {
                warningView("Vendor Kosong");
            }
            var Obj = {po: $scope.po, items: $scope.items};
            var Param = clearObj(Obj);
            myHelp.putParam('/purchasing/po/' + $scope.po.id_pur_po, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchasing.po", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })

    .controller('app.purchasing.po.edit.item', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.param = {};


        $scope.filters = {};
        $scope.status_item = ['pr'];
        $scope.priority = ['Normal', 'Urgent', 'AOG'];
        $scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $scope.years = [];
        $scope.BASE_URL = BASE_URL;

        var date = new Date();

        for (var a = 0; a < 5; a++) {
            $scope.years.push(date.getFullYear() - a);
        }

        $scope.filters.status_item = 'pr';

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/purchasing/pr_item', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.aircraft = respons.data.aircraft;
                    $scope.datas = respons.data.data;
                });
        }
        $scope.filter(1);


    })

    /*----------------------------------------------------------------------------------------------
    PO
    /*----------------------------------------------------------------------------------------------*/
    .controller('app.purchasing.wo', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.filters = {};
        $scope.loadData = false;

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/purchasing/wo', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);

        $scope.deletewo = function () {
            deleteParam($http, $state, '/purchasing/po/' + $stateParams.id_pur_po, {}, 'app.purchasing.wo', {reload: true});
        }

    })

    .controller('app.purchasing.wo.add', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {
        $scope.wo_item = [];
        $scope.wo_pree_item = {};
        $scope.po = {};

        /*-------------------------- POPUP  ----------------------------------------*/
        $scope.popup = false;
        $scope.isshowListService = false;
        $scope.ukuranPreeAdd = 0;
        $scope.startPopup = function () {
            $scope.popup = true;
        }
        $scope.closePopup = function () {
            $scope.popup = false;
        }

        $scope.showListService = function () {
            if ($scope.isshowListService == false) {
                $scope.isshowListService = true;
            }
            else {
                $scope.isshowListService = false;
            }

        }

        $scope.togleUkuranPreeAdd = function (status) {
            if (status == true) {
                $scope.ukuranPreeAdd = 3;
            }
            else {
                $scope.ukuranPreeAdd = 0;
            }

        }

        /*-------------------------- POPUP  ----------------------------------------*/

        myHelp.getParam('/purchasing/wo/create', {})
            .then(function (response) {
                $scope.po = response.data.data;
                $scope.preeadd = response.data.preeadd;
                $scope.shipping = response.data.shipping;

                $scope.getPart($scope.po.id_pur_po);
            })

        $scope.popupSearchParent = function () {
            var param = {};

            if ($scope.vendor.keyword.length > 0) {
                param = clearObj($scope.vendor);
                console.log(param);
                myHelp.getParam('/purchasing/vendor', param)
                    .then(function (respons) {
                        $scope.params = respons.data;
                    });
            }
        }

        $scope.selectItem = function (id_our_vendor, vendor) {
            $scope.po.id_pur_vendor = id_our_vendor;
            console.log($scope.po);
            $scope.vendor_name = vendor;
            $scope.popup = false;

            myHelp.getDetail('/purchasing/vendor_contact/' + id_our_vendor)
                .then(function (respons) {
                    $scope.vendor_contact = respons.data;
                });

        }

        myHelp.getDetail('/master/currency')
            .then(function (respons) {
                $scope.currencys = respons.data;
                debugData(respons);
            });

        $scope.preeAddPart = function (id_engineer_rti) {
            $scope.wo_pree_item.id_engineer_rti = id_engineer_rti;
            $scope.togleUkuranPreeAdd(true);
        }


        $scope.addPart = function () {
            $scope.wo_pree_item.id_pur_po = $scope.po.id_pur_po;
            myHelp.postParam('/purchasing/wo_item', $scope.wo_pree_item)
                .then(function (respons) {
                    $scope.getPart($scope.po.id_pur_po);
                    $scope.isshowListService = false;
                    $scope.ukuranPreeAdd = 0;
                });
        }

        $scope.getPart = function (id_pur_po) {
            console.log($scope.po.id_pur_po);
            myHelp.getDetail('/purchasing/wo_item/' + id_pur_po)
                .then(function (respons) {
                    $scope.wo_item = respons.data;
                });
        }

        $scope.simpanForm = function () {
            $scope.hitungTotal();
            if ($scope.wo_item.length < 1) {
                warningView("Part Item Kosong");
            }
            if ($scope.po.id_pur_vendor < 1) {
                warningView("Vendor Kosong");
            }
            var Obj = {po: $scope.po, items: $scope.wo_item};
            var Param = clearObj(Obj);
            myHelp.putParam('/purchasing/wo/' + $scope.po.id_pur_po, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

        $scope.hitungTotal = function () {

            var Total = 0;
            for (a = 0; a < $scope.wo_item.length; a++) {
                Total = Total + ($scope.wo_item[a].price * $scope.wo_item[a].qty_po);
            }
            if (Total < 1) {
                warningView("tentukan Harga Terlebih dahulu");
            }
            $scope.po.price_total = Total;
        }

        $scope.delete = function (id) {
            myHelp.delete('/purchasing/wo_item/' + id)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $scope.wo_item = [];
                        $scope.getPart($scope.po.id_pur_po);
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }


    })


    .controller('app.purchasing.wo.edit', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {
        $scope.wo_item = [];
        $scope.wo_pree_item = {};
        $scope.po = {};

        /*-------------------------- POPUP  ----------------------------------------*/
        $scope.popup = false;
        $scope.isshowListService = false;
        $scope.ukuranPreeAdd = 0;
        $scope.startPopup = function () {
            $scope.popup = true;
        }
        $scope.closePopup = function () {
            $scope.popup = false;
        }

        $scope.showListService = function () {
            if ($scope.isshowListService == false) {
                $scope.isshowListService = true;
            }
            else {
                $scope.isshowListService = false;
            }

        }

        $scope.togleUkuranPreeAdd = function (status) {
            if (status == true) {
                $scope.ukuranPreeAdd = 3;
            }
            else {
                $scope.ukuranPreeAdd = 0;
            }

        }

        /*-------------------------- POPUP  ----------------------------------------*/

        myHelp.getParam('/purchasing/wo/' + $stateParams.id_pur_po + '/edit', {})
            .then(function (response) {
                $scope.po = response.data.data;
                $scope.preeadd = response.data.preeadd;
                $scope.shipping = response.data.shipping;

                $scope.getPart($scope.po.id_pur_po);
                //$scope.ambilContact();

                myHelp.getDetail('/master/currency')
                    .then(function (respons) {
                        $scope.currencys = respons.data;
                        debugData(respons);
                    });
                $scope.selectItem($scope.po.id_pur_vendor, $scope.po.id_pur_vendor);

            })

        $scope.popupSearchParent = function () {
            var param = {};

            if ($scope.vendor.keyword.length > 0) {
                param = clearObj($scope.vendor);
                console.log(param);
                myHelp.getParam('/purchasing/vendor', param)
                    .then(function (respons) {
                        $scope.params = respons.data;
                    });
            }
        }

        $scope.selectItem = function (id_our_vendor, vendor) {
            $scope.po.id_pur_vendor = id_our_vendor;
            console.log($scope.po);
            $scope.vendor_name = vendor;
            $scope.popup = false;
            $scope.ambilContact(id_our_vendor);

        }

        $scope.ambilContact = function (id_our_vendor) {
            myHelp.getDetail('/purchasing/vendor_contact/' + id_our_vendor)
                .then(function (respons) {
                    $scope.vendor_contact = respons.data;
                });
        }


        $scope.preeAddPart = function (id_engineer_rti) {
            $scope.wo_pree_item.id_engineer_rti = id_engineer_rti;
            $scope.togleUkuranPreeAdd(true);
        }


        $scope.addPart = function () {
            $scope.wo_pree_item.id_pur_po = $scope.po.id_pur_po;
            myHelp.postParam('/purchasing/wo_item', $scope.wo_pree_item)
                .then(function (respons) {
                    $scope.getPart($scope.po.id_pur_po);
                    $scope.isshowListService = false;
                    $scope.ukuranPreeAdd = 0;
                });
        }

        $scope.getPart = function (id_pur_po) {
            console.log($scope.po.id_pur_po);
            myHelp.getDetail('/purchasing/wo_item/' + id_pur_po)
                .then(function (respons) {
                    $scope.wo_item = respons.data;
                });
        }

        $scope.simpanForm = function () {
            $scope.hitungTotal();
            if ($scope.wo_item.length < 1) {
                warningView("Part Item Kosong");
            }
            if ($scope.po.id_pur_vendor < 1) {
                warningView("Vendor Kosong");
            }
            var Obj = {po: $scope.po, items: $scope.wo_item};
            var Param = clearObj(Obj);
            myHelp.putParam('/purchasing/wo/' + $scope.po.id_pur_po, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

        $scope.hitungTotal = function () {

            var Total = 0;
            for (a = 0; a < $scope.wo_item.length; a++) {
                Total = Total + ($scope.wo_item[a].price * $scope.wo_item[a].qty_po);
            }
            if (Total < 1) {
                warningView("tentukan Harga Terlebih dahulu");
            }
            $scope.po.price_total = Total;
        }

        $scope.delete = function (id) {
            myHelp.delete('/purchasing/wo_item/' + id)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $scope.wo_item = [];
                        $scope.getPart($scope.po.id_pur_po);
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }


    })


    .controller('app.purchasing.wo.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp, $rootScope, $window) {

        myHelp.getDetail('/purchasing/wo/' + $stateParams.id_pur_po)
            .then(function (response) {
                $scope.po = response.data.data;
                $scope.items = response.data.items;
            })

        $scope.acc = function (acc) {
            PindahParam($http, $state, '/purchasing/po/' + acc, {id_pur_po: $stateParams.id_pur_po}, 'app.purchasing.wo.detail', {id_pur_po: $stateParams.id_pur_po}, acc);
        }

        $scope.delete = function () {
            deleteParam($http, $state, '/purchasing/po/' + $stateParams.id_pur_po, {}, '^', {});
        }

        $scope.export_report = function (id) {

            var url = $rootScope.report_url('OLD/PORODOL_WO.rpt', '&' + Object_toReport({id: id}));

            $window.open(url);

        }
        $scope.export_report_sh = function (id) {

            var url = $rootScope.report_url('release/SHIPPING_NOTE.RPT', '&' + Object_toReport({id: id}));

            $window.open(url);

        }

    })


    /*----------------------------------------------------------------------------------------------
     return
     /*----------------------------------------------------------------------------------------------*/
    .controller('app.purchasing.return', function truncateCtrl($scope, $rootScope, $window, $state, $stateParams, myHelp) {
        $scope.param = {};
        $scope.filters = {};
        $scope.loadData = false;

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/purchasing/return', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;
                    $scope.loadData = false;
                });
        }
        $scope.filter(1);

        $scope.export_report = function (id) {
            console.log('sds')
            var url = $rootScope.report_url('release/RETURN.rpt', '&' + Object_toReport({id: id}));

            $window.open(url);

        }
    })

    .controller('app.purchasing.return.add', function truncateCtrl($scope, $window, $rootScope, $timeout, $state, $stateParams, myHelp) {
        /*-------------------------- POPUP  ----------------------------------------*/
        $scope.popup = false;
        $scope.isshowListService = false;
        $scope.ukuranPreeAdd = 0;
        $scope.startPopup = function () {
            $scope.popup = true;
        }
        $scope.closePopup = function () {
            $scope.popup = false;
        }

        $scope.showListService = function () {
            if ($scope.isshowListService == false) {
                $scope.isshowListService = true;
            }
            else {
                $scope.isshowListService = false;
            }

        }

        $scope.togleUkuranPreeAdd = function (status) {
            if (status == true) {
                $scope.ukuranPreeAdd = 3;
            }
            else {
                $scope.ukuranPreeAdd = 0;
            }

        }

        /*-------------------------- POPUP  ----------------------------------------*/

        $scope.popupSearchParent = function () {
            var param = {};

            if ($scope.vendor.keyword.length > 0) {
                param = clearObj($scope.vendor);
                console.log(param);
                myHelp.getParam('/purchasing/vendor', param)
                    .then(function (respons) {
                        $scope.params = respons.data;
                    });
            }
        }

        $scope.autoSave = function () {
            $timeout(function () {
                $scope.return_pree_item.id_pur_po = $scope.return.id_pur_po;
                myHelp.postParam('/purchasing/return', $scope.return)
                    .then(function () {

                    });
            }, 200)

        }

        myHelp.getParam('/purchasing/return/create', {})
            .then(function (response) {
                $scope.return = response.data.data;
                $scope.shipping = response.data.shipping;
                $scope.getPart($scope.return.id_pur_po);

                $scope.selectItem($scope.return.id_pur_vendor,  $scope.return.id_pur_vendor);
            })

        $scope.selectItem = function (id_our_vendor, vendor) {
            $scope.return.id_pur_vendor = id_our_vendor;
            console.log($scope.po);
            $scope.vendor_name = vendor;
            $scope.popup = false;

            myHelp.getDetail('/purchasing/vendor_contact/' + id_our_vendor)
                .then(function (respons) {
                    $scope.vendor_contact = respons.data;
                });

            myHelp.getDetail('/purchasing/return_item/create?id_pur_vendor=' + id_our_vendor + '')
                .then(function (respons) {
                    $scope.preeadd = respons.data;
                    $scope.autoSave();
                });

        }

        myHelp.getDetail('/master/currency')
            .then(function (respons) {
                $scope.currencys = respons.data;
                debugData(respons);
            });

        $scope.return_pree_item = {};
        $scope.preeAddPart = function (id_enginer_prcl) {
            $scope.return_pree_item.id_pur_prcl = id_enginer_prcl;
            $scope.togleUkuranPreeAdd(true);
        }


        $scope.addPart = function () {
            $scope.return_pree_item.id_pur_po = $scope.return.id_pur_po;
            myHelp.postParam('/purchasing/return_item', $scope.return_pree_item)
                .then(function (respons) {
                    $scope.getPart($scope.return.id_pur_po);
                    $scope.isshowListService = false;
                    $scope.ukuranPreeAdd = 0;
                });
        }

        $scope.getPart = function (id_pur_po) {
            console.log($scope.return.id_pur_po);
            myHelp.getDetail('/purchasing/return_item/' + id_pur_po)
                .then(function (respons) {
                    $scope.return_item = respons.data;
                });
        }

        $scope.simpanForm = function () {
            $scope.hitungTotal();
            if ($scope.return_item.length < 1) {
                warningView("Part Item Kosong");
            }
            if ($scope.return.id_pur_vendor < 1) {
                warningView("Vendor Kosong");
            }
            var Obj = {po: $scope.return, items: $scope.return_item};
            var Param = clearObj(Obj);
            myHelp.putParam('/purchasing/return/' + $scope.return.id_pur_po, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchasing.return", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

        $scope.hitungTotal = function () {

            var Total = 0;
            for (a = 0; a < $scope.return_item.length; a++) {
                Total = Total + ($scope.return_item[a].price * $scope.return_item[a].qty_po);
            }

            $scope.return.price_total = Total;
        }

        $scope.delete = function (id) {
            myHelp.delete('/purchasing/wo_item/' + id)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $scope.return_item = [];
                        $scope.getPart($scope.return.id_pur_po);
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

    })


    .controller('app.purchasing.return.add.item', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.param = {};
        $scope.param.keyword = "";
        $scope.currentPage = 1;
        $scope.pageSize = 1;

        $scope.getPaginate = function (page) {
            var param = {};
            if ($scope.return.id_pur_vendor == '') {
                errorView('Please select Vendor');
                $state.go('^', {}, {});
            }
            param.page = page;
            param.id_pur_vendor = $scope.return.id_pur_vendor;
            myHelp.getParam('/purchasing/return_item/create', clearObj(param))
                .then(function (response) {
                    $scope.po_wo = response.data;
                });
        }
        $scope.getPaginate(1);

    })


    .controller('app.purchasing.return.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {
        myHelp.getDetail('/purchasing/return/' + $stateParams.id_pur_po)
            .then(function (response) {
                $scope.return = response.data.data;
                $scope.items = response.data.items;
            })

        $scope.acc = function (acc) {
            PindahParam($http, $state, '/purchasing/po/' + acc, {id_pur_po: $stateParams.id_pur_po}, 'app.purchasing.return.detail', {id_pur_po: $stateParams.id_pur_po}, acc);
        }

        $scope.delete = function () {
            deleteParam($http, $state, '/purchasing/return/' + $stateParams.id_pur_po, {}, '^', {});
        }

        // $scope.export_report = function (id) {
        //
        //     var url = $rootScope.report_url('OLD/PORODOL_WO.rpt','&' + Object_toReport({id:id}));
        //
        //     $window.open(url);
        //
        // }
        // $scope.export_report_sh = function (id) {
        //
        //     var url = $rootScope.report_url('OLD/POPODOL_SHIIPING.rpt','&' + Object_toReport({id:id}));
        //
        //     $window.open(url);
        //
        // }

    })


    /*----------------------------------------------------------------------*/
    /*                      GRN PURCHASING
    /*----------------------------------------------------------------------*/


    .controller('app.purchasing.grn.add', function truncateCtrl($scope, $state, $stateParams, myHelp, $timeout) {
        $scope.grn = {};
        $scope.po = {};
        $scope.vendor = {};
        $scope.param = {};
        $scope.items = [];
        $scope.id_pur_po_item = [];

        myHelp.getParam('/purchasing/grn/create', {id_pur_po: $stateParams.id_pur_po})
            .then(function (response) {
                $scope.grn = response.data.data;

                $timeout(function () {
                    $scope.vendor = response.data.vendor;
                }, 100)

                $scope.param.id_pur_grn = $scope.grn.id_pur_grn;
                $scope.getItems($scope.grn.id_pur_grn);

            });

        $scope.getItems = function (id) {
            myHelp.getParam('/purchasing/grn_item/' + id)
                .then(function (response) {
                    $scope.items = response.data;
                    debugData(response);
                });
        }

        $scope.add_item = function () {
            myHelp.postParam('/purchasing/grn_item', {
                id_pur_vendor: $scope.grn.id_pur_vendor,
                id_pur_po_item: $scope.id_pur_po_item,
                id_pur_grn: $scope.grn.id_pur_grn
            })
                .then(function mySuccesresponse() {
                        $scope.getItems($scope.grn.id_pur_grn);
                        $state.go("^", {})
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }
        $scope.delete_item = function (id) {
            myHelp.delete('/purchasing/grn_item/' + id)
                .then(function mySuccesresponse() {
                        $scope.getItems($scope.grn.id_pur_grn);
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        }

        $scope.save = function () {
            myHelp.postParam('/purchasing/grn', {grn: clearObj($scope.grn), last: 0})
                .then(
                    function berhasil() {

                    }
                )
        }

        $scope.simpanForm = function () {

            if ($scope.items.length < 1) {
                warningView("Part Item Kosong");
                return false;
            }
            if ($scope.grn.id_pur_vendor.length < 1) {
                warningView("Part Item Kosong");
                return false;
            }

            var Obj = {grn: $scope.grn, items: $scope.items};
            var Param = clearObj(Obj);
            Param.last = 1;
            Param.id_pur_grn = $scope.param.id_pur_grn;
            Param.id_pur_vendor = $scope.grn.id_pur_vendor;
            myHelp.postParam('/purchasing/grn', clearObj(Param))
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $state.go("app.purchasing.grn", {}, {reload: true})

                    }
                    , function myError(respon) {
                        errorView(respon.data.status);
                    });
        };

    })


    .controller('app.purchasing.grn.add.item', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.param = {};
        $scope.param.keyword = "";
        $scope.currentPage = 1;
        $scope.pageSize = 1;

        $scope.getPaginate = function (page) {
            var param = {};
            if ($scope.grn.id_pur_vendor == '') {
                errorView('Please select Vendor');
                $state.go('^', {}, {});
            }
            param.page = page;
            param.id_pur_vendor = $scope.grn.id_pur_vendor;
            myHelp.getParam('/purchasing/grn_item/create', clearObj(param))
                .then(function (response) {
                    $scope.po_wo = response.data;
                });
        }
        $scope.getPaginate(1);

    })

    .controller('app.purchasing.grn', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.param = {};
        $scope.filters = {};
        $scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $scope.years = [];
        $scope.status_grn = ['Purchasing', 'QA'];
        $scope.BASE_URL = BASE_URL;

        $scope.loadData = false;

        var date = new Date();

        for (var a = 0; a < 5; a++) {
            $scope.years.push(date.getFullYear() - a);
        }

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/purchasing/grn', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);
    })

    .controller('app.purchasing.grn.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {
        $scope.prcls = [];

        myHelp.getDetail('/purchasing/grn/' + $stateParams.id_pur_grn)
            .then(function (responsee) {
                $scope.grn = responsee.data.data;
                $scope.po = responsee.data.po;
                $scope.items = responsee.data.items;
                $scope.prcls = responsee.data.prcl;
            })

        myHelp.getDetail('/master/part_condition')
            .then(function (responsee) {
                $scope.part_condition = responsee.data;
            })

        $scope.simpanForm = function () {
            console.log("sadsa");
        }

        $scope.sendQa = function (acc) {
            for (a = 0; a < $scope.prcls.length; a++) {
                console.log($scope.prcls[a].sn);
                if ($scope.prcls[a].sn == "" || $scope.prcls[a].sn == null) {
                    if ($scope.prcls[a].issingle == "y") {
                        errorView("error , serial number " + $scope.prcls[a].keyword);
                        return false;
                    }

                }
            }
            PindahParam($http, $state, '/purchasing/validation/grn/' + acc, {id_pur_grn: $stateParams.id_pur_grn}, 'app.purchasing.grn.detail', {id_pur_grn: $stateParams.id_pur_grn}, "Send to QA");
        }
        $scope.deleted = function (id) {
            myHelp.deleteConfirm('/purchasing/prcl/' + id, {}, 'app.purchasing.grn.detail', {id_pur_grn: $stateParams.id_pur_grn});
        }

    })


    .controller('app.purchasing.grn.detail.prcl', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {
        $scope.prcl = {};
        $scope.prcl.id_pur_vendor = "";

        myHelp.getDetail('/purchasing/prcl/' + $stateParams.id_pur_prcl + '/edit' + '?id_pur_grn=' + $stateParams.id_pur_grn)
            .then(function (responsee) {
                $scope.prcl = responsee.data.prcl;
                $scope.parent = responsee.data.parent;
                $scope.po_item = responsee.data.po_item;
                $scope.variabel = responsee.data.variabel;
                $scope.alternative = responsee.data.alternative;


                console.log("iko ==> " + $scope.prcl.id_mpart)
                myHelp.getDetail('/master/mpart/' + $scope.prcl.id_mpart)
                    .then(function (responsee) {
                        $scope.mpart = responsee.data;
                    });

                $scope.event_mpart_alternative = function () {
                    if ($scope.prcl.mpart_alternative != null) {
                        myHelp.getDetail('/master/mpart/' + $scope.prcl.mpart_alternative)
                            .then(function (responsee) {
                                $scope.mpart = responsee.data;
                            });
                    }
                    else {
                        myHelp.getDetail('/master/mpart/' + $scope.prcl.id_mpart)
                            .then(function (responsee) {
                                $scope.mpart = responsee.data;
                            });
                    }


                }

                myHelp.getParam('/purchasing/vendor', {is_manufacture: 'y', ispage: 'n'})
                    .then(function (responsee) {
                        $scope.vendor = responsee.data.vendors;
                    });

                myHelp.getDetail('/master/part_condition')
                    .then(function (responsee) {
                        $scope.part_condition = responsee.data;
                    });

                myHelp.getDetail('/master/life_time_limit')
                    .then(function (responsee) {
                        $scope.life_time_limit = responsee.data;
                    });

                myHelp.getDetail('/master/condition_monitoring')
                    .then(function (responsee) {
                        $scope.condition_monitoring = responsee.data;
                    });

            });

        $scope.submitForm = function () {
            $scope.prcl.id_pur_vendor = $scope.grn.id_pur_vendor;
            $scope.prcl.id_currency = $scope.grn.id_currency;
            $scope.prcl.price = $scope.po_item.price;

            var Param = clearObj($scope.prcl);
            myHelp.putParam('/purchasing/prcl/' + $scope.prcl.id_pur_prcl, Param)
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $state.go("app.purchasing.grn.detail", {id_pur_grn: $scope.prcl.id_pur_grn}, {reload: "app.purchasing.grn.detail"})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    /*----------------------------------------------------------------------*/
    /*                      GRN QA
    /*----------------------------------------------------------------------*/

    .controller('app.purchasing.grn.detail_qa', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/qa/grn/' + $stateParams.id_pur_grn)
            .then(function (responsee) {
                $scope.grn = responsee.data.data;
                $scope.po = responsee.data.po;
                $scope.items = responsee.data.items;
                $scope.prcls = responsee.data.prcl;
            })

        // $scope.validasi = function () {
        //     var acc="test";
        //     PindahParam($http,$state,'/qa/validation/grn/' + acc, {id_pur_grn:$stateParams.id_pur_grn} ,'qa.grn.detail',{id_pur_grn:$stateParams.id_pur_grn},"Send to QA");
        // }

    })


    .controller('app.purchasing.grn.detail_qa.prcl', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {
        $scope.prcl = {};
        $scope.prcl.id_pur_vendor = "";

        myHelp.getDetail('/qa/prcl/' + $stateParams.id_pur_prcl + '/edit' + '?id_pur_grn=' + $stateParams.id_pur_grn)
            .then(function (responsee) {

                $scope.prcl = responsee.data.prcl;
                $scope.parent = responsee.data.parent;
                $scope.variabel = responsee.data.variabel;
                $scope.alternative = responsee.data.alternative;


                myHelp.getParam('/purchasing/vendor', {is_manufacture: 'y', ispage: 'n'})
                    .then(function (responsee) {
                        $scope.vendor = responsee.data.vendors;
                    });

                myHelp.getDetail('/master/part_condition')
                    .then(function (responsee) {
                        $scope.part_condition = responsee.data;
                    });

                myHelp.getDetail('/master/life_time_limit')
                    .then(function (responsee) {
                        $scope.life_time_limit = responsee.data;
                    });

                myHelp.getDetail('/master/condition_monitoring')
                    .then(function (responsee) {
                        $scope.condition_monitoring = responsee.data;
                    });

            });

        $scope.submitForm = function () {
            if (!$scope.prcl.part_checking_qa) {
                errorView("please check QTY check !");
                return false;
            }

            var Param = clearObj($scope.prcl);
            myHelp.putParam('/qa/prcl/' + $scope.prcl.id_pur_prcl, Param)
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $state.go("^", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })

    /*----------------------------------------------------------------------------------------------
     SEMI CATALOG
     /*----------------------------------------------------------------------------------------------*/
    .controller('app.purchasing.grn.detail_qa.prcl_child', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {
        $scope.master = {};
        $scope.params = {};

        $scope.master.mpart = {};
        $scope.master.part = {};
        $scope.master.part.qty = 1;
        $scope.master.mpart.issingle = 'y';
        $scope.master.id_aircraft = $stateParams.id_aircraft;

        //untuak form
        $scope.master.mpart.id_mpart = null;
        $scope.master.part.parent = null;

        $scope.catalog = [];
        $scope.id_mpart = 0;
        $scope.parent = {};
        $scope.parent.keyword = "Select Parent";
        $scope.parent.part_number = "";

        myHelp.getParam('/purchasing/prcl/create', {id_pur_prcl: $stateParams.id_pur_prcl})
            .then(function (responsee) {
                $scope.calon_parent = responsee.data;
            });

        $scope.selectParentPart = function (id_part_install, id_part, keyword, part_number) {
            // $scope.master.mpart.parent = id_part;
            $scope.master.part.parent = id_part_install;
            $scope.parent.keyword = keyword;
            $scope.parent.part_number = part_number;
            colosePopup();
        }

        //masuan mode prcl
        myHelp.getParam('/purchasing/vendor', {is_manufacture: 'y', ispage: 'n'})
            .then(function (responsee) {
                $scope.params.vendor = responsee.data.vendors;
            });

        myHelp.getDetail('/master/part_condition')
            .then(function (responsee) {
                $scope.params.part_condition = responsee.data;
            });

        myHelp.getDetail('/master/life_time_limit')
            .then(function (responsee) {
                $scope.params.life_time_limit = responsee.data;
            });

        myHelp.getDetail('/master/condition_monitoring')
            .then(function (responsee) {
                $scope.params.condition_monitoring = responsee.data;
            });

        myHelp.getDetail('/master/variabel/prcl')
            .then(function (responsee) {
                $scope.params.variabel = responsee.data;
            });

        $scope.submitForm = function () {
            if ($scope.master.mpart.id_mpart == null) {
                errorView("Master part Empty !");
                return false;
            }
            if ($scope.master.part.parent == null) {
                errorView("Part parent empty !");
                return false;
            }
            if ($scope.master.mpart.issingle == 'y') {
                $scope.master.part.qty = 1;
            }

            var Param = clearObj($scope.master.part);
            Param.id_pur_grn = $stateParams.id_pur_grn;
            Param.id_mpart = $scope.master.mpart.id_mpart;
            Param.qty_prcl = $scope.master.part.qty;

            myHelp.postParam('/purchasing/prcl', Param)
                .then(function mySuccesresponsee() {
                        berhasilView();
                        //$scope.getPartInstall();
                        $state.go('^', {}, {reload: "app.purchasing.grn.detail_qa"})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })

    .controller('app.purchasing.grn.detail_qa.prcl_child.search_mpart', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        console.log($stateParams.keyword);
        var param = {};
        $scope.pencarian = function (keyword) {
            param.keyword = keyword;
            myHelp.getParam('/master/mpart', param)
                .then(function (respons) {
                    $scope.parents = respons.data;
                });
        }
        $scope.pencarian($stateParams.keyword);


        $scope.selectMpart = function (id_mpart) {
            myHelp.getDetail('/master/mpart/' + id_mpart)
                .then(function (respons) {
                    $scope.master.mpart = respons.data;

                    $scope.master.part.code_ata = respons.data.code_ata;
                    $scope.master.part.id_life_time_limit = respons.data.id_life_time_limit;
                    $scope.master.part.life_limit_cycle = respons.data.life_limit_cycle;
                    $scope.master.part.life_limit_hours = respons.data.life_limit_hours;
                    $scope.master.part.life_limit_var = respons.data.life_limit_var;
                    $scope.master.part.life_limit_val = respons.data.life_limit_val;

                    $scope.master.part.figure_index = respons.data.mfigure_index;
                    $scope.master.part.item_index = respons.data.mitem_index;

                });
            $state.go('^');
        }

    })
    .controller('app.purchasing.grn.detail_qa.prcl_child.add_mpart', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.param = {};
        $scope.param.pencarian = '';
        $scope.master.mpart.keyword = 'Select mpart';


        myHelp.getDetail('/master/mpart/create')
            .then(function (respons) {
                $scope.masters = respons.data;
                debugData(respons);
            });

        $scope.popupSearchParent = function () {
            var param = {};
            if ($scope.param.pencarian.length > 2) {
                param.keyword = $scope.param.pencarian;
            }

            myHelp.getParam('/master/mpart', param)
                .then(function (respons) {
                    $scope.parents = respons.data;
                });
        }

        $scope.selectParentMpart = function (parent, keyword) {
            console.log(keyword);
            $scope.master.mpart.parent = parent;
            $scope.master.mpart.keyword = keyword;
            colosePopup();
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.master.mpart);
            if ($scope.master.mpart.parent == null) {
                warningView('Please Select Parent !');
                return false;
            }

            myHelp.postParam('/master/mpart', Param)
                .then(function mySuccesresponse(respons) {
                        berhasilView();
                        $scope.master.mpart = respons.data.data;
                        $scope.master.part.id_life_time_limit = respons.data.data.id_life_time_limit;
                        $scope.master.part.life_limit_cycle = respons.data.data.life_limit_cycle;
                        $scope.master.part.life_limit_hours = respons.data.data.life_limit_hours;
                        $scope.master.part.life_limit_var = respons.data.data.life_limit_var;
                        $scope.master.part.life_limit_val = respons.data.data.life_limit_val;

                        $scope.master.part.figure_index = respons.data.data.mfigure_index;
                        $scope.master.part.item_index = respons.data.data.mitem_index;

                        $state.go('^')

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });


        };

    })
    .controller('app.purchasing.grn.detail_qa.prcl_child.tree', function truncateCtrl($scope, $state, $stateParams, myHelp, $log) {

        $scope.ac = function () {
            return true;
        }
        $scope.treeData = [];

        myHelp.getParam('/share/tree_view', {id_aircraft: $stateParams.id_aircraft})
            .then(function (respons) {
                $scope.treeData = respons.data.data;

                $scope.treeConfig = {
                    core: {
                        multiple: false,
                        animation: true,
                        error: function (error) {
                            $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                        },
                        check_callback: true,
                        worker: true
                    },
                    version: 1
                };
                $scope.reCreateTree = function () {
                    this.treeConfig.version++;
                }

                $scope.treeEventsObj = {
                    'ready': readyCB,
                    'create_node': createNodeCB
                }

                function readyCB() {
                    $log.info('ready called');

                };

                function createNodeCB(e, item) {
                    $log.info('create_node called');
                };


                $scope.setParent = function () {
                    var node = $scope.treeInstance.jstree(true).get_selected();
                    if (node.length > 0) {
                        myHelp.getDetail('/tech_rec/tree_view/' + node[0])
                            .then(function (respons) {
                                $scope.master.mpart.parent = respons.data.id_mpart;
                                $scope.master.part.parent = respons.data.node[0];
                                $scope.parent.keyword = respons.data.keyword;
                                $scope.parent.part_number = respons.data.part_number;

                            });
                    }
                    else {
                        $scope.master.mpart.parent = null;
                        $scope.master.part.parent = null;
                        $scope.parent.keyword = "";
                        $scope.parent.part_number = "";
                        $scope.master.mpart.keyword = "This Not have Parent";
                    }


                }


            });


    })


    /*----------------------------------------------------------------------*/
    /*                      VENDOR
    /*----------------------------------------------------------------------*/


    .controller('app.purchasing.vendor', function truncateCtrl($scope, $state, $stateParams, myHelp, $rootScope, $window, $http) {
        $scope.filters = {};
        $scope.services = ['vendor', 'manufacture', 'repairer', 'shipper', 'workshop'];

        $scope.loadData = false;
        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/purchasing/vendor', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.contacts = respons.data.contacts;
                    $scope.vendors = respons.data.vendors;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);

        $scope.export_report = function () {

            var url = $rootScope.report_url('release/VENDOR_LIST.RPT', '');

            $window.open(url);

        }

        $scope.delete = function (id) {
            deleteParam($http, $state, '/purchasing/vendor/' + id, {}, 'app.purchasing.vendor', {});
        }
    })

    .controller('app.purchasing.vendor.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {

        $scope.items = [];
        $scope.mpart = {};
        $scope.param = {};
        $scope.vendor = {};

        myHelp.getDetail('/purchasing/vendor/' + $stateParams.id_pur_vendor)
            .then(function (respons) {
                $scope.vendor = respons.data.data;
                $scope.items = respons.data.item;
                debugData(respons);
            });

        $scope.acc = function (acc) {
            PindahParam($http, $state, '/purchasing/validation/vendor/' + acc, {id_pur_vendor: $stateParams.id_pur_vendor}, 'app.vendor.detail', {id_pur_vendor: $stateParams.id_pur_vendor}, acc);
        }

    })


    .controller('app.purchasing.vendor.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.items = [];
        $scope.mpart = {};
        $scope.param = {};
        $scope.has = ['y', 'n'];
        $scope.status = [{status: "Active"}, {status: "NonActive"}];

        myHelp.getDetail('/purchasing/vendor/create')
            .then(function (respons) {
                $scope.vendor = respons.data.data;
                debugData(respons);

                $scope.param.id_pur_vendor = $scope.vendor.id_pur_vendor;

            });


        myHelp.getDetail('/master/currency')
            .then(function (respons) {
                $scope.currencys = respons.data;
                debugData(respons);
            });

        //persiapan tambah >>>

        $scope.simpanForm = function () {
            var Param = clearObj($scope.vendor);
            myHelp.putParam('/purchasing/vendor/' + $scope.param.id_pur_vendor, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchasing.vendor", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })


    .controller('app.purchasing.vendor.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.items = [];
        $scope.mpart = {};
        $scope.param = {};
        $scope.has = ['y', 'n'];
        $scope.status = [{status: "Active"}, {status: "NonActive"}];

        myHelp.getDetail('/purchasing/vendor/' + $stateParams.id_pur_vendor + '/edit')
            .then(function (respons) {
                $scope.vendor = respons.data.data;
                debugData(respons);

                $scope.param.id_pur_vendor = $scope.vendor.id_pur_vendor;


                myHelp.getDetail('/master/currency')
                    .then(function (respons) {
                        $scope.currencys = respons.data;
                        debugData(respons);
                    });


            });

        //persiapan tambah >>>

        $scope.simpanForm = function () {
            var Param = clearObj($scope.vendor);
            myHelp.putParam('/purchasing/vendor/' + $scope.param.id_pur_vendor, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchasing.vendor", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })



    /*----------------------------------------------------------------------------------------------
     PRCL
     /*----------------------------------------------------------------------------------------------*/


    .controller('app.purchasing.prcl', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.filters = {};
        $scope.id_pur_prcl = [];
        $scope.status_prcl = ['open', 'close'];
        $scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $scope.years = [];
        $scope.BASE_URL = BASE_URL;


        $scope.loadData = false;
        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/qa/prcl', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);

    })

    .controller('app.purchasing.prcl.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.transfer = {};
        $scope.items = [];

        myHelp.getDetail('/qa/transfer/create')
            .then(function (respons) {
                $scope.transfer = respons.data.data;
                $scope.site = respons.data.site;
                debugData(respons);
                $scope.getItems();

            });

        $scope.getItems = function () {
            console.log($scope.id_pur_prcl)
            myHelp.getParam('/qa/transfer_prcl/create', $scope.id_pur_prcl)
                .then(function (respons) {
                    $scope.items = respons.data;
                    debugData(respons);
                });
        }

        $scope.simpanForm = function () {

            if ($scope.items.length < 1) {
                warningView("Part Item Kosong");
                return false;
            }
            $scope.transfer.type_transfer = 'prcl';
            var Obj = {transfer: $scope.transfer, items: $scope.items};
            var Param = clearObj(Obj);
            myHelp.putParam('/qa/transfer/' + $scope.transfer.id_qa_transfer, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.purchasing.prcl", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })

/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/


