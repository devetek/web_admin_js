var app = angular.module('inspinia');



app.controller('setting.users', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){

    $scope.param = {};
    $scope.filters = {};
    $scope.months = [1,2,3,4,5,6,7,8,9,10,11,12];
    $scope.years = [];
    $scope.BASE_URL = BASE_URL;

    var date = new Date();

    for (var a=0; a < 5;a++)
    {
        $scope.years.push(date.getFullYear()-a);
    }

    $scope.loadData = false;
    $scope.filter = function(page)
    {
        $scope.filters.page = page;
        myHelp.getParam('/acl/users',clearObj($scope.filters))
            .then(function(respons){
                $scope.status = respons.data.status;
                $scope.level_user = respons.data.level_user;
                $scope.groups = respons.data.groups;
                $scope.datas = respons.data.data;
                $scope.loadData = true;
            });
    }
    $scope.filter(1);

    $scope.delete = function (id) {
        deleteParam($http, $state, '/acl/user/' + id, {}, 'setting.users', {});
    }

});
app.controller('setting.users.add', function truncateCtrl($scope,$state,$stateParams,myHelp){


    myHelp.getDetail('/acl/users/create')
        .then(function(respons){
            $scope.level_user = respons.data.level_user;
            $scope.groups = respons.data.groups;
            $scope.site = respons.data.site;
            debugData(respons);
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.user);
        myHelp.postParam('/acl/users', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.users",{}, { reload: true })
                }
                , function myError(response)
                {
                    errorView(response.data.status);
                });

    };

});

app.controller('setting.users.edit', function truncateCtrl($http,$scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/acl/users/' + $stateParams.id_user)
    .then(function(respons){
        $scope.user = respons.data;
        $scope.user.password = '';

        myHelp.getDetail('/acl/users/create')
            .then(function(respons){
                $scope.level_user = respons.data.level_user;
                $scope.groups = respons.data.groups;
                $scope.site = respons.data.site;
                debugData(respons);
            });
    });



    $scope.submitForm = function() {

        var file = $scope.myFile;
        var fd = new FormData();
        fd.append('file_td', file);
        fd.append('name', $scope.user.name);
        fd.append('email', $scope.user.email);
        fd.append('password', $scope.user.password);
        fd.append('level_user', $scope.user.level_user);
        fd.append('id_group', $scope.user.id_group);
        fd.append('id_lokasi', clearObj($scope.user.id_lokasi));
        fd.append('id', $scope.user.id);
        fd.append('status', $scope.user.status);

        fd.append('edit', $scope.user.id);

        $http.post(BASE_URL + '/acl/users', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .then(function(){
                berhasilView();
                $state.go("setting.users", {id_user:$stateParams.id_user}, {reload: true});
            })

    };

});

app.controller('setting.groups.add', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.submitForm = function() {
        var Param = clearObj($scope.group);

        myHelp.postParam('/acl/group', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.groups",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

});

app.controller('setting.groups', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){
    $scope.loadData = false;
    myHelp.getDetail('/acl/group')
        .then(function(respons){
            $scope.loadData = true;
            $scope.datas = respons.data;
        });

    $scope.delete = function (id) {
        deleteParam($http, $state, '/acl/group/' + id, {}, 'setting.groups', {});
    }

});


app.controller('setting.groups.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/acl/group/' + $stateParams.id_group)
        .then(function(respons){
            $scope.group = respons.data;
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.group);

        myHelp.putParam('/acl/group/' + $stateParams.id_group, Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.groups",{}, { reload: true })
                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });

    };

});

app.controller('setting.groups.action', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.group_action = [];
    myHelp.getParam('/acl/group_action' ,{id_group:$stateParams.id_group})
        .then(function(respons){
            $scope.detail = respons.data.detail;
            $scope.action = respons.data.data;
            $scope.group_action = respons.data.group_action;
        });

    $scope.ganti_status = function(y,z) {
        var Param = clearObj({action:z, id_group:$stateParams.id_group,id_modul:y});

        myHelp.postParam('/acl/group_action', Param)
            .then(function mySuccesresponse()
                {
                    //berhasilView();
                    //$state.go("setting.groups.action",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

});


app.controller('setting.moduls', function truncateCtrl($scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/acl/modul')
        .then(function(respons){
            $scope.moduls = respons.data;
            debugData(respons);
        });

});
app.controller('setting.controllers', function truncateCtrl($scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/acl/controllers')
        .then(function(respons){
            $scope.controllers = respons.data;
            debugData(respons);
        });

});
app.controller('setting.controllers.add', function truncateCtrl($scope,$state,$stateParams,myHelp){


    myHelp.getDetail('/acl/controllers/add')
        .then(function(respons){
            $scope.master = respons.data;
            debugData(respons);
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.controller);

        myHelp.postParam('/acl/controllers/simpan', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.controllers",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

});

app.controller('setting.controllers.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){


    myHelp.getDetail('/acl/controllers/edit/' + $stateParams.id_controller)
        .then(function(respons){
            $scope.master =respons.data;
            $scope.controller = respons.data.controller;
            $scope.controller.id_modul = respons.data.controller.id_modul*1;
            $scope.controller.no_segment = respons.data.controller.no_segment*1;
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.controller);

        myHelp.postParam('/acl/controllers/simpan-edit', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.controllers",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

});


app.controller('setting.log', function truncateCtrl($scope,$state,$stateParams,myHelp){
    $scope.params = {};
    $scope.filters = function (page) {
        $scope.params.page = page;
        myHelp.getParam('/acl/logs',clearObj($scope.params))
            .then(function(respons){
                $scope.logs = respons.data.data;
                $scope.method = respons.data.method ;
                $scope.users = respons.data.users ;
            });
    }
    $scope.filters(1);


});


/*------------------------- MODUL ------------------------ */

app.controller('setting.modul', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){
    $scope.params = {};
    $scope.filters = function (page) {
        $scope.params.page = page;
        myHelp.getParam('/acl/modul',clearObj($scope.params))
            .then(function(respons){
                $scope.data = respons.data;
            });
    }
    $scope.filters(1);

    $scope.delete_action = function(id_modul,action) {
        deleteParam($http,$state,'/acl/modul_action/1?id_modul='+ id_modul +'&action='+ action +'' , {}, 'setting.modul',{});
    }

});

app.controller('setting.modul.add', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.submitForm = function() {
        var Param = clearObj($scope.modul);

        myHelp.postParam('/acl/modul', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.modul",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });

    };

});
app.controller('setting.modul.add_action', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.action = {};
    $scope.action.id_modul = $stateParams.id_modul;
    $scope.submitForm = function() {
        var Param = clearObj($scope.action);

        myHelp.postParam('/acl/modul_action', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.modul",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });

    };

});
