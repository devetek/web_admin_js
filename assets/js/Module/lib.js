/**
 * INSPINIA - Responsive Admin Theme
 *
 */

/**
 * MainCtrl - controller
 */
function Mpart($state, myHelp, $scope, $localStorage, $rootScope) {
    var param = {};
    $scope.filters = {};
    $scope.datas = [];
    $scope.loadData = false;

    myHelp.getDetail('/share/variabel/partfilter')
        .then(function(respons){
            $scope.master = respons.data;
        });

    $scope.filter = function (page) {
        if($state.current.name !== 'app.lib.mpart')
        {
            return false;
        }
        $scope.loadData = false;

        param = $scope.filters;
        param.page = page;
        myHelp.getParam('/master/mpart',clearObj(param))
            .then(function(respons){
                $scope.datas = respons.data.data.data;
                $scope.loadData = true;
            });

    }
    $scope.delete = function(id)
    {
        deleteParam($http,$state,'/master/mpart/' +id, {}, 'app.lib.mpart',{});
        $scope.filter(1);
    }

    $scope.filter(1);

};


angular
    .module('inspinia')
    .controller('app.lib.mpart', ['$state', 'myHelp', '$scope', '$localStorage', '$rootScope', '$stateParams', Mpart])


.controller('app.lib.mpart.add', function truncateCtrl($scope,$state,$stateParams,myHelp,$timeout,$filter){
    $scope.mpart = {};
    $scope.param={};
    $scope.param.pencarian='';
    $scope.mpart.applicable=[];
    $scope.popup = false;

    $scope.cekIssingle = function()
    {
        $timeout(function () {
            var cek = $filter('filter')($scope.master.partid, { id_partid: clearInt($scope.mpart.id_partid)});
            console.log(cek)
            if(cek.length > 0)
            {
                $scope.mpart.issingle =  cek[0].issingle;
                console.log('aaaa')
            }
            else
            {
                $scope.mpart.issingle = 'n';
            }
        },200)
    }

    myHelp.getDetail('/master/mpart/create')
        .then(function(respons){
            $scope.master = respons.data;
            debugData(respons);
        });

    myHelp.getDetail('/master/variabel/prcl')
        .then(function (responsee) {
            $scope.variabel = responsee.data;
        });

    $scope.popupSearchParent = function () {
        var param = {};
        $scope.popup = true;

        if($scope.param.pencarian.length > 2)
        {
            param.keyword= $scope.param.pencarian;
        }

        myHelp.getParam('/master/mpart',param)
            .then(function(respons){
                $scope.parents = respons.data;
            });
    }

    $scope.closePopup = function()
    {
        console.log('sss')
        $scope.popup = false;
    }

    $scope.selectParent = function(parent,keyword)
    {
        console.log(parent);
        $scope.mpart.parent = parent;
        $scope.keyword=keyword;
        $scope.popup = false;
    }

    $scope.submitForm = function() {
        var Param = clearObj($scope.mpart);
        if($scope.mpart.parent == null)
        {
            warningView('Please Select Parent !');
            return false;
        }

        myHelp.postParam('/master/mpart', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.lib.mpart",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };


})

.controller('app.lib.mpart.edit', function truncateCtrl($scope,$state,$stateParams,myHelp,$timeout,$filter){

    $scope.mpart ={};
    $scope.param = {};
    $scope.popup = false;

    $scope.cekIssingle = function()
    {
        $timeout(function () {
            var cek = $filter('filter')($scope.master.partid, { id_partid: clearInt($scope.mpart.id_partid)});
            console.log(cek)
            if(cek.length > 0)
            {
                $scope.mpart.issingle =  cek[0].issingle;
                console.log('aaaa')
            }
            else
            {
                $scope.mpart.issingle = 'n';
            }
        },200)
    }

    $scope.param.pencarian = '';
    myHelp.getDetail('/master/mpart/'+ $stateParams.id_mpart +'/edit')
        .then(function(respons){
            $scope.mpart = respons.data.mpart;
            $scope.mpart.applicable = respons.data.applicable;
            $scope.keyword = respons.data.mpart.parent;
            $scope.cekIssingle();

            //ambil parent nama
            if(respons.data.mpart.parent==0)
            {
                $scope.keyword = "This is parent";
            }
            else
            {
                myHelp.getDetail('/master/mpart/'+ respons.data.mpart.parent)
                    .then(function(respons){
                        $scope.keyword = respons.data.keyword;

                    });
            }
            myHelp.getDetail('/master/variabel/prcl')
                .then(function (responsee) {
                    $scope.variabel = responsee.data;
                });


            myHelp.getDetail('/master/mpart/create')
                .then(function(respons){
                    $scope.master = respons.data;
                    debugData(respons);
                });
        });

    $scope.popupSearchParent = function () {
        var param = {};
        $scope.popup = true;

        if($scope.param.pencarian.length > 2)
        {
            param.keyword= $scope.param.pencarian;
        }

        myHelp.getParam('/master/mpart',param)
            .then(function(respons){
                $scope.parents = respons.data;
            });
    }

    $scope.closePopup = function()
    {
        console.log('sss')
        $scope.popup = false;
    }

    $scope.selectParent = function(parent,keyword)
    {
        $scope.mpart.parent = parent;
        $scope.keyword=keyword;
        colosePopup();
    }

    $scope.submitForm = function() {
        var Param = clearObj($scope.mpart);

        myHelp.putParam('/master/mpart/' + $stateParams.id_mpart, Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.lib.mpart",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})


/*----------------------------------------------------------------------*/
/*                      ATA
/*----------------------------------------------------------------------*/

.controller('app.lib.ata', function truncateCtrl($http,$scope,$state,$stateParams,myHelp){

    $scope.loadData = false;
    $scope.datas = [];

    $scope.filter = function()
    {
        $scope.loadData = false;
        myHelp.getDetail('/master/ata')
            .then(function(respons){
                $scope.datas = respons.data;
                $scope.loadData = true;
            });
    }
    $scope.filter();

    $scope.delete = function(id) {
        deleteParam($http,$state,'/master/ata/' + id, {}, 'app.lib.ata',{});
    }

    // $scope.delete = function(id)
    // {
    //     myHelp.deleteParam('/master/ata/' + id, {})
    //         .then(function mySuccesresponse()
    //             {
    //                 berhasilView();
    //                 $state.go("app.lib.ata",{}, { reload: true })
    //
    //             }
    //             , function myError(respon)
    //             {
    //                 errorView(respon.data.status);
    //             });
    //     $scope.filter();
    // }

})

.controller('app.lib.ata.add', function truncateCtrl($scope,$state,$stateParams,myHelp){



    $scope.submitForm = function() {
        var Param = clearObj($scope.ata);

        myHelp.postParam('/master/ata', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.lib.ata",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });

    };



})

.controller('app.lib.ata.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.ata = {};
    myHelp.getParam('/master/ata/' + $stateParams.id_ata +'/edit')
        .then(function(respons){
            $scope.ata = respons.data;

            //jiko ado master
            myHelp.getDetail('/master/ata/create')
                .then(function(respons){
                    $scope.master = respons.data;
                    debugData(respons);
                });
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.ata);

        myHelp.putParam('/master/ata/'+ $stateParams.id_ata, Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.lib.ata",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
        
    };

})

/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/
.controller('app.lib.maintenance', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){

    $scope.loadData = false;
    $scope.filter = function()
    {
        myHelp.getDetail('/master/maintenance')
            .then(function(respons){
                $scope.datas = respons.data;
                $scope.loadData = true;
            });
    }

    $scope.filter();

    $scope.delete = function(id) {
        deleteParam($http,$state,'/master/maintenance/' + id, {}, 'app.lib.maintenance',{});
    }

})

.controller('app.lib.maintenance.add', function truncateCtrl($scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/master/maintenance/create')
        .then(function(respons){
            $scope.master = respons.data;
            debugData(respons);
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.maintenance);

        myHelp.postParam('/master/maintenance', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.lib.maintenance",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})


.controller('app.lib.maintenance.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.maintenance = {};
    myHelp.getParam('/master/maintenance/' + $stateParams.id_maintenance_code +'/edit')
        .then(function(respons){
            $scope.maintenance = respons.data;

            //jiko ado master
            myHelp.getDetail('/master/maintenance/create')
                .then(function(respons){
                    $scope.master = respons.data;
                    debugData(respons);
                });
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.maintenance);

        myHelp.putParam('/master/maintenance/'+ $stateParams.id_maintenance_code, Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.lib.maintenance",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };



})


.controller('app.lib.maintenance.detail', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.maintenance = {};
    $scope.references = [];
    $scope.m_reference = [];
    myHelp.getParam('/master/maintenance/' + $stateParams.id_maintenance_code +'/edit')
        .then(function(respons){
            $scope.maintenance = respons.data;


        });

    /*----- Reference ----*/
    $scope.maintenance_reference = function () {
        myHelp.getDetail('/master/maintenance_reference/' + $stateParams.id_maintenance_code)
            .then(function(respons){
                $scope.references = respons.data.references;
                $scope.m_reference = respons.data.reference;
            });
    }
    $scope.maintenance_reference();

    $scope.reference = {};
    $scope.add_reference = function() {

        $scope.reference.id_maintenance_code = $stateParams.id_maintenance_code;
        var Param = clearObj($scope.reference);
        myHelp.postParam('/master/maintenance_reference', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.maintenance_reference();

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

    $scope.deleteReference = function(id)
    {
        myHelp.deleteParam('/master/maintenance_reference/' + id, {})
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.maintenance_reference();

                }
                , function myError()
                {
                    errorView("gagal hapus, data tidak ditemukan");
                });
    }


    /*----- Form Issued ----*/
    $scope.maintenance_form_issued = function () {
        myHelp.getDetail('/master/maintenance_form_issued/' + $stateParams.id_maintenance_code)
            .then(function(respons){
                $scope.form_issueds = respons.data.form_issueds;
                $scope.m_form_issued = respons.data.form_issued;
            });
    }
    $scope.maintenance_form_issued();

    $scope.form_issued = {};
    $scope.add_form_issued = function() {

        $scope.form_issued.id_maintenance_code = $stateParams.id_maintenance_code;
        var Param = clearObj($scope.form_issued);
        myHelp.postParam('/master/maintenance_form_issued', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.maintenance_form_issued();

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

    $scope.deleteform_issued = function(id)
    {
        myHelp.deleteParam('/master/maintenance_form_issued/' + id, {})
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.maintenance_form_issued();

                }
                , function myError()
                {
                    errorView("gagal hapus, data tidak ditemukan");
                });
    }
    /*----- end ----*/
 

})


/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/
    .controller('app.lib.mel', function truncateCtrl($scope,$state,$stateParams,myHelp){

        $scope.loadData = false;
        $scope.filter = function()
        {
            myHelp.getDetail('/master/mel')
                .then(function(respons){
                    $scope.datas = respons.data;
                    $scope.loadData = true;
                });
        }

        $scope.filter();
        $scope.delete = function(id)
        {
            myHelp.deleteParam('/master/mel/' + id, {})
                .then(function mySuccesresponse()
                    {
                        berhasilView();
                        $state.go("app.lib.mel",{}, { reload: true })

                    }
                    , function myError()
                    {
                        errorView("gagal hapus, data tidak ditemukan");
                    });
        }

    })

    .controller('app.lib.mel.add', function truncateCtrl($scope,$state,$stateParams,myHelp){

        myHelp.getDetail('/master/mel/create')
            .then(function(respons){
                $scope.master = respons.data;
                debugData(respons);
            });

        $scope.submitForm = function() {
            var Param = clearObj($scope.mel);

            myHelp.postParam('/master/mel', Param)
                .then(function mySuccesresponse()
                    {
                        berhasilView();
                        $state.go("app.lib.mel",{}, { reload: true })

                    }
                    , function myError()
                    {
                        errorView("Error data entry (400)");
                    });


        };

    })


    .controller('app.lib.mel.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){

        $scope.mel = {};
        myHelp.getParam('/master/mel/' + $stateParams.mel +'/edit')
            .then(function(respons){
                $scope.mel = respons.data;
                $scope.mel.day = respons.data.day*1;

            });

        $scope.submitForm = function() {
            var Param = clearObj($scope.mel);

            myHelp.putParam('/master/mel/'+ $stateParams.mel, Param)
                .then(function mySuccesresponse()
                    {
                        berhasilView();
                        $state.go("app.lib.mel",{}, { reload: true })

                    }
                    , function myError()
                    {
                        errorView("Error data entry (400)");
                    });
        };

    })


/*----------------------------------------------------------------------*/
/*                      REFERENCE
/*----------------------------------------------------------------------*/
.controller('app.lib.reference', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.loadData = false;
    myHelp.getDetail('/master/reference')
        .then(function(respons){
            $scope.datas = respons.data;
            $scope.loadData = true;
        });

    $scope.delete = function(id)
    {
        myHelp.deleteParam('/master/reference/' + id, {})
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.lib.reference",{}, { reload: true })

                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    }

})

.controller('app.lib.reference.add', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){


    $scope.submitForm = function() {

        var file = $scope.myFile;
        var fd = new FormData();
        fd.append('name_file', file);
        fd.append('number_reference', $scope.reference.number_reference);
        fd.append('last_update', $scope.reference.last_update);


        $http.post(BASE_URL + '/master/reference', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function(respons){
            berhasilView();
            $state.go("^", {}, {reload: true});
        }, function (err) {
            errorView()
        });


    };

})


.controller('app.lib.reference.edit', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){
    console.log("Asdsa")
    myHelp.getDetail('/master/reference/' + $stateParams.id_lib_reference)
        .then(function(respons){
            $scope.reference = respons.data;
            debugData(respons);
        });

    $scope.submitForm = function() {

        var file = $scope.myFile;
        var fd = new FormData();
        fd.append('name_file', file);
        fd.append('number_reference', $scope.reference.number_reference);
        fd.append('last_update', $scope.reference.last_update);
        fd.append('id_lib_reference', $scope.reference.id_lib_reference);

        fd.append('edit', 'edit');

        $http.post(BASE_URL + '/master/reference', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .then(function(respons){
                berhasilView();
                $state.go("^", {}, {reload: true});
            }, function (err) {
                errorView()
            });

    };

})

/*----------------------------------------------------------------------*/
/*                      LIB FORM
/*----------------------------------------------------------------------*/
.controller('app.lib.form_issued', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.loadData = false;
    myHelp.getDetail('/master/form_issued')
        .then(function(respons){
            $scope.datas = respons.data;
            $scope.loadData = true;
        });

    $scope.delete = function(id)
    {
        myHelp.deleteParam('/master/form_issued/' + id, {})
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.lib.form_issued",{}, { reload: true })

                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    }

})

.controller('app.lib.form_issued.add', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){

    $scope.submitForm = function() {

        var file = $scope.myFile;
        var fd = new FormData();
        fd.append('name_file', file);
        fd.append('number_form', $scope.form_issued.number_form);
        fd.append('number_w', $scope.form_issued.number_w);
        fd.append('last_update', $scope.form_issued.last_update);
        fd.append('revision', $scope.form_issued.revision);


        $http.post(BASE_URL + '/master/form_issued', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .then(function(respons){
                berhasilView();
                $state.go("^", {}, {reload: true});
            }, function (err) {
                errorView()
            });

    };



})


.controller('app.lib.form_issued.edit', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){
    console.log("Asdsa")
    myHelp.getDetail('/master/form_issued/' + $stateParams.id_lib_form_issued)
        .then(function(respons){
            $scope.form_issued = respons.data;
            debugData(respons);
        });

    $scope.submitForm = function() {

        var file = $scope.myFile;
        var fd = new FormData();
        fd.append('name_file', file);
        fd.append('number_form', $scope.form_issued.number_form);
        fd.append('number_w', $scope.form_issued.number_w);
        fd.append('last_update', $scope.form_issued.last_update);
        fd.append('revision', $scope.form_issued.revision);
        fd.append('id_lib_form_issued', $scope.form_issued.id_lib_form_issued);

        fd.append('edit', 'edit');

        $http.post(BASE_URL + '/master/form_issued', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function(respons){
            berhasilView();
            $state.go("^", {}, {reload: true});
        }, function (err) {
            errorView()
        });

    };



})
