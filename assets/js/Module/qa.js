angular
    .module('inspinia')



    /*----------------------------------------------------------------------*/
    /*                      SITE / STORE
    /*----------------------------------------------------------------------*/

    .controller('app.qa.transfer', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.filters = {};
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getParam('/qa/transfer', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);
    })

    .controller('app.qa.transfer.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/qa/transfer/' + $stateParams.id_qa_transfer)
            .then(function (responsee) {
                $scope.transfer = responsee.data.data;
                $scope.items = responsee.data.items;
            })

    })

    .controller('app.qa.transfer.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.transfer = {};
        $scope.items = [];

        myHelp.getDetail('/qa/transfer/' + $stateParams.id_qa_transfer + '/edit')
            .then(function (respons) {
                $scope.transfer = respons.data.data;
                $scope.site = respons.data.site;
                $scope.items = respons.data.items;

            });


        $scope.simpanForm = function () {

            if ($scope.items.length < 1) {
                warningView("Part Item Kosong");
            }

            var Obj = {transfer: $scope.transfer, items: $scope.items};
            var Param = clearObj(Obj);
            myHelp.putParam('/qa/transfer/' + $scope.transfer.id_qa_transfer, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.qa.transfer", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })


/*----------------------------------------------------------------------*/
/*                      RPD
/*----------------------------------------------------------------------*/

.controller('app.qa.rpd.add', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {
    $scope.rpd = {};
    $scope.id_engineer_rti = [];
    $scope.type = 'all';

    $scope.tipe = 'rti';
    $scope.type = 'rti';
    $scope.title = 'rpd';
    if ($stateParams.type == 'xpd') {
        $scope.tipe = 'espd';
        $scope.type = 'xpd';
        $scope.title = 'XPD';
    }
    myHelp.getParam('/qa/rpd/create', {tipe: $stateParams.type})
        .then(function (respons) {
            $scope.rpd = respons.data.rpd;
        });

    $scope.getItems = function () {
        myHelp.getParam('/qa/rpd_item/create', {items: $scope.id_engineer_rti.join()})
            .then(function (respons) {
                $scope.preeadd = respons.data.preeadd;
                $scope.rpd.preeadd = respons.data.preeadd;
                $scope.dispotition = respons.data.dispotition;

            });
    }


    $scope.add_item = function () {
        $scope.getItems();
        $state.go("^", {})
        // myHelp.getParam('/qa/rpd_item/create',{id_engineer_rpd:$scope.id_engineer_rti, items:$scope.id_engineer_rti})
        //     .then(function mySuccesresponse() {
        //             $scope.getItems();
        //             $state.go("^", {})
        //         }
        //         , function myError() {
        //             errorView("Error data entry (400)");
        //         });
    }


    $scope.submitForm = function () {

        for (var a = 0; a < $scope.preeadd.length; a++) {
            if (!$scope.preeadd[a].dispotition) {
                errorView("Please Select Dispotition " + $scope.preeadd[a].part_name);
                return false;
            }
        }

        myHelp.postParam('/qa/rpd', clearObj($scope.rpd))
            .then(function mySuccesresponsee() {
                    berhasilView();
                    $state.go('^', {}, {reload: "app.qa.rpd"});
                }
                , function myError() {
                    errorView("Error data entry (400)");
                });
    };


})

.controller('app.qa.rpd', function truncateCtrl($scope, $state, $stateParams, myHelp, $window, $rootScope) {
    $scope.jo = {};
    $scope.filters = {};

    $scope.loadData = false;

    $scope.filter = function (page) {
        $scope.filters.page = page;
        myHelp.getParam('/qa/rpd', clearObj($scope.filters))
            .then(function (respons) {
                $scope.datas = respons.data.data;
                $scope.filters = respons.data.request;
                $scope.loadData = true;
            });
    }
    $scope.filter(1);

    $scope.delete = function (id) {
        myHelp.deleteConfirm('/qa/rpd/' + id, {}, 'app.qa.rpd', {});
    }

})


.controller('app.qa.rpd.edit', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {

    $scope.rpd = {};
    $scope.preeadd = [];

    myHelp.getDetail('/qa/rpd/' + $stateParams.id_qa_rpd + '/edit')
        .then(function (respons) {
            $scope.rpd = respons.data.rpd;
            $scope.preeadd = respons.data.preeadd;
            $scope.dispotition = respons.data.dispotition;
        });


    $scope.submitForm = function () {
        $scope.rpd.preeadd = $scope.preeadd;

        for (var a = 0; a < $scope.preeadd.length; a++) {
            if (!$scope.preeadd[a].dispotition) {
                errorView("Please Select Dispotition " + $scope.preeadd[a].part_name);
                return false;
            }
        }

        myHelp.putParam('/qa/rpd/' + $stateParams.id_qa_rpd, clearObj($scope.rpd))
            .then(function mySuccesresponsee() {
                    berhasilView();
                    $state.go('^', {}, {reload: "app.qa.rpd"});
                }
                , function myError() {
                    errorView("Error data entry (400)");
                });
    };


})


.controller('app.qa.rpd.detail', function truncateCtrl($scope, $state, $stateParams, myHelp) {
    myHelp.getDetail('/qa/rpd/' + $stateParams.id_qa_rpd)
        .then(function (respons) {
            $scope.rpd = respons.data.rpd;
            $scope.rti = respons.data.rti;
        });
})

//
// .controller('app.qa.rpd.edit', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {
//     $scope.rpd = {};
//
//     myHelp.getDetail('/qa/rpd/edit' + $stateParams.id_qa_rpd)
//         .then(function (respons) {
//             $scope.dispotition = respons.data.dispotition;
//
//             myHelp.getDetail('/qa/rti/' + $stateParams.id_engineer_rti)
//                 .then(function (respons) {
//                     $scope.rti = respons.data.rti;
//                     $scope.user = respons.data.user;
//                 });
//         });
//
//     $scope.submitForm = function () {
//         $scope.rpd.id_engineer_rti = $stateParams.id_engineer_rti;
//         myHelp.putParam('/qa/rpd', clearObj($scope.rpd))
//             .then(function mySuccesresponsee() {
//                     berhasilView();
//                     $state.go('^', {reload: "app.rdp"});
//                 }
//                 , function myError() {
//                     errorView("Error data entry (400)");
//                 });
//     };
//
// })

.controller('app.qa.rpd_item', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {
    $scope.filters = {};
    $scope.BASE_URL = BASE_URL;
    $scope.loadData = false;

    $scope.filter = function (page) {
        $scope.filters.page = page;
        myHelp.getParam('/qa/rpd_item', clearObj($scope.filters))
            .then(function (respons) {
                $scope.datas = respons.data.data;
                $scope.filters = respons.data.request;
                $scope.dispotition = respons.data.dispotition;
                $scope.to_position = respons.data.to_position;
                $scope.loadData = true;
            });
    }
    $scope.filter(1);

    $scope.sent_to_purchasing = function (acc) {
        PindahParam($http, $state, '/mpc/rpd_item/' + acc, {
            id_qa_rpd: acc,
            to: 'purchasing'
        }, 'app.qa.rpd_item', {}, 'To Purchasing');
    }
    $scope.sent_gse = function (acc) {
        PindahParam($http, $state, '/mpc/rpd_item/' + acc, {id_qa_rpd: acc, to: 'gse'}, 'app.qa.rpd_item', {}, 'To GSE');
    }
    $scope.back_mpc = function (acc) {
        PindahParam($http, $state, '/mpc/rpd_item/' + acc, {id_qa_rpd: acc, to: 'mpc'}, 'app.qa.rpd_item', {}, 'To MPC');
    }


    $scope.sent_to_serviceable = function (acc) {
        PindahParam($http, $state, '/qa/fom_serviceable/' + acc, {
            id_engineer_rti: acc,
            to: 'store'
        }, 'app.qa.rpd_item', {}, 'Serviceable');
    }

})


.controller('app.qa.xpd_item', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {
    $scope.filters = {};
    $scope.BASE_URL = BASE_URL;
    $scope.loadData = false;

    $scope.filter = function (page) {
        $scope.filters.page = page;
        myHelp.getParam('/qa/xpd_item', clearObj($scope.filters))
            .then(function (respons) {
                $scope.datas = respons.data.data;
                $scope.filters = respons.data.request;
                $scope.dispotition = respons.data.dispotition;
                $scope.to_position = respons.data.to_position;
                $scope.loadData = true;
            });
    }
    $scope.filter(1);

    $scope.sent_to_purchasing = function (acc) {
        PindahParam($http, $state, '/mpc/rpd_item/' + acc, {
            id_qa_rpd: acc,
            to: 'purchasing'
        }, 'app.qa.rpd_item', {}, 'To Purchasing');
    }
    $scope.sent_gse = function (acc) {
        PindahParam($http, $state, '/mpc/rpd_item/' + acc, {id_qa_rpd: acc, to: 'gse'}, 'app.qa.rpd_item', {}, 'To GSE');
    }
    $scope.back_mpc = function (acc) {
        PindahParam($http, $state, '/mpc/rpd_item/' + acc, {id_qa_rpd: acc, to: 'mpc'}, 'app.qa.rpd_item', {}, 'To MPC');
    }


    $scope.sent_to_serviceable = function (acc) {
        PindahParam($http, $state, '/qa/fom_serviceable/' + acc, {
            id_engineer_rti: acc,
            to: 'store'
        }, 'app.qa.rpd_item', {}, 'Serviceable');
    }

})


.controller('app.qa.rpd_item.task_card', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){
    $scope.maintenance = {};
    $scope.jenis = 'rti';
    myHelp.getParam('/mpc/self_maintenance/create', {id_engineer_rti:$stateParams.id_engineer_rti})
        .then(function(respons){
            $scope.maintenance = respons.data.maintenance;
            $scope.rti = respons.data.rti;
        });

    $scope.submitForm = function() {
        var file = $scope.myFile;
        var fd = new FormData();
        fd.append('file_td', file);
        fd.append('id_mpc_self_maintenance', $scope.maintenance.id_mpc_self_maintenance);
        fd.append('date_maintenance', $scope.maintenance.date_maintenance);
        fd.append('report_maintenance', $scope.maintenance.report_maintenance);
        fd.append('id_engineer_rti', $stateParams.id_engineer_rti);
        fd.append('number_maintenance', $scope.maintenance.number_maintenance);

        $http.post(BASE_URL + '/mpc/self_maintenance', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (suc) {
            berhasilView();
            $state.go('^',{}, { reload: "app.qa.rpd_item" })
        })

        //     var Param = clearObj($scope.maintenance);
        //     myHelp.postParam('/mpc/self_maintenance' ,Param)
        //         .then(function mySuccesresponse()
        //             {
        //                 berhasilView();
        //                 $state.go('^',{}, { reload: "app.rpd_item" })
        //
        //             }
        //             , function myError()
        //             {
        //                 errorView("Error data entry (400)");
        //             });
    };
})

.controller('app.qa.xpd_item.task_card', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){
    $scope.jenis = 'xpd';
    $scope.maintenance = {};
    myHelp.getParam('/mpc/self_maintenance/create', {id_engineer_rti:$stateParams.id_engineer_rti})
        .then(function(respons){
            $scope.maintenance = respons.data.maintenance;
            $scope.rti = respons.data.rti;
        });

    $scope.submitForm = function() {
        var file = $scope.myFile;
        var fd = new FormData();
        fd.append('file_td', file);
        fd.append('id_mpc_self_maintenance', $scope.maintenance.id_mpc_self_maintenance);
        fd.append('date_maintenance', $scope.maintenance.date_maintenance);
        fd.append('report_maintenance', $scope.maintenance.report_maintenance);
        fd.append('id_engineer_rti', $stateParams.id_engineer_rti);

        $http.post(BASE_URL + '/mpc/self_maintenance', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (suc) {
            berhasilView();
            $state.go('^',{}, { reload: "app.qa.xpd_item" })
        })

        //     var Param = clearObj($scope.maintenance);
        //     myHelp.postParam('/mpc/self_maintenance' ,Param)
        //         .then(function mySuccesresponse()
        //             {
        //                 berhasilView();
        //                 $state.go('^',{}, { reload: "app.rpd_item" })
        //
        //             }
        //             , function myError()
        //             {
        //                 errorView("Error data entry (400)");
        //             });
    };
})


.controller('app.qa.rpd_item.destroy', function ($scope, $state, $stateParams, myHelp, $http) {
    $scope.BASE_URL = BASE_URL;
    $scope.destroy = {};

    myHelp.getParam('/store/destroy/create', {id_engineer_rti: $stateParams.id_engineer_rti})
        .then(function (respons) {
            $scope.rti = respons.data.rti;
            $scope.destroy.ref_rti = $scope.rti.id_engineer_rti;
            $scope.destroy.id_part = $scope.rti.id_part;
        });

    $scope.submitForm = function () {

        myHelp.postParam('/store/destroy', $scope.destroy)
            .then(function mySuccesresponsee() {
                    berhasilView();
                    $state.go('^', {}, {reload: true});

                }
                , function myError() {
                    errorView("Error data entry (400)");
                });
    };

})


.controller('app.qa.xpd_item.destroy', function ($scope, $state, $stateParams, myHelp, $http) {
    $scope.BASE_URL = BASE_URL;
    $scope.destroy = {};

    myHelp.getParam('/store/destroy/create', {id_engineer_rti: $stateParams.id_engineer_rti})
        .then(function (respons) {
            $scope.rti = respons.data.rti;
            $scope.destroy.ref_rti = $scope.rti.id_engineer_rti;
            $scope.destroy.id_part = $scope.rti.id_part;
        });

    $scope.submitForm = function () {

        myHelp.postParam('/store/destroy', $scope.destroy)
            .then(function mySuccesresponsee() {
                    berhasilView();
                    $state.go('^', {}, {reload: true});

                }
                , function myError() {
                    errorView("Error data entry (400)");
                });
    };

})



/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/


