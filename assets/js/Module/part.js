
angular
    .module('inspinia') 



/*----------------------------------------------------------------------*/
/*                     DETAIL
/*----------------------------------------------------------------------*/
.controller('app.part.detail', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope){
    $scope.part = {};
    myHelp.getDetail('/umum/part/' + $stateParams.id_part)
        .then(function(respons){
            $scope.part = respons.data.part;
            $scope.time = respons.data.time;

        });
})

.controller('app.part.detail.life', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope){

    $scope.UpdateLife = function()
    {
        myHelp.postParam('/umum/part/lifetime', $scope.part)
            .then(function mySuccesresponsee()
                {
                    berhasilView();
                    $state.go("^",{},{reload:true});

                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    }

})

.controller('app.part.detail.update', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope){
    $scope.update_part = {};
    myHelp.getDetail('/umum/part/update/' + $stateParams.id_part  )
        .then(function (responsee) {
            $scope.update_part = responsee.data.part;
            console.log(responsee.data)

            myHelp.getParam('/purchasing/vendor' , {is_manufacture:'y',ispage:'n'}  )
                .then(function (responsee) {
                    $scope.vendor = responsee.data.vendors;
                });

            myHelp.getDetail('/master/part_condition')
                .then(function (responsee) {
                    $scope.part_condition = responsee.data;
                });

            myHelp.getDetail('/master/life_time_limit')
                .then(function (responsee) {
                    $scope.life_time_limit = responsee.data;
                });

            myHelp.getDetail('/master/condition_monitoring')
                .then(function (responsee) {
                    $scope.condition_monitoring = responsee.data;
                });

            myHelp.getDetail('/master/variabel/prcl')
                .then(function (responsee) {
                    $scope.variabel = responsee.data;
                });

        });



    $scope.updatePart = function()
    {
        myHelp.postParam('/umum/part/update', clearObj($scope.update_part))
            .then(function mySuccesresponsee()
                {
                    berhasilView();
                    $state.go("^",{},{reload:true});

                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    }

})

.controller('app.part.detail.last_maintenance', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope,$http){

    $scope.filters = {};

    $scope.loadData = false;
    $scope.filter = function(page)
    {
        $scope.filters.page = page;
        myHelp.getParam('/engineer/last_maintenance/' + $stateParams.id_part ,clearObj($scope.filters))
            .then(function(respons){
                $scope.datas = respons.data.data;
                $scope.maintenance = respons.data.maintenance;
                $scope.filters = respons.data.data.request;
                $scope.loadData = true;
            });
    }
    $scope.filter(1);

    $scope.remove = function(id_last_maintenance)
    {
        deleteParam($http,$state,'/engineer/last_maintenance/' + id_last_maintenance, {}, 'app.part.detail.last_maintenance',{id_part:$stateParams.id_part});
    }

})


.controller('app.part.detail.last_maintenance.add', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope){
    $scope.last_maintenance = {};


    myHelp.getParam('/engineer/last_maintenance/create',{id_part:$stateParams.id_part})
        .then(function(respons){
            $scope.he = respons.data.he;
            if(respons.data.hasOwnProperty('parent'))
            {
                $scope.last_maintenance.parent_cycle = respons.data.parent.cycle;
                $scope.last_maintenance.parent_hours = respons.data.parent.hours;

            }

            $scope.maintenance_code = respons.data.maintenance_code;
            $scope.type_ofwork = respons.data.type_ofwork;

            $scope.last_maintenance.last_cycle = 1*$scope.he.tsn_cycle;
            $scope.last_maintenance.last_hours = 1*$scope.he.tsn_hours;

        });

    $scope.show = false;
    $scope.showxxx = function()
    {
        if(clearInt($scope.last_maintenance.type_of_work)==3)
        {
            $scope.show = true;
        }
        else
        {
            $scope.show = false;
        }
    }



    $scope.Insert_last_maintenance = function() {
        $scope.last_maintenance.id_part = $stateParams.id_part;

        if($scope.last_maintenance.type_of_work==3)
        {
            if($scope.last_maintenance.parent_cycle.length < 1)
            {
                errorView("TSN paren Must > 0");
                return false;
            }
            if($scope.last_maintenance.parent_hours.length < 1)
            {
                errorView("TSN paren Must > 0");
                return false;
            }
        }
        myHelp.postParam('/engineer/last_maintenance', clearObj($scope.last_maintenance))
            .then(function mySuccesresponsee()
                {
                    berhasilView();
                    $state.go('app.part.detail.last_maintenance',{id_part:$stateParams.id_part},{reload:true});
                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    };



})

.controller('app.part.detail.location', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope){
    $scope.position = {};
    $scope.bins = [];
    myHelp.getDetail('/umum/filter_part/location/' + $stateParams.id_part)
        .then(function(respons){
            $scope.location = respons.data.location;
            if(respons.data.hasOwnProperty('bins'))
            {
                $scope.bins = respons.data.bins;
            }
        });

    myHelp.getDetail('/stock/position/create')
        .then(function(respons){
            $scope.site = respons.data.site;
        });

    $scope.UpdatePosition = function() {
        $scope.position.id_part = $stateParams.id_part;

        if($scope.part.issingle=='n')
        {
            if($scope.part.qty < $scope.position.qty)
            {
                errorView("MAX qty must  " + $scope.part.qty);
                return false;
            }
        }
        myHelp.postParam('/stock/position', clearObj($scope.position))
            .then(function mySuccesresponsee()
                {
                    berhasilView();
                    $state.go('app.part.detail.location',{id_part:$stateParams.id_part},{reload:true});
                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    };

    $scope.UpdatePositionBin = function() {
        $scope.position.id_part = $stateParams.id_part;
        myHelp.postParam('/umum/part/bin', clearObj($scope.position))
            .then(function mySuccesresponsee()
                {
                    berhasilView();
                    $state.go('app.part.detail.location',{id_part:$stateParams.id_part},{reload:true});
                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    };



})

.controller('app.part.detail.history', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope){
    $scope.position = {};
    myHelp.getDetail('/umum/history/' + $stateParams.id_part)
        .then(function(respons){
            $scope.history = respons.data.history;
        });
})

.controller('app.part.detail.component', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope){


    myHelp.getDetail('/umum/part/component/' + $stateParams.id_part + '/' + $scope.part.status_on_inventory)
        .then(function(respons){
            $scope.component = respons.data.component;
            $scope.treeData = respons.data.tree;
            $scope.treeConfig = {
                core : {
                    multiple : false,
                    animation: true,
                    error : function(error) {
                        $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                    },
                    check_callback : true,
                    worker : true
                },
                version : 1
            };
            $scope.reCreateTree = function() {
                this.treeConfig.version++;
            }

            $scope.treeEventsObj = {
                'ready': readyCB,
                'create_node': createNodeCB
            }

            function readyCB() {
                $log.info('ready called');

            };

            function createNodeCB(e,item) {
                $log.info('create_node called');
            };
        });


})


.controller('app.part.detail.espd', function truncateCtrl($scope, $state, $stateParams, myHelp, $http, $log) {
    $scope.rti = {};
    $scope.rti.sn = "Please Select Part";
    $scope.rti.id_part == null;
    $scope.status_service = ['serviceable', 'unserviceable', 'reject'];

    myHelp.getParam('/store/espd/create' ,{id_part:$stateParams.id_part})
        .then(function (respons) {
            console.log(respons.data)
            $scope.rti.sn = respons.data.sn;
            $scope.rti.id_part = respons.data.id_part;
            $scope.rti.base = respons.data.site;
            $scope.rti.reason = respons.data.reason;
        });

    $scope.submitForm = function () {
        if ($scope.rti.id_part == null) {
            errorView("Please Select Part");
            return false;
        }
        $scope.rti.tipe = 'injek';

        $scope.rti.id_site = $scope.part.id_site;
        myHelp.postParam('/store/espd', {rti:$scope.rti})
            .then(function mySuccesresponsee() {
                    berhasilView();
                    $state.go('^', {}, {reload: "app.part"});
                }
                , function myError() {
                    errorView("Error data entry (400)");
                });
    };


})
/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/


