angular
    .module('inspinia')



    /*----------------------------------------------------------------------*/
    /*                      RTI
    /*----------------------------------------------------------------------*/
    .controller('app.maintenance.rti.add', function truncateCtrl($scope, $state, $stateParams, myHelp, $http, $log) {


        $scope.rti = {};
        $scope.identifys = [];

        $scope.rti.sn = "Please Select Part";
        $scope.rti.id_part == null;
        $scope.status_service = ['serviceable', 'unserviceable', 'reject'];

        myHelp.getParam('/engineer/rti/create', {})
            .then(function (respons) {
                $scope.jo = respons.data.jo;
                $scope.ac = respons.data.ac;
            });


        //---------jstrree
        $scope.ignoreModelChanges = function () {
            return true;
        }
        $scope.treeData = [];
        $scope.treeConfig = {
            core: {
                multiple: false,
                animation: true,
                error: function (error) {
                    $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                },
                check_callback: true,
                worker: true
            },
            version: 1
        };
        $scope.reCreateTree = function () {
            $log.info('rsdsds');
            this.treeConfig.version++;
        }

        $scope.treeEventsObj = {
            'ready': readyCB,
            'create_node': createNodeCB
        }


        function readyCB() {
            $log.info('ready called');

        };

        function createNodeCB(e, item) {
            $log.info('create_node called');
        };

        $scope.setPart = function () {
            var node = $scope.treeInstance.jstree(true).get_selected();
            if (node.length > 0) {
                myHelp.getDetail('/share/part/' + node[0] + '?install=y')
                    .then(function (respons) {
                        $scope.rti.id_part = respons.data.id_part;
                        $scope.rti.sn = respons.data.sn + ' ' + respons.data.keyword;
                        $scope.rti.id_part_install = node[0];
                    });

            }
            else {
                $scope.rti.id_part = null;
                $scope.rti.sn = "This Not have Selected Part";
                $scope.rti.id_part_install = null;
            }
        }

        $scope.setIdentify = function () {
            var node = $scope.treeInstance.jstree(true).get_selected();
            if (node.length > 0) {
                myHelp.getDetail('/share/part/' + node[0] + '?install=y')
                    .then(function (respons) {

                        var identify = {};
                        identify.id_part = respons.data.id_part;
                        identify.part_number = respons.data.part_number;
                        identify.keyword = respons.data.keyword;
                        identify.sn = respons.data.sn;
                        identify.id_part_install = respons.data.id_part_install;

                        $scope.identifys.push(identify);
                    });

            }


        }

        //---------jstrree

        $scope.filterAircraft = function () {
            myHelp.getParam('/share/tree_view', {id_aircraft: $scope.rti.id_aircraft})
                .then(function (respons) {
                    $scope.reCreateTree();
                    $scope.treeData = respons.data.data;


                });

            $scope.removeIdentify = function (urutan) {
                $scope.identifys.splice(urutan - 1, 1);
            }


        }


        $scope.submitForm = function () {
            if ($scope.rti.id_part == null) {
                errorView("Please Select Part");
                return false;
            }
            myHelp.postParam('/engineer/rti', {tipe: 'injek', rti: $scope.rti, identify: $scope.identifys})
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $state.go('^', {}, {reload: "app.maintenance.rti"});

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    .controller('app.maintenance.rti.edit', function truncateCtrl($scope, $state, $stateParams, myHelp, $http, $log) {


        $scope.rti = {};
        $scope.identifys = [];
        $scope.rti.sn = "Please Select Part";
        $scope.rti.id_part == null;
        $scope.status_service = ['serviceable', 'unserviceable', 'reject'];

        myHelp.getDetail('/engineer/rti/' + $stateParams.id_engineer_rti + '/edit')
            .then(function (respons) {
                $scope.jo = respons.data.jo;
                $scope.ac = respons.data.ac;
                $scope.rti = respons.data.rti;
                $scope.identifys = respons.data.identify;

                $scope.filterAircraft();
            });

        $scope.filterAircraft = function () {
            console.log('dsdsds');
            myHelp.getParam('/share/tree_view', {id_aircraft: $scope.rti.id_aircraft})
                .then(function (respons) {

                    $scope.treeData = respons.data.data;
                    $scope.treeConfig = {
                        core: {
                            multiple: false,
                            animation: true,
                            error: function (error) {
                                $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                            },
                            check_callback: true,
                            worker: true
                        },
                        version: 1
                    };
                    $scope.reCreateTree = function () {
                        this.treeConfig.version++;
                    }

                    $scope.treeEventsObj = {
                        'ready': readyCB,
                        'create_node': createNodeCB
                    }

                    function readyCB() {
                        $log.info('ready called');

                    };

                    function createNodeCB(e, item) {
                        $log.info('create_node called');
                    };

                    $scope.setPart = function () {
                        var node = $scope.treeInstance.jstree(true).get_selected();
                        if (node.length > 0) {
                            myHelp.getDetail('/share/part/' + node[0] + '?install=y')
                                .then(function (respons) {
                                    $scope.rti.id_part = respons.data.id_part;
                                    $scope.rti.sn = respons.data.sn + ' ' + respons.data.keyword;
                                });

                        }
                        else {
                            $scope.rti.id_part = null;
                            $scope.rti.sn = "This Not have Selected Part";
                        }


                    }

                    $scope.setIdentify = function () {
                        var node = $scope.treeInstance.jstree(true).get_selected();
                        if (node.length > 0) {
                            myHelp.getDetail('/share/part/' + node[0] + '?install=y')
                                .then(function (respons) {

                                    var identify = {};
                                    identify.id_part = respons.data.id_part;
                                    identify.part_number = respons.data.part_number;
                                    identify.keyword = respons.data.keyword;
                                    identify.sn = respons.data.sn;
                                    identify.id_part_install = respons.data.id_part_install;

                                    $scope.identifys.push(identify);
                                });

                        }


                    }

                });

            $scope.removeIdentify = function (urutan) {
                $scope.identifys.splice(urutan - 1, 1);
            }

        }

        $scope.submitForm = function () {

            if ($scope.rti.id_part == null) {
                errorView("Please Select Part");
                return false;
            }
            myHelp.putParam('/engineer/rti/' + $scope.rti.id_engineer_rti, {
                tipe: 'injek',
                rti: $scope.rti,
                identify: $scope.identifys
            })
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $state.go('^', {}, {reload: "app.maintenance.rti"});

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    .controller('app.maintenance.rti', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.jo = {};
        $scope.filters = {};
        $scope.status_rti = ['open', 'close'];
        $scope.BASE_URL = BASE_URL;
        $scope.preeadd = [];
        $scope.role = {};

        $scope.loadData = false;

        if ($scope.type) {
            $scope.filters.type_rti = 'all';
            $scope.filters.status_rti = 'open';
        }
        if ($stateParams.tipe) {
            $scope.filters.type_rti = $stateParams.tipe;
        }

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/engineer/rti', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;
                    $scope.ac = respons.data.ac;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);

        $scope.delete = function (id) {
            myHelp.deleteConfirm('/engineer/rti/' + id, {}, 'app.maintenance.rti', {});
        }


        //RPD
        $scope.rpd = {};


    })

    .controller('app.maintenance.rti.detail', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/engineer/rti/' + $stateParams.id_engineer_rti)
            .then(function (respons) {
                $scope.rti = respons.data.rti;
            });


    })


    /*----------------------------------------------------------------------*/
    /*                      SHEDULE INSPECTION
    /*----------------------------------------------------------------------*/


    .controller('app.maintenance.inspection', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.filters = {};
        $scope.filters.id_part = null;
        $scope.master = {};
        $scope.inspections = [];
        $scope.inspection = {};

        $scope.loadData = false;
        myHelp.getParam('/mpc/inspection', {})
            .then(function (respons) {
                $scope.master.aircraft = respons.data;
            });

        $scope.pilih_part = function () {
            myHelp.getParam('/mpc/inspection', {'id_aircraft': clearObj($scope.filters.id_aircraft)})
                .then(function (respons) {
                    $scope.master.mayor_part = respons.data;
                    $scope.filters.id_part = null;
                });
        }

        $scope.pilih_inspection = function () {
            myHelp.getParam('/mpc/inspection', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.inspections = respons.data.data;
                    $scope.master.aircraft = respons.data.ac;
                    $scope.loadData = true;
                });
        }

        $scope.pilih_inspection();


        $scope.delete = function (id) {
            myHelp.deleteConfirm('/mpc/inspection/' + id, {}, 'app.maintenance.inspection', {});
        }
    })
    .controller('app.maintenance.inspection.add', function truncateCtrl($scope, $state, $stateParams, myHelp, $timeout, $filter) {

        if ($scope.filters.id_part == null) {
            errorView("Please Select Mayor Part");
            $state.go('^');
            return false;
        }
        $scope.inspection.id_part = $scope.filters.id_part;
        $scope.inspection.id_aircraft = $scope.filters.id_aircraft;

        $scope.inspection.description_inspection = "";
        $scope.master = {};

        // $scope.setAta = function () {
        //     if($scope.mpart.code_ata === undefined)
        //     {
        //         $timeout($scope.setAta,300);
        //     }
        //     else
        //     {
        //         $scope.inspection.atachapter = $scope.mpart.code_ata + '-' + $scope.mpart.mfigure_index + '-'+ $scope.mpart.mitem_index;
        //     }
        // }
        //
        // $timeout($scope.setAta,3000);


        myHelp.getDetail('/mpc/inspection/create')
            .then(function (respons) {
                $scope.master = respons.data;
            });

        $scope.ngChange = function () {
            $timeout(function () {
                var id_maintenance_code = clearString($scope.inspection.id_maintenance_code);
                var data = $filter('filter')($scope.master.code, {id_maintenance_code: id_maintenance_code});
                if (data.length > 0) {
                    $scope.inspection.description_inspection = data[0].description_maintenance;
                }

            }, 100)
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.inspection);
            Param.type_inspection = 'inspection';
            myHelp.postParam('/mpc/inspection', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $scope.pilih_inspection();
                        $state.go('^', {}, {reload: "app.maintenance.inspection"})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    .controller('app.maintenance.inspection.edit', function truncateCtrl($scope, $state, $stateParams, myHelp, $timeout, $filter) {

        $scope.inspection.description_inspection = "";
        $scope.master = {};


        myHelp.getDetail('/mpc/inspection/' + $stateParams.id_inspection + '/edit')
            .then(function (respons) {
                $scope.inspection = respons.data.data;
                $scope.master.code = respons.data.code;
                $scope.master.time_unit = respons.data.time_unit;

                $scope.ngChange();
            });

        $scope.ngChange = function () {
            var id_maintenance_code = $scope.inspection.id_maintenance_code;
            var data = $filter('filter')($scope.master.code, {id_maintenance_code: id_maintenance_code})[0];
            console.log(data);
            $scope.inspection.description_inspection = data.description_maintenance;
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.inspection);
            Param.type_inspection = 'inspection';
            myHelp.putParam('/mpc/inspection/' + $stateParams.id_inspection, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $scope.pilih_inspection();
                        $state.go('^', {}, {reload: "app.maintenance.inspection"})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })

    /*----------------------------------------------------------------------*/
    /*                      TASK CARD
    /*----------------------------------------------------------------------*/

    .controller('app.maintenance.task_card.edit', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {
        $scope.maintenance = {};
        myHelp.getParam('/mpc/self_maintenance/' + $stateParams.id + '/edit')
            .then(function (respons) {
                $scope.maintenance = respons.data.maintenance;
                $scope.rti = respons.data.rti;
            });

        $scope.submitForm = function () {
            var file = $scope.myFile;
            var fd = new FormData();
            fd.append('file_td', file);
            fd.append('id_mpc_self_maintenance', $scope.maintenance.id_mpc_self_maintenance);
            fd.append('date_maintenance', $scope.maintenance.date_maintenance);
            fd.append('report_maintenance', $scope.maintenance.report_maintenance);
            fd.append('id_engineer_rti', $scope.maintenance.id_engineer_rti);
            fd.append('number_maintenance', $scope.maintenance.number_maintenance);

            $http.post(BASE_URL + '/mpc/self_maintenance', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (suc) {
                berhasilView();
                $state.go('^', {}, {reload: "app.maintenance.task_card"})
            })
        };

        // $scope.submitForm = function() {
        //     var Param = clearObj($scope.maintenance);
        //     myHelp.putParam('/mpc/self_maintenance/' + $stateParams.id ,Param)
        //         .then(function mySuccesresponse()
        //             {
        //                 berhasilView();
        //                 $state.go('^',{}, { reload: "app.maintenance.task_card" })
        //
        //             }
        //             , function myError()
        //             {
        //                 errorView("Error data entry (400)");
        //             });
        // };
    })
    .controller('app.maintenance.task_card.detail', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {
        $scope.maintenance = {};
        myHelp.getParam('/mpc/self_maintenance/' + $stateParams.id )
            .then(function (respons) {
                $scope.maintenance = respons.data.data;
            });

    })

    .controller('app.maintenance.task_card', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {

        $scope.filters = {};
        $scope.id_mpc_self_maintenance = [];
        $scope.loadData = false;

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/mpc/self_maintenance', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;
                    $scope.transfer_maintenance = respons.data.transfer_maintenance;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);

        $scope.approved = function (id) {
            PindahParam($http, $state, '/qa/self_maintenance/' + id, {id: id}, 'app.maintenance.task_card', {id: id}, "Approve");
        }

    })


    .controller('app.maintenance.task_card.transfer', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.transfer = {};
        $scope.items = [];

        myHelp.getDetail('/qa/transfer/create')
            .then(function (respons) {
                $scope.transfer = respons.data.data;
                $scope.site = respons.data.site;

                $scope.getItems();

            });

        $scope.getItems = function () {
            console.log($scope.id_mpc_self_maintenance);
            myHelp.getParam('/qa/transfer_maintenance/create', $scope.id_mpc_self_maintenance)
                .then(function (respons) {
                    $scope.items = respons.data;
                    debugData(respons);
                });
        }


        $scope.simpanForm = function () {

            if ($scope.items.length < 1) {
                warningView("Part Item Kosong");
                return false;
            }
            $scope.transfer.type_transfer = "maintenance";

            var Obj = {transfer: $scope.transfer, items: $scope.items};
            var Param = clearObj(Obj);
            myHelp.putParam('/qa/transfer/' + $scope.transfer.id_qa_transfer, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.transfer", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })


/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/


