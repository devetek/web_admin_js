
angular
    .module('inspinia') 



/*----------------------------------------------------------------------*/
/*                      SITE / STORE
/*----------------------------------------------------------------------*/
.controller('app.store.site', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.loadData = false;
    myHelp.getDetail('/master/site')
        .then(function(respons){
            $scope.datas = respons.data;
            $scope.loadData = true;
        });

    $scope.delete = function(id)
    {
        myHelp.deleteParam('/master/site/' + id, {})
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.store.site",{}, { reload: true })
                }
                , function myError()
                {
                    errorView("gagal hapus, data tidak ditemukan");
                });
    }



})

.controller('app.store.site.add', function truncateCtrl($scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/master/site/create')
        .then(function(respons){
            $scope.master = respons.data;
            debugData(respons);
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.site);

        myHelp.postParam('/master/site', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.store.site",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})

.controller('app.store.site.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.site = {};
    myHelp.getParam('/master/site/' + $stateParams.id_site +'/edit')
        .then(function(respons){
            $scope.site = respons.data;

            //jiko ado master
            myHelp.getDetail('/master/site/create')
                .then(function(respons){
                    $scope.master = respons.data;
                    debugData(respons);
                });
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.site);

        myHelp.putParam('/master/site/1', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("app.store.site",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})

.controller('app.store.site.detail', function truncateCtrl($scope,$state,$stateParams,myHelp,$rootScope,$http){

    $rootScope.toglefilter = false;
    $scope.site = {};
    myHelp.getParam('/master/site/' + $stateParams.id_site +'/edit')
        .then(function(respons){
            $scope.site = respons.data;
            $scope.loadBin();
        });

    $scope.loadBin = function()
    {
        myHelp.getParam('/master/bin?id_site=' + $stateParams.id_site )
            .then(
                function (res) {
                    $scope.bins = res.data;
                }
            )
    }
    $scope.delete_bin = function (id) {
        deleteParam($http, $state, '/master/bin/' + id, {id_site:$stateParams.id_site}, 'app.store.site.detail', {});
    }

    $scope.submitForm = function() {

        $scope.bin.id_site = $stateParams.id_site;
        var Param = clearObj($scope.bin);
        myHelp.postParam('/master/bin', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.loadBin();
                    $rootScope.toglefilter = false;

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})

/*----------------------------------------------------------------------*/
/*                      RECEIVED QA
/*----------------------------------------------------------------------*/


    .controller('app.store.received', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.filters = {};
        $scope.loadData = false;

        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/store/transfer_qa', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.filters = respons.data.request;

                    $scope.loadData = true;
                });
        }
        $scope.filter(1);
    })

    .controller('app.store.received.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp,$timeout) {

        myHelp.getDetail('/store/transfer_qa/' + $stateParams.id_qa_transfer)
            .then(function (responsee) {
                $scope.transfer = responsee.data.data;
                $scope.items = responsee.data.items;
                $scope.bins = responsee.data.bins;
            })

        $scope.changeBin = function(id_prcl,bin)
        {
            $timeout(function () {
                var param = {};
                param.id_pur_prcl = id_prcl;
                param.bin = bin;
                myHelp.postParam('/store/transfer_qa', clearObj(param))
                    .then(function mySuccesresponse(response)
                        {
                        }
                        , function myError()
                        {
                            errorView("Error data entry (400)");
                        });
            }, 400)
        }



        $scope.acc = function (acc) {

            PindahParam($http, $state, '/store/transfer_qa/' + $stateParams.id_qa_transfer, {id_qa_transfer: $stateParams.id_qa_transfer}, 'app.store.received.detail', {id_qa_transfer: $stateParams.id_qa_transfer}, acc);
        }

    })

/*----------------------------------------------------------------------*/
/*                      STOCK
/*----------------------------------------------------------------------*/


.controller('app.store.stock', function truncateCtrl($scope, $state, $stateParams, myHelp,$window,$rootScope,$localStorage) {
    $scope.param = {};
    $scope.currentPage = 1;
    $scope.pageSize = 1;
    $scope.filters = {};
    $scope.filters.id_site = '0';
    $scope.filters.status_on_inventory = 'inStore';
    $scope.filters.id_partid = 81;
    var param = {};
    $scope.datas = [];

    $scope.loadData = false;

    myHelp.getDetail('/share/variabel/partfilter')
        .then(function (respons) {
            $scope.master = respons.data;
        });

    $scope.filter = function () {

        param = $scope.filters;
        param = clearObj(param);
        myHelp.getParam('/store/mpart', param)
            .then(function (response) {
                $scope.datas = response.data;
                $scope.loadData = true;
            });
    }


    $scope.export_report = function () {
        param = $scope.filters;
        param = clearObj(param);

        var url = $rootScope.report_url('release/LIST_OF_INVENTORY.RPT','&' + Object_toReport($scope.filters));

        $window.open(url);

    }
})


/*----------------------------------------------------------------------*/
/*                      RFS/ ESPD
/*----------------------------------------------------------------------*/
    .controller('app.store.espd', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {
        $scope.jo = {};
        $scope.filters = {};
        $scope.status_rti = ['open', 'close'];

        $scope.loadData = false;
        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/store/espd', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.site = respons.data.site;
                    $scope.filters = respons.data.request;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);

        $scope.delete = function(id) {
            deleteParam($http,$state,'/store/espd/' + id, {}, 'app.store.espd',{});
        }

    })

    .controller('app.store.espd.edit', function truncateCtrl($scope, $state, $stateParams, myHelp, $http, $log) {
        $scope.rti = {};
        $scope.status_service = ['serviceable', 'unserviceable', 'reject'];

        myHelp.getDetail('/store/espd/' + $stateParams.id_engineer_rti +'/edit')
            .then(function (respons) {
                $scope.rti = respons.data.rti;
            });

        $scope.submitForm = function () {
            if ($scope.rti.id_part == null) {
                errorView("Please Select Part");
                return false;
            }
            $scope.rti.tipe = 'injek';
            myHelp.putParam('/store/espd/' + $stateParams.id_engineer_rti, $scope.rti)
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $state.go('^', {}, {reload: "app.store.espd"});

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    .controller('app.store.espd.detail', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/store/espd/' + $stateParams.id_engineer_rti)
            .then(function (respons) {
                $scope.rti = respons.data.rti;
            });

    })

/*----------------------------------------------------------------------*/
/*                      Destroy
/*----------------------------------------------------------------------*/

.controller('app.store.destroy', function truncateCtrl($scope, $state, $stateParams, myHelp) {
    $scope.filters = {};
    $scope.loadData = false;
    $scope.filter = function () {
        myHelp.getParam('/store/destroy', clearObj($scope.filters))
            .then(function (respons) {
                $scope.datas = respons.data.data;
                $scope.filters = respons.data.request;
                $scope.loadData = true;
            });
    }
    $scope.filter(1);

})

/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/


