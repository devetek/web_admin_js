/**
 * INSPINIA - Responsive Admin Theme
 *
 */

/**
 * MainCtrl - controller
 */
function Login($state,myHelp,$scope,$localStorage,$rootScope,$http) {
    console.log('asdsa')
    $scope.user = {};
    // $scope.user.email = "arivin29@yahoo.co.id";
    // $scope.user.password = "admin123";
    $scope.BASE_URL = BASE_URL;

    $scope.login = function () {
        $http.post(BASE_URL + "/login",$scope.user)
            .then(function(respon) {
                $rootScope.is_run = true;

                $rootScope.c = respon.data.token;
                $localStorage.token = respon.data.token;

                myHelp.getParam_noToken('/acl/akses', {token: respon.data.token})
                    .then(function (respons) {
                        $rootScope.curren_user = respons.data.user;
                        $localStorage.curren_user = respons.data.user

                        myHelp.getParam('/acl/group_action/' + respons.data.user.id_group)
                            .then(function (respons) {
                                $rootScope.menu = respons.data.menu;
                                $localStorage.akses = respons.data.menu;
                            });

                        $rootScope.is_run = false;

                        $state.go('app.dashboard', {}, {});

                    });
            });


    }

};


angular
    .module('inspinia')
    .controller('loginCont',['$state','myHelp','$scope','$localStorage','$rootScope','$http', Login])
    .run(function (authManager, $rootScope,myHelp,$state,$window,$localStorage) {
        $rootScope.getCurrentUser = function()
        {
            return $localStorage.curren_user;
        }
    });