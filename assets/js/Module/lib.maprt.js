/**
 * INSPINIA - Responsive Admin Theme
 *
 */

/**
 * MainCtrl - controller
 */

angular
    .module('inspinia')
    /*----------------------------------------------------------------------*/
    /*                      DETAIL
    /*----------------------------------------------------------------------*/
    .controller('app.lib.mpart.detail', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.param = {};
        $scope.mpart = {};
        $scope.applicable = [];
        myHelp.getDetail('/umum/mpart/' + $stateParams.id_mpart)
            .then(function (respons) {
                $scope.mpart = respons.data.mpart;
                $scope.role = respons.data.role;
                $scope.applicable = respons.data.applicable;

                if($state.current.name === 'app.lib.mpart.detail')
                {
                    $state.go('app.lib.mpart.detail.stok', {id_mpart: $stateParams.id_mpart})
                }

            });
    })
    .controller('app.mpart.detail.stok', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        var param = {};
        $scope.parts = {};
        $scope.param_part = {};
        param.id_mpart = $stateParams.id_mpart;
        myHelp.getDetail('/store/mpart/' + $stateParams.id_mpart)
            .then(function (response) {
                $scope.mpart = response.data.mpart;
                $scope.status = response.data.status;

                if ($scope.status.length > 0) {
                    $scope.getPart($scope.status[0].status_on_inventory);
                }
            });

        //part detail
        $scope.getPart = function (status_on_inventory) {
            param.status_on_inventory = status_on_inventory;
            $scope.param_part = param;
            myHelp.getParam('/store/part', param)
                .then(function (response) {
                    $scope.parts = response.data.data;
                });
        }
    })

    .controller('app.mpart.detail.onprocess', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/umum/mpart/onprocess/' + $stateParams.id_mpart)
            .then(function (response) {
                $scope.all = response.data.all;
                $scope.pro = response.data.pro;
                $scope.pr = response.data.pr;
                $scope.po = response.data.po;
                $scope.prcl = response.data.prcl;
            });

    })

    .controller('app.mpart.detail.history', function truncateCtrl($scope, $state, $stateParams, myHelp, $rootScope) {
        $scope.position = {};
        myHelp.getDetail('/umum/mhistory/' + $stateParams.id_mpart)
            .then(function (respons) {
                $scope.history = respons.data.history;
            });
    })
    /*----------------------------------------------------------------------*/
    /*                      ROUTINE
    /*----------------------------------------------------------------------*/
    .controller('app.lib.mpart.detail.routine', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {
        $scope.maintenance = {};
        $scope.maintenance.id_mpart = $stateParams.id_mpart;

        $scope.param.title = 'Routine In Aircraft';
        var param = {};
        param.id_mpart = $stateParams.id_mpart;
        param.type_maintenance = "routine";

        myHelp.getParam('/mpc/maintenance', param)
            .then(function (respons) {
                $scope.routine = respons.data;
            });

        $scope.delete = function (id) {
            deleteParam($http, $state, '/mpc/maintenance/' + id, {}, 'app.lib.mpart.detail.routine', {id_mpart: $stateParams.id_mpart});
        }

    })

    .controller('app.lib.mpart.detail.routine.add', function truncateCtrl($scope, $state, $stateParams, myHelp, $timeout, $filter) {
        $scope.param.title = 'Add In Aircraft';
        $scope.maintenance.description_maintenance = "";
        $scope.master = {};

        $scope.setAta = function () {
            if ($scope.mpart.code_ata === undefined) {
                $timeout($scope.setAta, 300);
            }
            else {
                $scope.maintenance.atachapter = $scope.mpart.code_ata + '-' + $scope.mpart.mfigure_index + '-' + $scope.mpart.mitem_index;
            }
        }

        $timeout($scope.setAta, 3000);


        myHelp.getDetail('/mpc/maintenance/create')
            .then(function (respons) {
                $scope.master = respons.data;
            });

        $scope.ngChange = function () {
            var id_maintenance_code = $scope.maintenance.id_maintenance_code;
            var data = $filter('filter')($scope.master.code, {id_maintenance_code: id_maintenance_code})[0];
            $scope.maintenance.description_maintenance = data.description_maintenance;
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.maintenance);
            Param.type_maintenance = 'routine';
            myHelp.postParam('/mpc/maintenance', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go('^', {}, {reload: "app.lib.mpart.detail.routine"})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })


    .controller('app.lib.mpart.detail.routine.edit', function truncateCtrl($scope, $state, $stateParams, myHelp, $timeout, $filter) {
        $scope.param.title = 'Add In Aircraft';
        $scope.maintenance.description_maintenance = "";
        $scope.master = {};

        myHelp.getDetail('/mpc/maintenance/' + $stateParams.id_maintenance + '/edit')
            .then(function (respons) {
                $scope.maintenance = respons.data.data;
                $scope.master.code = respons.data.code;
                $scope.master.time_unit = respons.data.time_unit;

                $scope.ngChange();
            });

        $scope.ngChange = function () {
            var id_maintenance_code = $scope.maintenance.id_maintenance_code;
            var data = $filter('filter')($scope.master.code, {id_maintenance_code: id_maintenance_code})[0];
            $scope.maintenance.description_maintenance = data.description_maintenance;
        }

        $scope.submitForm = function () {
            var Param = clearObj($scope.maintenance);
            Param.type_maintenance = 'routine';
            myHelp.putParam('/mpc/maintenance/' + $stateParams.id_maintenance, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go('^', {}, {reload: "app.lib.mpart.detail.routine"})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

/*----------------------------------------------------------------------*/
/*                      COM Factor
/*----------------------------------------------------------------------*/

.controller('app.lib.mpart.detail.com_factor', function truncateCtrl($scope,$state,$stateParams,myHelp){
    $scope.com_factor = ['default','manual'];
    $scope.rotary_factor ={};
    $scope.mpart_edit ={};


    myHelp.getDetail('/master/rotary_factor/' + $stateParams.id_mpart )
        .then(function(respons){
            $scope.rotary_factor = clearObjNull(respons.data);

            if($scope.rotary_factor.calculated_cycle=='default')
            {
                $scope.rotary_factor.rumus_cycle = 'landings';
            }
            if($scope.rotary_factor.calculated_hours=='default')
            {
                $scope.rotary_factor.rumus_hours = 'hours_flight';
            }

        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.rotary_factor);
        console.log(Param)
        myHelp.postParam('/master/rotary_factor'  , Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go('^',{}, { reload: "app.lib.mpart.detail.com_factor" })
                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    };

})
.controller('app.lib.mpart.detail.com_hoist', function truncateCtrl($scope,$state,$stateParams,myHelp){
    $scope.com_factor = ['default','manual'];
    $scope.hoist = {};

    myHelp.getDetail('/master/rotary_factor/' + $stateParams.id_mpart )
        .then(function(respons){
            $scope.hoist = clearObjNull(respons.data);

            if($scope.hoist.calculated_hours=='default')
            {
                $scope.hoist.is_hoist= 0;
                $scope.hoist.rumus_hours = 'hours_flight';
            }
            else
            {
                $scope.hoist.is_hoist= 1;
            }

        });

    $scope.submitForm = function() {

        if($scope.hoist.is_hoist== 0)
        {
            $scope.hoist.calculated_hours=='default';
        }
        else
        {
            $scope.hoist.calculated_hours=='manual' ;
        }
        var Param = clearObj($scope.hoist);
        myHelp.postParam('/master/hours_hoist'  , Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go('^',{}, { reload: "app.lib.mpart.detail.com_hoist" })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    };

})


/*----------------------------------------------------------------------*/
/*                      ALTERNATE
/*----------------------------------------------------------------------*/

.controller('app.lib.mpart.detail.alternative', function truncateCtrl($http,$scope,$state,$stateParams,myHelp){
    $scope.param = {};
    $scope.alternative = {};
    $scope.loadAlternative = function () {
        myHelp.getParam('/master/mpart_alternative',{for_mpart: $stateParams.id_mpart })
            .then(function(respons){
                $scope.alternatives = respons.data.data;
                debugData(respons);
            });
    }
    $scope.loadAlternative();

    $scope.popupSearchParent = function () {
        var param = {};

        if($scope.altmpart.keyword.length > 0)
        {
            param = clearObj($scope.altmpart);
            param.limit = 2;
            myHelp.getParam('/master/mpart',param)
                .then(function(respons){
                    $scope.params = respons.data;
                });
        }
    }

    $scope.selectItem = function(id_mpart,keyword)
    {
        $scope.alternative.id_mpart = id_mpart;
        console.log($scope.alternative);
        $scope.keyword=keyword;
        $('.box-item').hide();$('.mpart').show()
    }

    $scope.deletedItem = function (id) {
        myHelp.delete('/master/mpart_alternative/' + id)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.loadAlternative();
                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    }



    $scope.addAlternative = function() {
        var Param = clearObj($scope.alternative);
        Param.for_mpart = $stateParams.id_mpart;

        myHelp.postParam('/master/mpart_alternative', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $scope.loadAlternative();

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });
    };

})

/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/


