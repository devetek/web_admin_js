
angular
    .module('inspinia')
/*----------------------------------------------------------------------------------------------
 CURRENCY
/*----------------------------------------------------------------------------------------------*/
.controller('setting.currency', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){

    $scope.loadData = false;
    myHelp.getDetail('/master/currency')
        .then(function(respons){
            $scope.datas = respons.data;
            $scope.loadData = true;
        });


    $scope.delete = function (id) {
        deleteParam($http, $state, '/master/currency/' + id, {}, 'setting.currency', {});
    }

})

.controller('setting.currency.add', function truncateCtrl($scope,$state,$stateParams,myHelp){


    $scope.submitForm = function() {
        var Param = clearObj($scope.currency);

        myHelp.postParam('/master/currency', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.currency",{}, { reload: true })

                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    };

})

.controller('setting.currency.detail', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.loadData = false;
    myHelp.getDetail('/master/currency/' + $stateParams.id_mcurrency)
        .then(function(respons){
            $scope.currency = respons.data.currency;
            debugData(respons);
        });
    myHelp.getParam('/share/currency' , {id_mcurrency:$stateParams.id_mcurrency})
        .then(function(respons){
            $scope.loadData = true;
            $scope.updated = respons.data;
            debugData(respons);
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.currency);

        myHelp.postParam('/master/currency', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.currency",{}, { reload: true })

                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    };

})

.controller('setting.currency.detail.update', function truncateCtrl($scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/master/currency/' + $stateParams.id_mcurrency)
        .then(function(respons){
            $scope.updated = respons.data;
            debugData(respons);
        });
    $scope.submitForm = function() {
        var Param = clearObj($scope.u_currency);
        Param.old_value = $scope.currency.new_value;
        Param.id_mcurrency = $scope.currency.id;

        myHelp.postParam('/share/currency', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("^",{}, { reload: true })

                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    };

})



.controller('setting.currency.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){
    console.log("Asdsa")
    myHelp.getDetail('/master/currency/' + $stateParams.id_mcurrency)
        .then(function(respons){
            $scope.master = respons.data;
            $scope.currency = respons.data.currency;
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.currency);

        myHelp.putParam('/master/currency/' + $stateParams.id_mcurrency, Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.currency",{}, { reload: true })

                }
                , function myError(respon)
                {
                    errorView(respon.data.status);
                });
    };

})

/*----------------------------------------------------------------------------------------------
 CREW
 /*----------------------------------------------------------------------------------------------*/
.controller('setting.crew', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){

    $scope.loadData = false;
    myHelp.getDetail('/master/pilot')
        .then(function(respons){
            $scope.datas = respons.data;
            $scope.loadData = true;
        });

    $scope.delete = function (id) {
        deleteParam($http, $state, '/master/pilot/' + id, {}, 'setting.crew', {});
    }

})

.controller('setting.crew.add', function truncateCtrl($scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/master/pilot/create')
        .then(function(respons){
            $scope.master = respons.data.pilot;
            $scope.users = respons.data.user;
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.pilot);

        myHelp.postParam('/master/pilot', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.crew",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})

.controller('setting.crew.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.pilot = {};
    myHelp.getParam('/master/pilot/' + $stateParams.code_pilot +'/edit')
        .then(function(respons){
            $scope.pilot = respons.data;

        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.pilot);

        myHelp.putParam('/master/pilot/'+ $stateParams.code_pilot, Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.crew",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})



/*----------------------------------------------------------------------------------------------
 DESTINATION
 /*----------------------------------------------------------------------------------------------*/
.controller('setting.destination', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){

    $scope.loadData = false;
    myHelp.getDetail('/master/destination')
        .then(function(respons){
            $scope.datas = respons.data;
            $scope.loadData = true;
        });

    $scope.delete = function (id) {
        deleteParam($http, $state, '/master/destination/' + id, {}, 'setting.destination', {});
    }


})

.controller('setting.destination.add', function truncateCtrl($scope,$state,$stateParams,myHelp){

    myHelp.getDetail('/master/destination/create')
        .then(function(respons){
            $scope.master = respons.data;
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.destination);

        myHelp.postParam('/master/destination', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.destination",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})

.controller('setting.destination.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.destination = {};
    myHelp.getParam('/master/destination/' + $stateParams.code_location +'/edit')
        .then(function(respons){
            $scope.destination = respons.data;

            //jiko ado master
            // myHelp.getDetail('/master/destination/create')
            //     .then(function(respons){
            //         $scope.master = respons.data;
            //
            //     });
        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.destination);

        myHelp.putParam('/master/destination/'+ $stateParams.code_location, Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.destination",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})


/*----------------------------------------------------------------------------------------------
 SHIPPING
 /*----------------------------------------------------------------------------------------------*/
.controller('setting.shipping', function truncateCtrl($scope,$state,$stateParams,myHelp,$http){

    $scope.loadData = false;
    myHelp.getDetail('/master/shipping')
        .then(function(respons){
            $scope.datas = respons.data;
            $scope.loadData = true;
        });

    $scope.delete = function (id) {
        deleteParam($http, $state, '/master/shipping/' + id, {}, 'setting.shipping', {});
    }

})

.controller('setting.shipping.add', function truncateCtrl($scope,$state,$stateParams,myHelp){


    $scope.submitForm = function() {
        var Param = clearObj($scope.shipping);

        myHelp.postParam('/master/shipping', Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.shipping",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})

.controller('setting.shipping.edit', function truncateCtrl($scope,$state,$stateParams,myHelp){

    $scope.shipping = {};
    myHelp.getParam('/master/shipping/' + $stateParams.id_shipping +'/edit')
        .then(function(respons){
            $scope.shipping = respons.data;

        });

    $scope.submitForm = function() {
        var Param = clearObj($scope.shipping);

        myHelp.putParam('/master/shipping/'+ $stateParams.id_shipping, Param)
            .then(function mySuccesresponse()
                {
                    berhasilView();
                    $state.go("setting.shipping",{}, { reload: true })

                }
                , function myError()
                {
                    errorView("Error data entry (400)");
                });


    };

})

