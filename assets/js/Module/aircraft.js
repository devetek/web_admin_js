/**
 * INSPINIA - Responsive Admin Theme
 *
 */

/**
 * MainCtrl - controller
 */

angular
    .module('inspinia')
    /*----------------------------------------------------------------------*/
    /*                      AIRCRAFT
    /*----------------------------------------------------------------------*/

    .controller('app.aircraft.actype', function truncateCtrl($scope, $state, $stateParams, myHelp,$http) {

        $scope.loadData = false;
        myHelp.getDetail('/master/actype')
            .then(function (respons) {
                $scope.datas = respons.data;
                $scope.loadData = true;
            });

        $scope.delete_type = function (id) {
            console.log('sds')
            deleteParam($http, $state, '/master/actype/' + id, {}, $state.current.name, {});
        }

    })

    .controller('app.aircraft.actype.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.actype = {};
        $scope.actype.ac_category = "rotary";

        $scope.submitForm = function () {
            var Param = clearObj($scope.actype);

            myHelp.postParam('/master/actype', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.aircraft.actype", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })

    .controller('app.aircraft.actype.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.actype = {};
        myHelp.getParam('/master/actype/' + $stateParams.id_actype + '/edit')
            .then(function (respons) {
                $scope.actype = respons.data;

                //jiko ado master
                // myHelp.getDetail('/master/actype/create')
                //     .then(function (respons) {
                //         $scope.master = respons.data;
                //         debugData(respons);
                //     });
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.actype);

            myHelp.putParam('/master/actype/' + $stateParams.id_actype, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.aircraft.actype", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });


        };

    })

    // LIST


    .controller('app.aircraft.list', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        myHelp.getDetail('/tech_rec/aircraft')
            .then(function (respons) {
                $scope.datas = respons.data;
            });
    })

    .controller('app.aircraft.register', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.loadData = false;
        $scope.filter = function () {
            myHelp.getDetail('/master/aircraft')
                .then(function (respons) {
                    $scope.datas = respons.data.data;
                    $scope.loadData = true;
                });
        }
        $scope.filter();

        $scope.delete = function (id) {
            myHelp.deleteParam('/master/aircraft/' + id, {})
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.aircraft.register", {}, {reload: true})

                    }
                    , function myError(respon) {
                        errorView(respon.data.status);
                    });
        }
    })


    .controller('app.aircraft.register.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $scope.aircraft = {};
        $scope.aircraft.hasapu = "no";
        // $scope.aircraft.purchasedate='';
        myHelp.getDetail('/master/aircraft/create')
            .then(function (respons) {
                $scope.master = respons.data;
                debugData(respons);
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.aircraft);
            console.log($scope.aircraft.purchasedate);

            myHelp.postParam('/master/aircraft', Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.aircraft.register", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });

        };

    })

    .controller('app.aircraft.register.edit', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.aircraft = {};
        myHelp.getParam('/master/aircraft/' + $stateParams.id_aircraft + '/edit')
            .then(function (respons) {
                $scope.aircraft = respons.data;
                $scope.aircraft.tendurance = respons.data.tendurance * 1;
                $scope.aircraft.id_actype = respons.data.id_actype * 1;

                //jiko ado master
                myHelp.getDetail('/master/aircraft/create')
                    .then(function (respons) {
                        $scope.master = respons.data;
                        debugData(respons);
                    });
            });

        $scope.submitForm = function () {
            var Param = clearObj($scope.aircraft);

            myHelp.putParam('/master/aircraft/' + $stateParams.id_aircraft, Param)
                .then(function mySuccesresponse() {
                        berhasilView();
                        $state.go("app.aircraft.register", {}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })


    .controller('app.aircraft.detail', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {
        $("body").addClass("mini-navbar");
        $scope.cm = {};

        $scope.getPartInstall = function (param) {
            myHelp.getDetail('/tech_rec/aircraft/' + $stateParams.id_aircraft, param)
                .then(function (respons) {
                    $scope.aircraft = respons.data.aircraft;
                    $scope.detail = respons.data.detail;
                    $scope.engine = respons.data.engine;
                    $scope.airframe = respons.data.airframe;
                    $scope.history = respons.data.history;

                    $scope.cm.id_ac = $scope.detail.id_aircraft;

                });
        }

        $scope.getPartInstall({});

        $scope.ganti = false;

        $scope.triger_ganti = function () {
            if ($scope.ganti == false) {
                $scope.ganti = true;
            }
            else {
                $scope.ganti = false;
            }
        }

        $scope.gantiFoto = function () {
            var file = $scope.myFile;
            var fd = new FormData();
            fd.append('file_td', file);
            fd.append('id_aircraft', $stateParams.id_aircraft);

            $http.post(BASE_URL + '/share/aircraft', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(
                function (res) {
                    berhasilView();
                    $state.go("app.aircraft.detail.general", {id_user: $stateParams.id_aircraft}, {reload: true});
                }
            )

        };


    })
    .controller('app.aircraft.detail.general', function truncateCtrl($scope, $state, $stateParams, myHelp) {


    })
    .controller('app.aircraft.detail.history', function truncateCtrl($scope, $state, $stateParams, myHelp) {


    })


    .controller('app.aircraft.detail.report', function truncateCtrl($scope, $state, $stateParams, myHelp, $window, $rootScope) {
        $scope.export_report_install = function (id) {
            var url = $rootScope.report_url('release/COMPONEN_STATUS.RPT', '&' + Object_toReport(param));
            $window.open(url);
        }

    })

    .controller('app.aircraft.detail.updated', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.updated = function () {
            myHelp.postParam('/tech_rec/aircraft_detail', clearObj($scope.detail))
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $state.go('^', {}, {reload: "app.aircraft.detail"});
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };
    })

    .controller('app.aircraft.detail.full', function truncateCtrl($sce, $http, $scope, $state, $stateParams, myHelp) {

        myHelp.getDetail('/share/aircraft_detail_full?id_aircraft=' + $stateParams.id_aircraft)
            .then(function (respons) {
                $scope.list_file = respons.data;
            });

        $scope.delete = function (id) {
            deleteParam($http, $state, '/share/aircraft_detail_full/' + id, {id_aircraft: $stateParams.id_aircraft}, "app.aircraft.detail.full", {reload: true});
        }


        $scope.submitForm = function () {
            var file = $scope.myFile;
            if (!file) {
                alert('pelase select file!')
                return false;
            }
            var fd = new FormData();
            fd.append('file_td', file);
            fd.append('id_aircraft', $stateParams.id_aircraft);
            fd.append('file_number', $scope.file_number);
            fd.append('category', $scope.category);
            fd.append('file_date', $scope.file_date);


            $http.post(BASE_URL + '/share/aircraft_detail_full', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .then(function () {
                    berhasilView();
                    $state.go("app.aircraft.detail.full", {id_user: $stateParams.id_aircraft}, {reload: true});
                },
                    function () {
                        errorView()
                    }
                    )

        };

    })
    .controller('app.aircraft.detail.remove', function truncateCtrl($http, $scope, $state, $stateParams, myHelp) {

        $scope.getUninstall = function () {
            myHelp.getParam('/share/part_uninstall', {id_aircraft: $stateParams.id_aircraft})
                .then(function (respons) {
                    $scope.UnInstalls = respons.data.data;
                });
        }
        $scope.getUninstall();

    })

    /*----------------------------------------------------------------------*/
    /*                      CATALOG
    /*----------------------------------------------------------------------*/

    .controller('app.catalog', function truncateCtrl($scope, $state, $stateParams, myHelp, $log, $http) {
        $("body").addClass("mini-navbar");
        $scope.loadData = false;
        myHelp.getDetail('/tech_rec/aircraft/' + $stateParams.id_aircraft)
            .then(function (respons) {
                $scope.aircraft = respons.data.aircraft;
                $scope.ac_untuk_default_actype = $scope.aircraft.id_actype;
            });

        $scope.getPartInstall = function () {
            myHelp.getParam('/tech_rec/catalog', {id_aircraft: $stateParams.id_aircraft})
                .then(function (respons) {
                    $scope.datas = respons.data.install;
                    $scope.loadData = true;
                });
        }
        $scope.getPartInstall();

        //TREE
        $scope.ac = function () {
            return true;
        }
        $scope.treeData = [];

        myHelp.getParam('/share/tree_view', {id_aircraft: $stateParams.id_aircraft})
            .then(function (respons) {
                $scope.treeData = respons.data.data;

                $scope.treeConfig = {
                    core: {
                        multiple: false,
                        animation: true,
                        error: function (error) {
                            $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                        },
                        check_callback: true,
                        worker: true
                    },
                    version: 1
                };
                $scope.reCreateTree = function () {
                    this.treeConfig.version++;
                }

                $scope.treeEventsObj = {
                    'ready': readyCB,
                    'create_node': createNodeCB
                }

                function readyCB() {
                    $log.info('ready called');

                };

                function createNodeCB(e, item) {
                    $log.info('create_node called');
                };


                $scope.setParent = function () {
                    var node = $scope.treeInstance.jstree(true).get_selected();
                    if (node.length > 0) {
                        $state.go('app.catalog.add', {id_part_parent: node[0]}, {});
                    }
                    else {
                        $scope.master.mpart.parent = null;
                        $scope.master.part.parent = null;
                        $scope.parent.keyword = "";
                        $scope.parent.part_number = "";
                        $scope.master.mpart.keyword = "This Not have Parent";
                    }


                }


            });

        $scope.delete = function (id) {
            deleteParam($http, $state, '/tech_rec/catalog/' + id, {id_aircraft: $stateParams.id_aircraft}, "app.catalog", {reload: true});
        }


    })

    .controller('app.catalog.add', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $("body").addClass("mini-navbar");
        $scope.master = {};
        $scope.params = {};
        $scope.aircraft = {};

        $scope.master.mpart = {};
        $scope.master.part = {};
        $scope.master.part.id_part_install = '';
        $scope.master.part.qty = 1;
        $scope.master.mpart.issingle = 'y';
        $scope.master.id_aircraft = $stateParams.id_aircraft;

        //untuak form
        $scope.master.mpart.id_mpart = null;
        $scope.master.part.parent = null;

        $scope.master.edit = false;
        $scope.master.part.parent = $stateParams.id_part_parent;

        //masuan mode prcl
        myHelp.getParam('/purchasing/vendor', {is_manufacture: 'y', ispage: 'n'})
            .then(function (responsee) {
                $scope.params.vendor = responsee.data.vendors;
            });

        myHelp.getDetail('/master/part_condition')
            .then(function (responsee) {
                $scope.params.part_condition = responsee.data;
            });

        myHelp.getDetail('/master/life_time_limit')
            .then(function (responsee) {
                $scope.params.life_time_limit = responsee.data;
            });

        myHelp.getDetail('/master/condition_monitoring')
            .then(function (responsee) {
                $scope.params.condition_monitoring = responsee.data;
            });

        myHelp.getDetail('/master/variabel/prcl')
            .then(function (responsee) {
                $scope.params.variabel = responsee.data;
            });

        $scope.submitForm = function () {
            if ($scope.master.mpart.id_mpart == null) {
                errorView("Master part Empty !");
                return false;
            }
            if ($scope.master.part.parent == null) {
                errorView("Part parent empty !");
                return false;
            }
            if ($scope.master.mpart.issingle == 'y') {
                $scope.master.part.qty = 1;
            }

            var Param = clearObj($scope.master);
            myHelp.postParam('/tech_rec/catalog', Param)
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $state.go("app.catalog", {id_aircraft: $stateParams.id_aircraft}, {reload: true})

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    .controller('app.catalog.update', function truncateCtrl($scope, $state, $stateParams, myHelp) {
        $("body").addClass("mini-navbar");
        $scope.master = {};
        $scope.params = {};
        $scope.aircraft = {};

        $scope.master.mpart = {};
        $scope.master.part = {};
        $scope.master.part.id_part_install = '';
        $scope.master.part.qty = 1;
        $scope.master.mpart.issingle = 'y';
        $scope.master.id_aircraft = $stateParams.id_aircraft;

        //untuak form
        $scope.master.mpart.id_mpart = null;
        $scope.master.part.parent = null;

        $scope.master.edit = false;
        $scope.master.part.parent = $stateParams.id_part_parent;
        //ambil PART -> T_PART
        myHelp.getDetail('/tech_rec/catalog/' + $stateParams.id_part_install + '/edit')
            .then(function (responsee) {
                $scope.master.part = responsee.data.part;

                //part
                myHelp.getDetail('/master/mpart/' + $scope.master.part.id_mpart)
                    .then(function (respons) {
                        $scope.master.mpart = respons.data;

                        $scope.master.part.code_ata = respons.data.code_ata;
                        $scope.master.part.id_life_time_limit = respons.data.id_life_time_limit;
                        $scope.master.part.life_limit_cycle = respons.data.life_limit_cycle;
                        $scope.master.part.life_limit_hours = respons.data.life_limit_hours;
                        $scope.master.part.life_limit_var = respons.data.life_limit_var;
                        $scope.master.part.life_limit_val = respons.data.life_limit_val;

                        $scope.master.part.figure_index = respons.data.mfigure_index;
                        $scope.master.part.item_index = respons.data.mitem_index;


                        myHelp.getParam('/purchasing/vendor', {is_manufacture: 'y', ispage: 'n'})
                            .then(function (responsee) {
                                $scope.params.vendor = responsee.data.vendors;
                            });

                        myHelp.getDetail('/master/part_condition')
                            .then(function (responsee) {
                                $scope.params.part_condition = responsee.data;
                            });

                        myHelp.getDetail('/master/life_time_limit')
                            .then(function (responsee) {
                                $scope.params.life_time_limit = responsee.data;
                            });

                        myHelp.getDetail('/master/condition_monitoring')
                            .then(function (responsee) {
                                $scope.params.condition_monitoring = responsee.data;
                            });

                        myHelp.getDetail('/master/variabel/prcl')
                            .then(function (responsee) {
                                $scope.params.variabel = responsee.data;
                            });

                    });
            });

        $scope.updateForm = function () {
            console.log($scope.master.part);

            if ($scope.master.part.id_part == null) {
                errorView("Master part Empty !");
                return false;
            }
            if ($scope.master.part.parent == null) {
                errorView("Part parent empty !");
                return false;
            }
            if ($scope.master.mpart.issingle == 'y') {
                $scope.master.part.qty = 1;
            }

            var Param = clearObj($scope.master);
            myHelp.putParam('/tech_rec/catalog/' + $scope.master.part.id_part_install, Param)
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $scope.getPartInstall();
                        $state.go("app.catalog", {}, {reload: true});
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    .controller('app.catalog.add.search_mpart', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $scope.parents = [];
        $scope.loadData = false;
        var param = {};
        $scope.pencarian = function (keyword) {
            param.keyword = keyword;
            param.limt = true;

            if (keyword.length < 3) {
                errorView("At least 3 letters ");
                return false;
            }
            myHelp.getParam('/master/mpart', param)
                .then(function (respons) {
                    $scope.parents = respons.data;
                    $scope.loadData = true;
                });
        }
        $scope.pencarian($stateParams.keyword);


        $scope.selectMpart = function (id_mpart) {
            myHelp.getDetail('/master/mpart/' + id_mpart)
                .then(function (respons) {
                    $scope.master.mpart = respons.data;

                    $scope.master.part.code_ata = respons.data.code_ata;
                    $scope.master.part.id_life_time_limit = respons.data.id_life_time_limit;
                    $scope.master.part.life_limit_cycle = respons.data.life_limit_cycle;
                    $scope.master.part.life_limit_hours = respons.data.life_limit_hours;
                    $scope.master.part.life_limit_var = respons.data.life_limit_var;
                    $scope.master.part.life_limit_val = respons.data.life_limit_val;

                    $scope.master.part.figure_index = respons.data.mfigure_index;
                    $scope.master.part.item_index = respons.data.mitem_index;

                });
            $state.go('^');
        }

    })
    .controller('app.catalog.add.add_mpart', function truncateCtrl($scope, $state, $stateParams, myHelp, $timeout) {
        $scope.param = {};
        $scope.parent = {};
        $scope.param.pencarian = '';
        $scope.parent.keyword = 'Select mpart';
        $scope.popup = false;

        $scope.cekIssingle = function () {
            $timeout(function () {
                var cek = $filter('filter')($scope.master.partid, {id_partid: clearInt($scope.mpart.id_partid)});
                console.log(cek)
                if (cek.length > 0) {
                    $scope.mpart.issingle = cek[0].issingle;
                    console.log('aaaa')
                }
                else {
                    $scope.mpart.issingle = 'n';
                }
            }, 200)
        }

        $scope.popupSearchParent = function () {
            console.log('sds')
            var param = {};

            if ($scope.param.pencarian.length > 2) {
                param.keyword = $scope.param.pencarian;
            }
            else {
                alert('min 3 character');
                return false;
            }
            param.limit = true;

            myHelp.getParam('/master/mpart', param)
                .then(function (respons) {
                    $scope.parents = respons.data;
                });
        }

        $scope.startPopup = function () {

            $scope.popup = true;
        }
        $scope.closePopup = function () {
            console.log('sss')
            $scope.popup = false;
        }


        myHelp.getDetail('/master/mpart/create')
            .then(function (respons) {
                $scope.masters = respons.data;
                debugData(respons);
            });


        $scope.selectParentMpart = function (parent, keyword) {
            console.log(keyword);
            $scope.master.mpart.parent = parent;
            $scope.parent.keyword = keyword;
            $scope.popup = false;
        }

        $scope.master.mpart.min_qty = 1;

        $timeout(function () {
            $scope.master.mpart.applicable = [];
            $scope.master.mpart.applicable.push($scope.ac_untuk_default_actype*1);
            console.log($scope.master.mpart)
        }, 3000)

        $scope.submitForm = function () {
            var Param = clearObj($scope.master.mpart);
            if ($scope.master.mpart.parent == null) {
                warningView('Please Select Parent !');
                return false;
            }
            if ($scope.master.mpart.min_qty < 1) {
                warningView('Invalid Minimum QTY !');
                return false;
            }

            myHelp.postParam('/master/mpart', Param)
                .then(function mySuccesresponse(respons) {
                        berhasilView();
                        $scope.master.mpart = respons.data.data;
                        $scope.master.part.id_life_time_limit = respons.data.data.id_life_time_limit;
                        $scope.master.part.life_limit_cycle = respons.data.data.life_limit_cycle;
                        $scope.master.part.life_limit_hours = respons.data.data.life_limit_hours;
                        $scope.master.part.life_limit_var = respons.data.data.life_limit_var;
                        $scope.master.part.life_limit_val = respons.data.data.life_limit_val;

                        $scope.master.part.tbo_cycle = respons.data.data.mtbo_cycle;
                        $scope.master.part.tbo_hours = respons.data.data.mtbo_hours;
                        $scope.master.part.tbo_val = respons.data.data.mtbo_val;
                        $scope.master.part.tbo_var = respons.data.data.mtbo_var;
                        $scope.master.part.id_condition_monitoring = respons.data.data.id_condition_monitoring * 1;

                        $scope.master.part.figure_index = respons.data.data.mfigure_index;
                        $scope.master.part.item_index = respons.data.data.mitem_index;

                        $scope.master.part.item_index = clearObj($scope.master.part.item_index);

                        $state.go('^')

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });


        };

    })



    /*----------------------------------------------------------------------*/
    /*                      AML (ADD)
    /*----------------------------------------------------------------------*/

    .controller('app.aml', function truncateCtrl($scope, $state, $stateParams, myHelp) {

        $("body").addClass("mini-navbar");

        $scope.jo = {};
        $scope.filters = {};
        $scope.filters.id_aircraft = $stateParams.id_aircraft;
        $scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $scope.years = [];

        var date = new Date();

        for (var a = 0; a < 5; a++) {
            $scope.years.push(date.getFullYear() - a);
        }

        $scope.loadData = false;
        $scope.filter = function (page) {
            $scope.filters.page = page;
            myHelp.getParam('/tech_rec/flight_log', clearObj($scope.filters))
                .then(function (respons) {
                    $scope.datas = respons.data.data.data;
                    $scope.filters = respons.data.data.request;
                    $scope.ac = respons.data.ac;
                    $scope.loadData = true;
                });
        }
        $scope.filter(1);
    })


    .controller('app.aml.add', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {

        $("body").addClass("mini-navbar");

        $scope.log = {};
        $scope.log_item = {};
        $scope.log_items = [];
        $scope.log_engine = [];
        $scope.last_number = {};
        $scope.hoist = ['L', 'R'];
        $scope.cg_penalty = ['Y', 'N'];
        $scope.last_number.last_number = 1;

        $scope.last_number = {};
        $scope.log = {};
        $scope.log_engine = [];
        $scope.log_engine_2 = [];
        $scope.log_engine_check = [];
        $scope.ac = {};
        $scope.place = [];
        $scope.location = [];
        $scope.pilot = [];


        myHelp.getParam('/tech_rec/flight_log/create', {id_aircraft: $stateParams.id_aircraft})
            .then(function (respons) {
                $scope.last_number = respons.data.ujuang;
                $scope.log = respons.data.data;
                $scope.log_engine = respons.data.log_engine;
                $scope.log_engine_2 = respons.data.log_engine;
                $scope.log_engine_check = respons.data.log_engine_check;
                $scope.ac = respons.data.ac;
                $scope.place = respons.data.place;
                $scope.location = respons.data.location;
                $scope.pilot = respons.data.pilot;
                //$scope.log.number_log = $scope.log.id_aircraft = $scope.ac.serial_number + '/A.' + ($scope.last_number.last_number + 1);

                $scope.getLog_item();

            });


        $scope.log_item.flight_hours = "2";
        $scope.log_item.landing = "1";

        $scope.landing_time = function () {
            var start = $scope.log_item.takeof_time;
            var end = $scope.log_item.landing_time;
            if (start && end) {
                var start = moment(start);
                var end = moment(end);

                var duration = moment.duration(end.diff(start));
                $scope.log_item.flight_hours = duration.asHours();
            }
        }

        $scope.landing_time_plus = function () {
            var start = $scope.log_item.takeof_time;
            var hours = $scope.log_item.flight_hours;
            if (start && hours) {
                // var start=moment(start);
                // var hours=moment(hours);

                var duration = moment(start, 'HH').add(hours - 1, 'hours');

                // var duration = start.add(hours,'hours').format('hh:mm A');
                console.log(duration)
                $scope.log_item.landing_time = duration;
            }


        }


        $scope.sumbitAML = function () {
            var post = {};
            post.log = $scope.log;
            myHelp.postParam('/tech_rec/flight_log', clearObj(post))
                .then(function mySuccesresponsee() {
                        $scope.filter(1);
                        $state.go('app.aml', {id_aircraft: $stateParams.id_aircraft}, {reload: true});
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


        ///-------------------- END TAMBAHAN

        $scope.getLog_item = function () {
            myHelp.getDetail('/tech_rec/flight_log_item/' + $scope.log.id_tech_log)
                .then(function (respons) {
                    $scope.log_items = respons.data;

                    var total_hour = 0;
                    var total_cycle = 0;
                    for (var a = 0; a < $scope.log_items.length; a++) {
                        total_hour = total_hour + parseFloat($scope.log_items[a].flight_hours) * 1;
                        total_cycle = total_cycle + parseFloat($scope.log_items[a].landings) * 1;
                    }

                    $scope.log.hours_flight = total_hour;
                    $scope.log.cycle_flight = total_cycle;

                    // ubah cf
                    $scope.log.hours_cf = parseFloat($scope.log.hours_bf) + $scope.log.hours_flight;
                    $scope.log.cycle_cf = parseFloat($scope.log.cycle_bf) + $scope.log.cycle_flight;

                });
        }

        //ITEM-------------------------
        $scope.delete_log_item = function (id) {
            myHelp.delete('/tech_rec/flight_log_item/' + id)
                .then(function (respons) {

                    $scope.getLog_item();

                });
        }


        $scope.sumbitForm = function () {

            $scope.log_item.takeof_time = moment(moment($scope.log_item.takeof_time)).format('YYYY-MM-DD HH:mm:ss');
            $scope.log_item.landing_time = moment(moment($scope.log_item.landing_time)).format('YYYY-MM-DD HH:mm:ss');

            $scope.log_item.id_tech_log = $scope.log.id_tech_log;
            myHelp.postParam('/tech_rec/flight_log_item', clearObj($scope.log_item))
                .then(function mySuccesresponsee() {
                        $scope.getLog_item();
                        //$state.go('^',{},{reload:"app.tech_log"});
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

        $scope.saveLogEngine = function () {
            var post = {};
            post.log_engine = $scope.log_engine;
            myHelp.postParam('/tech_rec/flight_log_engine', clearObj(post))
                .then(function mySuccesresponsee() {

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

    })


    .controller('app.aml.edit', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {
        $scope.log = {};
        $scope.log_item = {};
        $scope.log_items = [];
        $scope.log_engine = [];
        $scope.last_number = {};
        $scope.hoist = ['L', 'R'];
        $scope.cg_penalty = ['Y', 'N'];
        $scope.last_number.last_number = 1;

        $scope.last_number = {};
        $scope.log = {};
        $scope.log_engine = [];
        $scope.log_engine_2 = [];
        $scope.log_engine_check = [];
        $scope.ac = {};
        $scope.place = [];
        $scope.location = [];
        $scope.pilot = [];

        myHelp.getParam('/tech_rec/flight_log/' + $stateParams.id_tech_log + '/edit')
            .then(function (respons) {

                $scope.log = respons.data.log;
                $scope.log_engine = respons.data.log_engine;
                $scope.log_engine_check = respons.data.log_engine_check;
                $scope.ac = respons.data.ac;
                $scope.place = respons.data.place;
                $scope.location = respons.data.location;
                $scope.pilot = respons.data.pilot;

                $scope.getLog_item();

            });


        $scope.landing_time = function () {
            var start = $scope.log_item.takeof_time;
            var end = $scope.log_item.landing_time;
            if (start && end) {
                var start = moment(start);
                var end = moment(end);

                var duration = moment.duration(end.diff(start));
                $scope.log_item.flight_hours = duration.asHours();
            }


        }

        $scope.is_authtogle = false;
        $scope.authtogle = function () {
            if ($scope.is_authtogle == true) {
                $scope.is_authtogle = false;
            }
            else {
                $scope.is_authtogle = true;
            }
        }

        $scope.sumbitAML = function () {
            var post = {};
            post.log = $scope.log;
            myHelp.postParam('/tech_rec/flight_log', clearObj(post))
                .then(function mySuccesresponsee() {
                        $state.go('app.aml.edit', {id_tech_log: $scope.log.id_tech_log}, {reload: "app.aml.edit"});
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

        $scope.authAML = function () {

            myHelp.putParam('/tech_rec/flight_log/' + $stateParams.id_tech_log, {
                id_tech_log: $stateParams.id_tech_log,
                password: $scope.log.password
            })
                .then(function mySuccesresponsee() {
                        $scope.filter(1);
                        $state.go('app.aml', {id_tech_log: $scope.log.id_tech_log}, {reload: true});
                    }
                    , function myError(respon) {
                        errorView(respon.data.status);
                    });
        };


        ///-------------------- END TAMBAHAN

        $scope.getLog_item = function () {
            myHelp.getDetail('/tech_rec/flight_log_item/' + $scope.log.id_tech_log)
                .then(function (respons) {
                    $scope.log_items = respons.data;

                    var total_hour = 0;
                    var total_cycle = 0;
                    for (var a = 0; a < $scope.log_items.length; a++) {
                        total_hour = total_hour + parseFloat($scope.log_items[a].flight_hours) * 1;
                        total_cycle = total_cycle + parseFloat($scope.log_items[a].landings) * 1;
                    }

                    $scope.log.hours_flight = total_hour;
                    $scope.log.cycle_flight = total_cycle;

                    // ubah cf
                    $scope.log.hours_cf = parseFloat($scope.log.hours_bf) + $scope.log.hours_flight;
                    $scope.log.cycle_cf = parseFloat($scope.log.cycle_bf) + $scope.log.cycle_flight;

                });
        }


    })


    .controller('app.aml.detail', function truncateCtrl($scope, $state, $stateParams, myHelp, $http) {

        myHelp.getParam('/tech_rec/flight_log/' + $stateParams.id_tech_log + '/edit')
            .then(function (respons) {

                $scope.log = respons.data.log;
                $scope.log_engine = respons.data.log_engine;
                $scope.log_engine_check = respons.data.log_engine_check;
                $scope.ac = respons.data.ac;
                $scope.place = respons.data.place;
                $scope.location = respons.data.location;
                $scope.pilot = respons.data.pilot;

                $scope.role = respons.data.role;

                $scope.getLog_item();

                $scope.getDefects();
                $scope.getDiscrepancies();
                $scope.getRti();
                $scope.getJos();
                $scope.getTDs();
                $scope.getUninstall();
                $scope.getInstall();
            });

        ///-------------------- TAMBAHAN
        $scope.getLog_item = function () {
            myHelp.getDetail('/tech_rec/flight_log_item/' + $scope.log.id_tech_log)
                .then(function (respons) {
                    $scope.log_items = respons.data;

                    var total_hour = 0;
                    var total_cycle = 0;
                    for (var a = 0; a < $scope.log_items.length; a++) {
                        total_hour = total_hour + parseFloat($scope.log_items[a].flight_hours) * 1;
                        total_cycle = total_cycle + parseFloat($scope.log_items[a].landings) * 1;
                    }

                    $scope.log.hours_flight = total_hour;
                    $scope.log.cycle_flight = total_cycle;

                    // ubah cf
                    $scope.log.hours_cf = parseFloat($scope.log.hours_bf) + $scope.log.hours_flight;
                    $scope.log.cycle_cf = parseFloat($scope.log.cycle_bf) + $scope.log.cycle_flight;

                });
        }

        $scope.getDiscrepancies = function () {
            myHelp.getDetail('/tech_rec/discrepancies/' + $scope.log.id_tech_log)
                .then(function (respons) {
                    $scope.discrepancies = respons.data.data;
                });
        }

        $scope.getDefects = function () {
            myHelp.getDetail('/tech_rec/defect/' + $scope.log.id_tech_log)
                .then(function (respons) {
                    $scope.defects = respons.data.data;
                });
        }

        $scope.getRti = function () {
            myHelp.getParam('/tech_rec/rti', {id_tech_log: $scope.log.id_tech_log})
                .then(function (respons) {
                    $scope.rtis = respons.data.data;
                    $scope.jos_rti = respons.data.jos;
                });
        }

        $scope.getJos = function () {
            myHelp.getParam('/tech_rec/flight_log_jo', {
                id_aircraft: $stateParams.id_aircraft,
                id_tech_log: $scope.log.id_tech_log
            })
                .then(function (respons) {
                    $scope.jos = respons.data.jos;
                    $scope.data_jos = respons.data.data;
                });
        }

        $scope.getTDs = function () {
            myHelp.getParam('/tech_rec/flight_log_td', {
                id_aircraft: $stateParams.id_aircraft,
                id_tech_log: $scope.log.id_tech_log
            })
                .then(function (respons) {
                    $scope.tds = respons.data.td;
                    $scope.data_tds = respons.data.td_close;
                });
        }

        $scope.getUninstall = function () {
            myHelp.getParam('/share/part_uninstall', {id_aircraft: $stateParams.id_aircraft})
                .then(function (respons) {
                    $scope.UnInstalls = respons.data.data;
                });
        }

        $scope.getInstall = function () {
            myHelp.getParam('/tech_rec/flight_log_pris', {id_tech_log: $stateParams.id_tech_log})
                .then(function (respons) {
                    $scope.Installs = respons.data.install;
                });
        }

        if ($state.current.name === 'app.aml.detail') {
            $state.go('app.aml.detail.log', {id_tech_log: $stateParams.id_tech_log}, {});
        }
    })

    .controller('app.aml.detail.log', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {


    })


    .controller('app.aml.detail.performance', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {

        $scope.saveLogEnginePerformance = function () {
            var post = {};
            post.log_engine_check = $scope.log_engine_check;
            myHelp.postParam('/tech_rec/flight_log_performance', clearObj(post))
                .then(function mySuccesresponsee() {

                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })


    .controller('app.aml.detail.defect', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter, $log) {
        $scope.defect = {};

        myHelp.getParam('/share/tree_view', {id_aircraft: $stateParams.id_aircraft})
            .then(function (respons) {
                $scope.treeData = respons.data.data;


                $scope.treeConfig = {
                    core: {
                        multiple: false,
                        animation: true,
                        error: function (error) {
                            $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                        },
                        check_callback: true,
                        worker: true
                    },
                    version: 1
                };
                $scope.reCreateTree = function () {
                    this.treeConfig.version++;
                }

                $scope.treeEventsObj = {
                    'ready': readyCB,
                    'create_node': createNodeCB
                }

                function readyCB() {
                    $log.info('ready called');

                };

                function createNodeCB(e, item) {
                    $log.info('create_node called');
                };


                $scope.setDefect = function () {
                    var node = $scope.treeInstance.jstree(true).get_selected();
                    if (node.length > 0) {
                        myHelp.getDetail('/share/part/' + node[0] + '?install=y')
                            .then(function (respons) {
                                $scope.defect.id_part = respons.data.id_part;
                                $scope.defect.keyword = respons.data.keyword;
                                $scope.defect.part_number = respons.data.part_number;
                                $scope.defect.id_part_install = respons.data.id_part_install;

                            });

                    }
                    else {
                        $scope.defect.id_part = null;
                        $scope.defect.keyword = "";
                        $scope.defect.part_number = "";
                        $scope.defect.keyword = "This Not have Parent";
                    }


                }


            });


        $scope.sumbitFormDefect = function () {

            if (!$scope.defect.id_part) {
                errorView("Select part");
            }
            $scope.defect.id_tech_log = $scope.log.id_tech_log;
            $scope.defect.id_aircraft = $stateParams.id_aircraft;

            myHelp.postParam('/tech_rec/defect', clearObj($scope.defect))
                .then(function mySuccesresponsee() {
                        $scope.getDefects();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


        $scope.removeDefect = function (id) {
            myHelp.delete('/tech_rec/defect/' + id)
                .then(function mySuccesresponsee() {
                        $scope.getDefects();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })


    .controller('app.aml.detail.discrepancies', function truncateCtrl($log, $scope, $state, $stateParams, myHelp, $filter) {
        $scope.discrepancie = {};

        myHelp.getParam('/share/tree_view', {id_aircraft: $stateParams.id_aircraft})
            .then(function (respons) {
                $scope.treeData = respons.data.data;


                $scope.treeConfig = {
                    core: {
                        multiple: false,
                        animation: true,
                        error: function (error) {
                            $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                        },
                        check_callback: true,
                        worker: true
                    },
                    version: 1
                };
                $scope.reCreateTree = function () {
                    this.treeConfig.version++;
                }

                $scope.treeEventsObj = {
                    'ready': readyCB,
                    'create_node': createNodeCB
                }

                function readyCB() {
                    $log.info('ready called');

                };

                function createNodeCB(e, item) {
                    $log.info('create_node called');
                };


                $scope.setDiscrepancies = function () {
                    var node = $scope.treeInstance.jstree(true).get_selected();
                    if (node.length > 0) {
                        myHelp.getDetail('/share/part/' + node[0] + '?install=y')
                            .then(function (respons) {
                                $scope.discrepancie.id_part = respons.data.id_part;
                                $scope.discrepancie.keyword = respons.data.keyword;
                                $scope.discrepancie.part_number = respons.data.part_number;
                                $scope.discrepancie.id_part_install = respons.data.id_part_install;

                            });

                    }
                    else {
                        $scope.discrepancie.id_part = null;
                        $scope.discrepancie.keyword = "";
                        $scope.discrepancie.part_number = "";
                        $scope.discrepancie.keyword = "This Not have Parent";
                    }


                }


            });


        $scope.sumbitFormDiscrepancies = function () {

            if (!$scope.discrepancie.id_part) {
                errorView("Select part");
            }
            $scope.discrepancie.id_tech_log = $scope.log.id_tech_log;

            myHelp.postParam('/tech_rec/discrepancies', clearObj($scope.discrepancie))
                .then(function mySuccesresponsee() {
                        $scope.getDiscrepancies();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


        $scope.removeDiscrepancies = function (id) {
            myHelp.delete('/tech_rec/discrepancies/' + id)
                .then(function mySuccesresponsee() {
                        $scope.getDefects();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })


    .controller('app.aml.detail.jo', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {

        $scope.replace = {};
        $scope.replaces = {};
        $scope.jo = {};
        $scope.jo_detail = {};

        $scope.getJoDetail = function (id) {
            myHelp.getDetail('/tech_rec/flight_log_jo/' + id)
                .then(function (respons) {
                    $scope.jo_detail = respons.data.data;
                });
        }

        $scope.sumbitFormJo = function () {

            $scope.jo.id_tech_log = $scope.log.id_tech_log;
            $scope.jo.id_part = $scope.jo_detail.id_part;

            myHelp.postParam('/tech_rec/flight_log_jo', clearObj($scope.jo))
                .then(function mySuccesresponsee() {
                        $scope.getJos();
                        $state.go('^', {});
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

        $scope.removeJo = function (id) {
            myHelp.delete('/tech_rec/flight_log_jo/' + id)
                .then(function mySuccesresponsee() {
                        $scope.getJos();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })


    .controller('app.aml.detail.jo.add', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {

        $scope.getJoDetail = function () {
            myHelp.getDetail('/tech_rec/flight_log_jo/' + $stateParams.id)
                .then(function (respons) {
                    $scope.jo.id_mpc_jo = $stateParams.id;
                    $scope.jo_detail = respons.data.data;
                });
        }

        $scope.getJoDetail();


    })


    .controller('app.aml.detail.td', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {

        $scope.td_detail = {};
        $scope.removeTD = function (id) {
            myHelp.delete('/tech_rec/flight_log_td/' + id)
                .then(function mySuccesresponsee() {
                        $scope.getTDs();
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    .controller('app.aml.detail.td.close', function truncateCtrl($scope, $state, $stateParams, myHelp, $filter) {

        $scope.getTDDetail = function () {
            myHelp.getParam('/tech_rec/flight_log_td/' + $stateParams.id, {id_tech_log: $stateParams.id_tech_log})
                .then(function (respons) {
                    $scope.td_detail = respons.data.td;
                    $scope.td_detail.complite_h = respons.data.log.hours_cf;
                    $scope.td_detail.complite_c = respons.data.log.cycle_cf;
                    $scope.td_detail.complite_d = respons.data.log.tech_log_date;
                    $scope.td_detail.id_tech_log = respons.data.log.id_tech_log;
                });
        }

        $scope.getTDDetail();

        $scope.sumbitFormTD = function () {
            myHelp.putParam('/tech_rec/flight_log_td/' + $scope.td_detail.id_td_detail_ac, clearObj($scope.td_detail))
                .then(function mySuccesresponsee() {
                        $scope.getTDs();
                        $state.go('^', {});
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })

    .controller('app.aml.detail.rti', function truncateCtrl($scope, $state, $stateParams, myHelp, $log) {
        $scope.rti = {};
        $scope.identifys = [];

        $scope.rti.sn = "Please Select Part";
        $scope.rti.id_part == null;
        $scope.status_service = ['serviceable', 'unserviceable', 'reject'];

        $scope.getJoDetail = function () {
            myHelp.getDetail('/tech_rec/flight_log_jo/' + $scope.rti.id_mpc_jo)
                .then(function (respons) {
                    $scope.jo_detail = respons.data.data;
                });
        }


        $scope.filters = {};


        $scope.deleteRTI = function (id) {
            myHelp.deleteConfirm('/tech_rec/rti/' + id, {id_tech_log: $stateParams.id_tech_log}, 'app.aml.detail.rti', {});
            $scope.getRti();
            $scope.rti = {};
            $scope.identifys = [];
            $scope.filterAircraftRTI();
            $scope.getJos();
        }

        //part
        $scope.filterAircraftRTI = function () {
            console.log('dsdsds');
            myHelp.getParam('/share/tree_view', {id_aircraft: $stateParams.id_aircraft})
                .then(function (respons) {

                    $scope.treeData = respons.data.data;
                    $scope.treeConfig = {
                        core: {
                            multiple: false,
                            animation: true,
                            error: function (error) {
                                $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                            },
                            check_callback: true,
                            worker: true
                        },
                        version: 1
                    };
                    $scope.reCreateTree = function () {
                        this.treeConfig.version++;
                    }

                    $scope.treeEventsObj = {
                        'ready': readyCB,
                        'create_node': createNodeCB
                    }

                    function readyCB() {
                        $log.info('ready called');

                    };

                    function createNodeCB(e, item) {
                        $log.info('create_node called');
                    };

                    $scope.setPart = function () {
                        var node = $scope.treeInstance.jstree(true).get_selected();
                        if (node.length > 0) {
                            myHelp.getDetail('/share/part/' + node[0] + '?install=y')
                                .then(function (respons) {
                                    $scope.rti.id_part = respons.data.id_part;
                                    $scope.rti.id_part_install = node[0];
                                    $scope.rti.sn = respons.data.sn + ' ' + respons.data.keyword;
                                });

                        }
                        else {
                            $scope.rti.id_part = null;
                            $scope.rti.id_part_install = null;
                            $scope.rti.sn = "This Not have Selected Part";
                        }
                    }

                    $scope.setIdentify = function () {
                        var node = $scope.treeInstance.jstree(true).get_selected();
                        if (node.length > 0) {
                            myHelp.getDetail('/share/part/' + node[0] + '?install=y')
                                .then(function (respons) {

                                    var identify = {};
                                    identify.id_part = respons.data.id_part;
                                    identify.part_number = respons.data.part_number;
                                    identify.keyword = respons.data.keyword;
                                    identify.sn = respons.data.sn;
                                    identify.id_part_install = respons.data.id_part_install;

                                    $scope.identifys.push(identify);
                                });

                        }


                    }

                });

            $scope.removeIdentify = function (urutan) {
                $scope.identifys.splice(urutan - 1, 1);
            }
        }

        $scope.filterAircraftRTI();


        $scope.submitFormRTI = function () {
            if ($scope.rti.id_part == null) {
                errorView("Please Select Part");
                return false;
            }
            $scope.rti.hash_tech_log = $scope.log.id_tech_log;
            $scope.rti.id_aircraft = $stateParams.id_aircraft;
            myHelp.postParam('/tech_rec/rti', clearObj({tipe: 'injek', rti: $scope.rti, identify: $scope.identifys}))
                .then(function mySuccesresponsee() {
                        berhasilView();
                        $scope.getRti();
                        $scope.getJos();
                        $scope.rti = {};
                        $scope.identifys = [];
                        $scope.filterAircraftRTI();
                        $state.go('app.aml.detail.rti', {id_aircraft: $stateParams.id_aircraft}, {reload: "app.aml.detail.rti"});

                    }
                    , function myError() {
                        errorView("Please do not empty from !");
                    });
        };

    })


    .controller('app.aml.detail.install', function truncateCtrl($log, $scope, $state, $stateParams, myHelp, $filter) {
        $scope.install = {};
        $scope.getUninstall();
        $scope.getInstall();

        $scope.installIdentify = function (x) {
            myHelp.delete('/share/part_uninstall/' + x)
                .then(function mySuccesresponsee() {
                        $scope.getUninstall();
                        $scope.getInstall();
                    }
                    , function myError() {
                        errorView("Please do not empty from !");
                    });
        }

        $scope.removeInstall = function (x) {
            myHelp.delete('/tech_rec/flight_log_pris/' + x)
                .then(function mySuccesresponsee() {
                        $scope.getUninstall();
                        $scope.getInstall();
                    }
                    , function myError() {
                        errorView("Please do not empty from !");
                    });
        }

    })


    .controller('app.aml.detail.install.pris', function truncateCtrl($log, $scope, $state, $stateParams, myHelp, $filter) {
        $scope.install = {};
        $scope.type = '';

        $scope.getAllPris = function () {

            myHelp.getParam('/tech_rec/flight_log_pris/create', {
                id_aircraft: $stateParams.id_aircraft,
                id_part: $stateParams.id_part
            })
                .then(function mySuccesresponsee(response) {
                        $scope.priss = response.data.data;
                        $scope.type = response.data.type;
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };
        $scope.getAllPris();

        $scope.getPrisItem = function () {

            myHelp.getDetail('/tech_rec/flight_log_pris/' + clearString($scope.pree.id_engineer_pris_item))
                .then(function mySuccesresponsee(response) {
                        $scope.install = response.data.data;
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };

        $scope.sumbitFormIntsall = function () {

            $scope.install.hash_tech_log = $scope.log.id_tech_log;
            $scope.install.id_aircraft = $stateParams.id_aircraft;
            $scope.install.id_part_remove = $stateParams.id_part;
            $scope.install.type = $scope.type;

            myHelp.postParam('/tech_rec/flight_log_pris', clearObj($scope.install))
                .then(function mySuccesresponsee() {
                        $state.go("^", {id_tech_log: $stateParams.id_tech_log}, {reload: "app.aml.detail.install"});
                    }
                    , function myError() {
                        errorView("Error data entry (400)");
                    });
        };


    })


/*----------------------------------------------------------------------*/
/*                      MAINTENANCE
/*----------------------------------------------------------------------*/


